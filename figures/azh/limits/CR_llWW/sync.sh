#[[ -d /home/dnielsen/hep/work/AZH/WSMaker_AZHeavyH/cutbased_CRoutput/allyears_withuppermhcut_AntiJvarCorrections/ ]] || { return; exit; }
#cd /home/dnielsen/hep/work/AZH/WSMaker_AZHeavyH/cutbased_CRoutput/allyears_withuppermhcut_AntiJvarCorrections/
cd /groups/hep/dnielsen/work/AZH/WSMaker_AZHeavyH_old/output/
rsync -RLavP --exclude=*.eps --exclude=*.root --exclude=*.png --exclude=*.txt AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf ~/ANA-HDBS-2018-13-INT1/figures/limits/CR_llWW/
cd -
#for f in AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*; do mv $f ${f/./}; done
for f in AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*; do d=(${f//_/ }); mv $f AZH_${d[5]}_${d[6]}; done
