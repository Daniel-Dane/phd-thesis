#[[ -d /groups/hep/dnielsen/work/AZH/WSMaker_AZHeavyH/cutbased_output/allyears_withuppermhcut_AntiJvarCorrections/ ]] || { return; exit; }
#cd /groups/hep/dnielsen/work/AZH/WSMaker_AZHeavyH/cutbased_output/allyears_withuppermhcut_AntiJvarCorrections/
cd /groups/hep/dnielsen/work/AZH/WSMaker_AZHeavyH_old/output/
rsync -RLavP --exclude=*.eps --exclude=*.root --exclude=*.txt AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*/{plots/fcc/AsimovFit_unconditionnal_mu0/,pdf-files/,plots/prefit/} ~/ANA-HDBS-2018-13-INT1/figures/limits/llWW/
cd -
#for f in AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*; do mv $f ${f/./}; done
for f in AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*; do d=(${f//_/ }); mv $f AZH_${d[5]}_${d[6]}; done
