#[[ -d /groups/hep/dnielsen/work/AZH/WSMaker_AZHeavyH/cutbased_output/allyears_withuppermhcut_AntiJvarCorrections/ ]] || { return; exit; }
#cd /groups/hep/dnielsen/work/AZH/WSMaker_AZHeavyH/cutbased_output/allyears_withuppermhcut_AntiJvarCorrections/
cd /groups/hep/dnielsen/work/AZH/WSMaker_AZHeavyH/output/
rsync -RLavP --exclude=*.eps --exclude=*.root --exclude=*.txt AZHeavyHllWW2020WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*/{plots/fcc/{AsimovFit_unconditionnal_mu0,GlobalFit_unconditionnal_mu1}/NP_allExceptGammas.pdf,pdf-files/,plots/prefit/,plots/postfit/Region_BMin0_incJet1_Y2015_DSRcutbasedJvarH*_T2_L2_distmVHcorr_J2_GlobalFit_conditionnal_mu0*.pdf} ~/ANA-HDBS-2018-13-INT1/figures/limits/unblinded_llWW/
cd -
#for f in AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*; do mv $f ${f/./}; done
for f in AZHeavyHllWW2020WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*; do d=(${f//_/ }); rsync -a $f/ AZH_${d[5]}_${d[6]}; done
rm -r AZHeavyHllWW2020WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA*
