[[ -d old ]] && rm -r old
mkdir old
mv 600_*cutbased* bkg_2tag*cutbased* old/ 
for f in $(cat files.txt); do rsync -LavP --exclude=systs --exclude="*.eps" --exclude="*.txt" --exclude="*.tex" /home/dnielsen/hep/work/AZH/WW/MIA/run/plotsallyears/$f . & done
#for f in old/bkg_test/*; do cp /home/dnielsen/hep/work/AZH/WW/MIA/run/versions/20200218/plotsallyears/bkg_test/$(basename $f) bkg_test/$(basename $f); done
