\documentclass[twoside=semi, a4paper, symmetric, notoc, nohyper, nobib]{tufte-book}

% We need \documentclass[nohyper] and \usepackage{xcolor} first in order to set color on links (and not get boxes around the links)
\usepackage{xcolor}
\usepackage{hyperref}
\hypersetup{colorlinks, pdfborder = {0 0 0}, linkcolor={red!50!black}, citecolor={blue!70!black}, urlcolor={blue!70!black}}

\usepackage{bookmark}

% https://colorpalettes.net/color-palette-2378/
\definecolor{colchap}{HTML}{d98100}
\definecolor{colsec}{HTML}{e1b200}
\definecolor{colsubsec}{HTML}{4b85a8}
%                              aba597
%\definecolor{colsubsubsec}{HTML}{182139}

\titleformat{\chapter}%
{\huge\rmfamily\boldmath\bfseries\color{colchap}}% format applied to label+text
{\colorbox{colchap}{\parbox{1.5cm}{\hfill\itshape\huge\color{white}\thechapter}}}% label
{2pt}% horizontal separation between label and title body
{\quad}% before the title body
[]% after the title body

% section format
\titleformat{\section}%
{\normalfont\Large\boldmath\bfseries\color{colsec}}% format applied to label+text
{\colorbox{colsec}{\parbox{1.5cm}{\hfill\color{white}\thesection}}}% label
{1em}% horizontal separation between label and title body
{}% before the title body
[]% after the title body

% subsection format
\titleformat{\subsection}%
{\normalfont\large\boldmath\bfseries\color{colsubsec}}% format applied to label+text
{\colorbox{colsubsec}{\parbox{1.5cm}{\hfill\color{white}\thesubsection}}}% label
{1em}% horizontal separation between label and title body
{}% before the title body
[]% after the title body

%%
% Book metadata
\title[In search of new Higgs bosons]{In search of\\\noindent new Higgs bosons}
\author[Daniel S. Nielsen]{Daniel S. Nielsen}
\publisher{PhD thesis \\ \noindent Niels Bohr Institute, University of Copenhagen}

\usepackage{amssymb}
\usepackage{adjustbox}

% Just some sample text
\usepackage{lipsum}

% Puts a box in a figure environment
\newcommand\FramedBox[3]{%
  \setlength\fboxsep{0pt}
  \mbox{\parbox[t][#1][c]{#2}{\centering\large #3}}
}

% Introduces classiccaptionstyle used in pagefigure/pagetable
\RequirePackage{etoolbox}
  \makeatletter
  \newif\if@tufte@margtab\@tufte@margtabfalse
  \AtBeginEnvironment{margintable}{\@tufte@margtabtrue}
  \AtEndEnvironment{margintable}{\@tufte@margtabfalse}
  \newcommand{\classiccaptionstyle}{ % 
      \long\def\@caption##1[##2]##3{%
          \par
          \addcontentsline{\csname ext@##1\endcsname}{##1}%
          { \protect\numberline{\csname the##1\endcsname}{\ignorespaces ##2}}%
          \begingroup
          \@parboxrestore
          \if@minipage
          \@setminipage
          \fi
          \normalsize
          \@makecaption{\csname fnum@##1\endcsname}{ \ignorespaces ##3}\par
          \endgroup}
      \long\def\@makecaption##1##2{ %
          \vskip\abovecaptionskip
          \sbox\@tempboxa{ \@tufte@caption@font##1: ##2}%
          \ifdim \wd\@tempboxa >\hsize
          \@tufte@caption@font\if@tufte@margtab\@tufte@caption@justification\fi##1:\raggedright ##2\par % remove \raggedright if you want the justified version
          \else
          \global \@minipagefalse
          \hb@xt@\hsize{ \hfil\box\@tempboxa\hfil}%
          \fi
          \vskip\belowcaptionskip}
        \setcaptionfont{\normalfont\small\setstretch{0.95}}
      \let\caption\@tufte@orig@caption%
      \let\label\@tufte@orig@label}
  \makeatother

% Creates environment for figures with classiccaption
\newenvironment{pagefigure}{%
  \begin{figure*}[p]
  \classiccaptionstyle
}{\end{figure*}}

% Creates environment for tables with classiccaption
\newenvironment{pagetable}{%
  \begin{table*}[p]
  \classiccaptionstyle
}{\end{table*}}

% ???
\usepackage{amsmath}

% For small inline matrices
\usepackage{mathtools}

% For nicely typeset tabular material
\usepackage{booktabs}

% For graphics / images
\usepackage{graphicx}
\setkeys{Gin}{width=\linewidth,totalheight=\textheight,keepaspectratio}
\graphicspath{{graphics/}}

% To center figures vertically
%\usepackage{graphbox}

% The fancyvrb package lets us customize the formatting of verbatim
% environments.  We use a slightly smaller font.
\usepackage{fancyvrb}
\fvset{fontsize=\normalsize}

%%
% Prints argument within hanging parentheses (i.e., parentheses that take
% up no horizontal space).  Useful in tabular environments.
\newcommand{\hangp}[1]{\makebox[0pt][r]{(}#1\makebox[0pt][l]{)}}

%%
% Prints an asterisk that takes up no horizontal space.
% Useful in tabular environments.
\newcommand{\hangstar}{\makebox[0pt][l]{*}}

%%
% Prints a trailing space in a smart way.
\usepackage{xspace}

% ???
\usepackage{units}

% Some atlas style files which include for instance \Zee
\usepackage{styles/atlasmisc} 
\usepackage{styles/atlasparticle} 
\usepackage{styles/atlasprocess} 
\usepackage{styles/atlasunit} 
\usepackage{styles/atlasxref} 
\usepackage{styles/TPPaper-defs} 

% Prints the month name (e.g., January) and the year (e.g., 2008)
\newcommand{\monthyear}{%
    \ifcase\month\or January\or February\or March\or April\or May\or June\or
    July\or August\or September\or October\or November\or
    December\fi\space\number\year
}

% Prints an epigraph and speaker in sans serif, all-caps type.
\newcommand{\openepigraph}[2]{%
    %\sffamily\fontsize{14}{16}\selectfont
    \begin{fullwidth}
        \sffamily\large
        \begin{doublespace}
            \noindent\allcaps{#1}\\% epigraph
            \noindent\allcaps{#2}% author
        \end{doublespace}
    \end{fullwidth}
}
% Inserts a blank page
\newcommand{\blankpage}{\newpage\hbox{}\thispagestyle{empty}\newpage}

% Typesets the font size, leading, and measure in the form of 10/12x26 pc.
\newcommand{\measure}[3]{#1/#2$\times$\unit[#3]{pc}}

\renewcommand{\cite}[2][0pt]{\sidenote[][#1]{\fullcite{#2}}}
% Macros for typesetting the documentation
\newcommand{\hlred}[1]{\textcolor{Maroon}{#1}}% prints in red
\newcommand{\hangleft}[1]{\makebox[0pt][r]{#1}}
\newcommand{\hairsp}{\hspace{1pt}}% hair space
\newcommand{\hquad}{\hskip0.5em\relax}% half quad space
\newcommand{\TODO}{\textcolor{red}{\bf TODO!}\xspace}
\newcommand{\ie}{\textit{i.\hairsp{}e.}\xspace}
\newcommand{\eg}{\textit{e.\hairsp{}g.}\xspace}
\newcommand{\na}{\quad--}% used in tables for N/A cells
\providecommand{\XeLaTeX}{X\lower.5ex\hbox{\kern-0.15em\reflectbox{E}}\kern-0.1em\LaTeX}
\newcommand{\tXeLaTeX}{\XeLaTeX\index{XeLaTeX@\protect\XeLaTeX}}
% \index{\texttt{\textbackslash xyz}@\hangleft{\texttt{\textbackslash}}\texttt{xyz}}
\newcommand{\tuftebs}{\symbol{'134}}% a backslash in tt type in OT1/T1
\newcommand{\doccmdnoindex}[2][]{\texttt{\tuftebs#2}}% command name -- adds backslash automatically (and doesn't add cmd to the index)
\newcommand{\doccmddef}[2][]{%
    \hlred{\texttt{\tuftebs#2}}\label{cmd:#2}%
    \ifthenelse{\isempty{#1}}%
    {% add the command to the index
        \index{#2 command@\protect\hangleft{\texttt{\tuftebs}}\texttt{#2}}% command name
    }%
    {% add the command and package to the index
        \index{#2 command@\protect\hangleft{\texttt{\tuftebs}}\texttt{#2} (\texttt{#1} package)}% command name
        \index{#1 package@\texttt{#1} package}\index{packages!#1@\texttt{#1}}% package name
    }%
}% command name -- adds backslash automatically
\newcommand{\doccmd}[2][]{%
    \texttt{\tuftebs#2}%
    \ifthenelse{\isempty{#1}}%
    {% add the command to the index
        \index{#2 command@\protect\hangleft{\texttt{\tuftebs}}\texttt{#2}}% command name
    }%
    {% add the command and package to the index
        \index{#2 command@\protect\hangleft{\texttt{\tuftebs}}\texttt{#2} (\texttt{#1} package)}% command name
        \index{#1 package@\texttt{#1} package}\index{packages!#1@\texttt{#1}}% package name
    }%
}% command name -- adds backslash automatically
\newcommand{\docopt}[1]{\ensuremath{\langle}\textrm{\textit{#1}}\ensuremath{\rangle}}% optional command argument
\newcommand{\docarg}[1]{\textrm{\textit{#1}}}% (required) command argument
\newenvironment{docspec}{\begin{quotation}\ttfamily\parskip0pt\parindent0pt\ignorespaces}{\end{quotation}}% command specification environment
\newcommand{\docenv}[1]{\texttt{#1}\index{#1 environment@\texttt{#1} environment}\index{environments!#1@\texttt{#1}}}% environment name
\newcommand{\docenvdef}[1]{\hlred{\texttt{#1}}\label{env:#1}\index{#1 environment@\texttt{#1} environment}\index{environments!#1@\texttt{#1}}}% environment name
\newcommand{\docpkg}[1]{\texttt{#1}\index{#1 package@\texttt{#1} package}\index{packages!#1@\texttt{#1}}}% package name
\newcommand{\doccls}[1]{\texttt{#1}}% document class name
\newcommand{\docclsopt}[1]{\texttt{#1}\index{#1 class option@\texttt{#1} class option}\index{class options!#1@\texttt{#1}}}% document class option name
\newcommand{\docclsoptdef}[1]{\hlred{\texttt{#1}}\label{clsopt:#1}\index{#1 class option@\texttt{#1} class option}\index{class options!#1@\texttt{#1}}}% document class option name defined
\newcommand{\docmsg}[2]{\bigskip\begin{fullwidth}\noindent\ttfamily#1\end{fullwidth}\medskip\par\noindent#2}
\newcommand{\docfilehook}[2]{\texttt{#1}\index{file hooks!#2}\index{#1@\texttt{#1}}}
\newcommand{\doccounter}[1]{\texttt{#1}\index{#1 counter@\texttt{#1} counter}}


\newcommand*{\DeltaR}{\ensuremath{\Delta \text{R}}\xspace}
\newcommand*{\dR}{\ensuremath{\Delta \text{R}}\xspace}
\newcommand*{\photon}{\ensuremath{\gamma}\xspace}
\newcommand*{\mue}{\ensuremath{\mu e\ \xspace}}
\newcommand*{\llbb}{\ensuremath{\ell\ell bb}\xspace}
\newcommand*{\llWW}{\ensuremath{\ell\ell WW}\xspace}
\newcommand*{\rowspace}{\rule{0pt}{4ex}}

\geometry{a4paper,left=24.8mm,top=27.4mm,headsep=2\baselineskip,textwidth=110mm,marginparsep=8.2mm,marginparwidth=49.4mm,textheight=49\baselineskip,headheight=\baselineskip}
%\geometry{a4paper,left=20.8mm,top=20.4mm,bottom=20mm,right=70mm,headsep=1\baselineskip,textwidth=120mm,marginparsep=4.2mm,marginparwidth=50.4mm,textheight=44\baselineskip,headheight=\baselineskip}


%%\usepackage{geometry}
%\usepackage{color,soul}
%\definecolor{shadecolor}{RGB}{230,230,230}
%\newcommand{\hlc}[2][shadecolor]{ {\sethlcolor{#1} \hl{#2}} }

% table of contents for each chapter
\usepackage{minitoc}

% Does not work with cleveref, even with the fix; use the terminal trick below this block to find unreferenced labels
%% checks if there are unused references
%%-----------------------------------------------------------
%%	Option       Command        Switch Function
%%-----------------------------------------------------------
%%	showrefs∗    \showrefnames  on     to put keys of labels
%%	norefs       \norefnames    off    in the marginal notes
%%-----------------------------------------------------------
%%	showcites∗   \showcitenames on     to put bibitem keys
%%	nocites      \nocitenames   off    in the marginal notes
%%-----------------------------------------------------------
%%	msgs∗        \setonmsgs     on     to write Refcheck’s
%%	nomsgs       \setoffmsgs    off    messages to .log
%%-----------------------------------------------------------
%%	chkunlbld∗   \checkunlbld   on     to check unlabelled
%%	ignoreunlbld \ignoreunlbld  off    equations.
%%-----------------------------------------------------------
%%                     * = default
\usepackage[norefs,nocites,msgs,ignoreunlbld]{refcheck}
%
%% To make refcheck aware of cleveref (nope)
%\makeatletter
%\newcommand{\refcheckize}[1]{%
%	\expandafter\let\csname @@\string#1\endcsname#1%
%	\expandafter\DeclareRobustCommand\csname relax\string#1\endcsname[1]{%
%		\csname @@\string#1\endcsname{##1}\@for\@temp:=##1\do{\wrtusdrf{\@temp}\wrtusdrf{{\@temp}}}}%
%	\expandafter\let\expandafter#1\csname relax\string#1\endcsname
%}
%\newcommand{\refcheckizetwo}[1]{%
%	\expandafter\let\csname @@\string#1\endcsname#1%
%	\expandafter\DeclareRobustCommand\csname relax\string#1\endcsname[2]{%
%		\csname @@\string#1\endcsname{##1}{##2}\wrtusdrf{##1}\wrtusdrf{{##1}}\wrtusdrf{##2}\wrtusdrf{{##2}}}%
%	\expandafter\let\expandafter#1\csname relax\string#1\endcsname
%}
%\makeatother
%
%\refcheckize{\cref}
%\refcheckize{\Cref}
%\refcheckizetwo{\crefrange}
%\refcheckizetwo{\Crefrange}

% Dump this in the directory containing the .tex files
% Inspired by https://tex.stackexchange.com/a/551055/47267
% But works with .tex files in subfolders as well; and it ignores labels and refs that are commented out; and it works with labels with similar names
% Fails to find labels if they contain \% before them (when does that happen, though?)
% Will falsely report labels as unreferenced if they contain \% before them (better false positive than no report, though)
% To include appendix files as well, remove "-not -iname '*appendix*.tex'"
%echo "Checking for unreferenced labels...";
%files=$(find . -iname '*.tex' -not -iname '*appendix*.tex')
%labels=$(sed -E "s/^[ \t]*[^%]*\\\label\{([^}]+)\}.*/\1/g;t;d" $files)
%for label in $labels; do
%grep -Eq "^[ \t]*[^%]*ref{[^}]*$label[},]" $files || echo "Label '$label' not referenced"
%done
%

% ??
\usepackage[caption=false]{subfig}
%\usepackage{subcaption}

% biblatex for references ( run biber thesis after pdflatex thesis.tex )
\usepackage[style=numeric-comp, sorting=none, backref]{biblatex}%<- specify style
%\DeclareFieldFormat
%[article,inbook,incollection,inproceedings,patent,thesis,
%unpublished,techreport,misc,book,booklet]
%{title}{\mkbibquote{#1}}
\addbibresource{references.bib}%<- specify bib file

\setcounter{tocdepth}{2}

\usepackage{array,multirow,makecell}
\newcolumntype{C}[1]{>{\centering\let\newline\\\arraybackslash\hspace{0pt}}m{#1}}

\usepackage{float}
% Generates the index
\usepackage{makeidx}
\makeindex

\usepackage[nottoc]{tocbibind}

\newcommand*\cleartoleftpage{%
  \clearpage
  \ifodd\value{page}\hbox{}\newpage\fi
}

\hyphenation{back-ground}
\hyphenation{brems-strahlung}
\hyphenation{min-i-mum}
\hyphenation{de-riv-a-tives}
\hyphenation{thresh-old}
\hyphenation{align-ment}
\hyphenation{con-ven-tion}
\hyphenation{Adapt-ed}

% showing page rotate in final document
%\usepackage{rotating} 
%\usepackage{pdflscape}

\renewcommand{\rmdefault}{pplx}
\usepackage{perpage} %the perpage package
\MakePerPage{footnote} %the perpage package command

% new column types which make it possible to align the rows at one specific character
\newcolumntype{R}{>{$}r<{$}}
\newcolumntype{L}{>{$}l<{$}}
\newcolumntype{M}{R@{${}\times{}$}L}
\newcolumntype{A}{R@{${}|\eta|{}$}L}
\newcolumntype{P}[1]{>{\centering\arraybackslash}p{#1}}

% Proper square root symbol
\usepackage{letltxmacro}
\makeatletter
\let\oldr@@t\r@@t
\def\r@@t#1#2{%
    \setbox0=\hbox{$\oldr@@t#1{#2\,}$}\dimen0=\ht0
    \advance\dimen0-0.2\ht0
    \setbox2=\hbox{\vrule height\ht0 depth -\dimen0}%
    {\box0\lower0.4pt\box2}}
\LetLtxMacro{\oldsqrt}{\sqrt}
\renewcommand*{\sqrt}[2][\ ]{\oldsqrt[#1]{#2}}
\makeatother

% dots in list of figures
\makeatletter
\renewcommand*\l@figure{\@dottedtocline{1}{1.5em}{2.3em}}
\makeatother

% dots in list of tables
\makeatletter
\renewcommand*\l@table{\@dottedtocline{1}{1.5em}{2.3em}}
\makeatother

% thick horizontal line in tables
\makeatletter
\newcommand{\thickhline}{%
    \noalign {\ifnum 0=`}\fi \hrule height 1pt
    \futurelet \reserved@a \@xhline
}
\newcolumntype{"}{@{\hskip\tabcolsep\vrule width 1pt\hskip\tabcolsep}}
\makeatother

% ???
%\usepackage{afterpage}

% appendix per chapter
\usepackage{appendix}
% Fuckin' a! This bad boy took a while to find; without it, the first appendix toc won't ever show up!
\let\oldappendices\appendices
\def\appendices{\oldappendices\adjustmtc}

\usepackage{pgffor} % For the amazing for loop
\usepackage[nomessages]{fp} % For simple calculations
%\usepackage{threeparttable} % Better tables
\usepackage{arydshln} % Dashed hline for tables
\usepackage{tabularx} % Better tables (especially the X column type which spans the remaining width)

% additional enumerate options; break tufte lists, though
%\usepackage[]{enumitem}
%\setlist[enumerate]{leftmargin=0pt} % move items into the margin
%\setlist[itemize]{leftmargin=0pt}
%\setlist{noitemsep} % no space between items (still space before and after list)

%\let\OLDitemize\itemize
%\renewcommand\itemize{\OLDitemize\setlength{\itemsep}{0pt}}
%
%\let\OLDenumerate\enumerate
%\renewcommand\enumerate{\OLDenumerate\setlength{\itemsep}{0pt}}

% Like the normal itemize and enumrate, but text is squeezed together much more
\newenvironment{myitemize}
{ \begin{itemize}
		\setlength{\itemsep}{0pt}
		\setlength{\parskip}{0pt}
		\setlength{\parsep}{0pt}     }
	{ \end{itemize}                  } 

\newenvironment{myenumerate}
{ \begin{enumerate}
		\setlength{\itemsep}{0pt}
		\setlength{\parskip}{0pt}
		\setlength{\parsep}{0pt}     }
	{ \end{enumerate}                  } 

\usepackage{feynmp}
\usepackage{feynmp-auto} % FeynMF to make Feynman 

% Awesome label reference package; customized to use abbr.
\usepackage{cleveref}
\newcommand{\creflastconjunction}{, and\nobreakspace} % Oxford comma
\newcommand{\crefrangeconjunction}{--} % en-dash instead of "to"
\crefname{chapter}{Chap.}{Chaps.}
\crefname{section}{Sect.}{Sects.}
\crefname{subsection}{Sect.}{Sects.}
\crefname{table}{Tab.}{Tabs.}
\crefname{appendix}{App.}{Apps.}
%\crefname{appsec}{App.}{Apps.}
\crefname{page}{p.}{pp.}
\crefname{equation}{Eq.}{Eqs.}
\crefformat{equation}{Eq.~(#2#1#3)} % Single: Eq. (2.1)
\crefrangeformat{equation}{Eqs.~(#3#1#4) to~(#5#2#6)} % Range: Eqs. (2.1) to (2.4)
\crefmultiformat{equation}{Eqs.~(#2#1#3)}{ and~(#2#1#3)}{, (#2#1#3)}{, and~(#2#1#3)} % Multi: Eqs. (2.1), (2.3), and (2.4) or Eqs. (2.1) and (2.3)
\crefrangelabelformat{equation}{(#3#1#4--#5\crefstripprefix{#1}{#2}#6)}
\crefrangelabelformat{subequation}{(#3#1#4--#5\crefstripprefix{#1}{#2}#6)} % Combine same sub-eq: Eqs. (1a–c)
\crefname{figure}{Fig.}{Figs.}
%\crefformat{figure}{Fig.~#2#1#3}
%\crefrangeformat{figure}{Figs.~#3#1#4--~#5\crefstripprefix{#1}{#2}#6}
%\crefmultiformat{figure}%
%	{\edef\crefstripprefixinfo{#1}Figs.~#2#1#3}%
%	{ and~#2#1#3}%
%	{, #2\crefstripprefix{\crefstripprefixinfo}{#1}#3}%
%	{, and~#2\crefstripprefix{\crefstripprefixinfo}{#1}#3}
\crefname{subfigure}{Fig.}{Figs.}
\crefname{subsubfigure}{Fig.}{Figs.}
\AtBeginEnvironment{subappendices}{\crefalias{section}{appendix}\crefalias{subsection}{appendix}} % (Sub)sections in appendix chapter should be referred to as App.

% Only use this, if you cannot use cleveref. Do not uncomment; it will break compilation.
%% This block: http://www.tedpavlic.com/post_preamble_for_journal_of_math_bio.php
%\labelformat{equation}{\textup{(#1)}}
%\labelformat{enumi}{\textup{(#1)}}
%\let\orgautoref\autoref
%% \autoref is used inside a sentence 
%% (this is a renew of the standard)
%\renewcommand{\autoref}
{%
%	\def\partautorefname{Part}%
%	\def\sectionautorefname{Sec.}%
%	\def\subsectionautorefname{Sec.}%
%	\def\tableautorefname{Tab.}%
%	\def\figureautorefname{Fig.}%
%	\def\chapterautorefname{Chap.}%
%	\def\appendixautorefname{Chap.}%
%	\def\equationautorefname{Eq.}%
%	\def\AMSnameautorefname{Eq.}%
%	\def\itemautorefname{Item}%
%	\def\pageautorefname{p.}%
%	\orgautoref%
%}
%% \autorefs is plural for inside a sentence
%\providecommand{\autorefs}
{%
%	\def\partautorefname{Parts}%
%	\def\sectionautorefname{Secs.}%
%	\def\subsectionautorefname{Secs.}%
%	\def\tableautorefname{Tabs.}%
%	\def\figureautorefname{Figs.}%
%	\def\chapterautorefname{Chaps.}%
%	\def\appendixautorefname{Chaps.}%
%	\def\equationautorefname{Eqs.}%
%	\def\AMSnameautorefname{Eqs.}%
%	\def\itemautorefname{Items}%
%	\def\pageautorefname{pp.}%
%	\orgautoref%
%}

\NewDocumentCommand{\rot}{O{45} O{1em} m}{\makebox[#2][l]{\rotatebox{#1}{#3}}}%
\NewDocumentCommand{\rotNinety}{O{90} O{1em} m}{\makebox[#2][l]{\rotatebox{#1}{#3}}}%
\newcommand{\tabitem}{~~\llap{\textbullet}~~}

% Fix marginfigure moving outside the page
% This makes them collide with figure captions instead!
%\usepackage{marginfix}

\usepackage{caption}
\captionsetup{font={small,stretch=0.95},position=bottom} % Smaller text in figure captions
\DeclareCaptionFormat{myformat}{#1#2#3\vspace{10pt}} % Space between subfloat captions and outer captions/text
\captionsetup[subfloat]{format=myformat}

% Used for something
\usepackage{rotating}

% up-right integral
\usepackage[integrals]{wasysym}

% To write proper bra and ket
\usepackage{braket}

% Even smaller tfrac (which is the small version of frac)
\newcommand{\ttfrac}{\genfrac{}{}{}2}

% Renew this command with the path to your figures so you don't have to write the full path for all subfigures from the same folder
\newcommand{\figpath}{}
\usepackage{wallpaper}
\definecolor{bkgcolor}{HTML}{004a17}
\AddToShipoutPictureBG*{\AtPageLowerLeft{\color{bkgcolor}\rule{\paperwidth}{\paperheight}}}
\ThisLRCornerWallPaper{1.0}{figures/Sigilum_Facultatis_Naturalis}
%\LRCornerWallPaper{0.5}{figures/Sigilum_Facultatis_Naturalis}
\usepackage{pagecolor}
\usepackage{afterpage}

\def \minGGAlimllWW {0.023} % 0.0226
\def \maxGGAlimllWW {8.9} % 8.9237
\def \minGGAlimExpllWW{0.041} % 0.0407
\def \maxGGAlimExpllWW{3.6} % 3.5533
\def \minGGAlimPointllWW{(770, 660)}
\def \maxGGAlimPointllWW{(340, 220)}

%\includeonly{sections/general/abstract,sections/azh/eventselection}
%\includeonly{sections/general/abstract,sections/general/introduction,sections/azh/conclusion,sections/conclusion}
%\includeonly{sections/trt/appendixCalibResultsAllParts,sections/trt/appendixZScores,sections/trt/appendixCalibResultsMCOnData,sections/trt/appendixCorrectionFactors,sections/trt/appendixProbForAllCFs,sections/trt/appendixCFCorrWithOcc,sections/trt/appendixROCforAdditionalCFs,sections/trt/appendixROCWith80GeVLines,sections/trt/appendixTracksBasedpHT}
%\includeonly{sections/azh/samples,sections/azh/objectdefinitions,sections/azh/eventselection,sections/azh/backgroundestimation,sections/azh/signalmodeling,sections/azh/systs,sections/azh/fitmodel,sections/azh/results,sections/azh/conclusion,sections/azh/appendix/appendix_example}

\begin{document}
	% Force numbers to start at the title page, which normal people don't want
	\mainmatter
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	% Custom title page
	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\begin{titlepage}
		\begin{fullwidth}
			\begin{center}
				\vspace*{-50pt}
				\color{white}
				{\textsc{Daniel S. Nielsen}}\\
				PhD thesis, Niels Bohr Institute, University of Copenhagen\\
				%	\vfill
				\vspace*{35pt}
				\textsc{
					\fontsize{60}{0}\selectfont
					In search of\\[30pt]
					new Higgs\\[30pt]
					bosons\\[30pt]
				}
				\vspace{\fill}
				\scriptsize
				This thesis has been submitted to the PhD School of The Faculty of Science, University of Copenhagen
				\clearpage
			\end{center}
	\end{fullwidth}
	%	{\bfseries\Large
	%		Performance Report\\
	%		First Quarter 2011\\
	%		\vskip2cm
	%		A. Uthor\\
	%	}    
	%	\vfill
	%	
	%	\vfill
		\vfill
		{}
	%\newpagecolor{gray}\afterpage{\restorepagecolor}
	\end{titlepage}


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % Frontmatter
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    \hypersetup{colorlinks,linkcolor={black},citecolor={black},urlcolor={red}}  
%    \frontmatter
%    \maketitle
    \clearpage
    \include{sections/general/jacket}
%    \cleardoublepage 

%	% ensure we're on an odd page
%	\cleardoublepage
%	% open a group for doing local redefinitions
%	\begingroup
%	% a \clearpage will close the group and restore the meaning
%	\let\clearpage\endgroup
%	
%	% here you type the text you want 
%	\emph{Note: Parts II and III each have their own appendix chapter.}
%	% Now issue \tableofcontents
%

    \dominitoc
    \tableofcontents
    { % Need to change pagestyle because \mainmatter is before the main matter; the brackets are super important
    \pagestyle{plain}
 
 
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % Meta sections
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \cleardoublepage
    \setcounter{secnumdepth}{0}
    \include{sections/general/abstract}
    \include{sections/general/disclaimer}
    \include{sections/general/acknowledgements}
    \include{sections/general/introduction}
	}
%    \mainmatter
    \setcounter{secnumdepth}{2}
%    \setcounter{mtc}{2}


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % PART I: Background
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \part{Background}
    \include{sections/info/standardmodel}


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	% PART II: Experimental setup
	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\part{Experimental setup}
    \include{sections/info/atlasexperiment}
    \include{sections/info/reconstruction}
%    \include{sections/info/trttheory}


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % PART III: Performance analysis: TRT electron identification
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \part{Performance analysis: TRT electron identification}
    \include{sections/trt/introduction}
    \include{sections/trt/method}
    \include{sections/trt/calib}
%    \include{sections/trt/additionalanalysis}
    \include{sections/trt/conclusion}

%    \chapter{Appendix}
%    \begin{subappendices}
%        \minitoc
%        \include{sections/trt/appendixCalibResultsAllParts}
%%        \include{sections/trt/appendixFitResultsRawData}
%        \include{sections/trt/appendixZScores}
%        \include{sections/trt/appendixCalibResultsMCOnData}
%        \include{sections/trt/appendixCorrectionFactors}
%        \include{sections/trt/appendixProbForAllCFs}
%        \include{sections/trt/appendixCFCorrWithOcc}
%        \include{sections/trt/appendixROCforAdditionalCFs}
%        \include{sections/trt/appendixROCWith80GeVLines}
%        \include{sections/trt/appendixTracksBasedpHT}
%    \end{subappendices}


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % PART IV: Physics analysis: Off-alignment Higgs bosons search
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    \part{Physics analysis: Off-alignment Higgs bosons search}
    \include{sections/azh/samples}
    \include{sections/azh/objectdefinitions} 
    \include{sections/azh/eventselection}
    \include{sections/azh/backgroundestimation}
    \include{sections/azh/signalmodeling}
    \include{sections/azh/systs}
    \include{sections/azh/fitmodel}
    \include{sections/azh/results}
    \include{sections/azh/conclusion} 

%    \chapter{Appendix}
%%	\crefalias{section}{appsec}
%%	\crefalias{subsection}{appendix}
%    \begin{subappendices}
%        \minitoc
%        \include{sections/azh/appendix/appendix_example}
%    \end{subappendices}


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	% PART V: Conclusion and bibliography
	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\part{Conclusion and bibliography}
	\include{sections/conclusion}

%	\part{Bibliography}
	\printbibliography[heading=bibintoc]
	\label{LastPage}


    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	%
	% PART VI: Appendix
	%
	%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
	\part{Appendix}
	\chapter{Performance analysis}
		\begin{subappendices}
			% Fuckin' a! This bad boy took a while to find; without it, this appendix toc won't ever show up!
			\adjustmtc
			\minitoc
			\include{sections/trt/appendixCalibResultsAllParts}
			%        \include{sections/trt/appendixFitResultsRawData}
			\include{sections/trt/appendixZScores}
			\include{sections/trt/appendixCalibResultsMCOnData}
			\include{sections/trt/appendixCorrectionFactors}
			\include{sections/trt/appendixProbForAllCFs}
			\include{sections/trt/appendixCFCorrWithOcc}
			\include{sections/trt/appendixROCforAdditionalCFs}
			\include{sections/trt/appendixROCWith80GeVLines}
			\include{sections/trt/appendixTracksBasedpHT}
		\end{subappendices}
	\chapter{Physics analysis}
		\begin{subappendices}
			\minitoc
			\include{sections/azh/appendix/appendix_example}
		\end{subappendices}

    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    %
    % Backmatter
    %
    %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%    \backmatter
%    \listoffigures
%    \listoftables
%    \glstoctrue
%    \printglossary[type=acronym,style=index]
%    \clearpage
%    \pagestyle{empty}
%    \cleardoublepage
%    \makebackpage
\end{document}