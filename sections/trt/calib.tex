\chapter{Analysis and results}
\emph{\small
	The tuning of the likelihood parameters will now follow. After this, the correction factors will be analyzed. Finally, the full PID tool is applied and ROC curves\footnote{\url{https://en.wikipedia.org/wiki/Receiver_operating_characteristic}} are shown.
}
\minitoc
\label{sec:calib}

\section{Calibration tuning}
\label{sec:calibration}
\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[page=1,width=0.45\textwidth]{figures/trt/data/probHTSliceLines.pdf}
	\includegraphics[page=1,width=0.45\textwidth]{figures/trt/mc/probHTSliceLines.pdf}
	\caption{The HT fraction plotted as a function of the track occupancy and the $\gamma$ factor with the left cluster in each sub-figure coming from muons and the right cluster coming from electrons. This figure is only for xenon straws in the barrel. The colored lines correspond to the colored points on the upper sub-figures of \cref{fig:fraction_of_gamma_factor}.}
	\label{fig:fraction_2D}
\end{figure*}
As explained in the previous section, each HT and LT hit is added to their respective 2D histograms, which are a function of the $\gamma$ factor and the track occupancy. These histograms are separated into the barrel, endcap A, and endcap B parts and further into argon and xenon gases. When the histograms are filled, the HT and LT histograms are divided to give the HT fraction (or probability), since the LT histograms also contain the HT hits. These histograms can be seen in \cref{fig:fraction_2D,fig:fraction_2D_after_fit,fig:fraction_2D_after_fit_ratio,fig:fraction_of_gamma_factor} for xenon in the barrel. See \cref{sec:appendixCalibResultsAllGasesAllParts} for the other parts and gases.%, and see \cref{sec:appendixFitResultsRawData} for the raw results of the fitting (which includes $\chi^2$).

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\subfloat[The HT fraction plotted as a function of the track occupancy and the $\gamma$ factor after fitting \cref{fig:fraction_2D}. Muons are on the left and electrons are on the right. This figure is only for xenon straws in the barrel.]{
			\includegraphics[page=1,width=0.45\textwidth]{figures/trt/data/probHTAfterFitFullWithBoxes.pdf}
			\includegraphics[page=1,width=0.45\textwidth]{figures/trt/mc/probHTAfterFitFullWithBoxes.pdf}
			\label{fig:fraction_2D_after_fit}
		}
		\\\vspace*{-10pt}
		\subfloat[Ratio of HT fractions before (\cref{fig:fraction_2D}) and after the fit (\cref{fig:fraction_2D_after_fit}).]{
			\includegraphics[page=1,width=0.45\textwidth]{figures/trt/data/probHTAfterFitRatio.pdf}
			\includegraphics[page=1,width=0.45\textwidth]{figures/trt/mc/probHTAfterFitRatio.pdf}
			\label{fig:fraction_2D_after_fit_ratio}
		}
		\\\vspace*{-10pt}
		\subfloat[Onset curves after fitting in slices of occupancy. The above sub-figures show slices in track occupancy of the 2D histograms before (points) and after (lines) fitting. The lower sub-figures show the 2D histograms before fitting for all $\gamma$ values (points) and after fitting for two choices of $\gamma$ (lines). This figure is only for xenon straws in the barrel. The arrow in the upper-right sub-figure points to a jump most clearly seen in simulation in the barrel with xenon.]{\begin{tabular}[b]{c}%
			\includegraphics[page=1,width=0.45\textwidth]{figures/trt/data/finalFitsOcc.pdf}
			\includegraphics[page=1,width=0.45\textwidth]{figures/trt/mc/finalFitsOcc.pdf}\\
			\includegraphics[page=1,width=0.45\textwidth]{figures/trt/data/finalFitsGamma.pdf}
			\includegraphics[page=1,width=0.45\textwidth]{figures/trt/mc/finalFitsGamma.pdf}
			\label{fig:fraction_of_gamma_factor}
		\end{tabular}}
	\end{center}
	\vspace*{-20pt}
	\caption{(a) The HT fraction after fitting. (b) The ratio of the fit to the raw values. (c) The fit shown in slices of occupancy (upper row) and $\gamma$ factor (lower row).}
\end{pagefigure}

\cref{fig:fraction_2D} shows the fraction before fitting. The colored lines correspond to the colored points on the upper sub-figures of \cref{fig:fraction_of_gamma_factor}. $p_\mathrm{HT}^{e,\mu}(\gamma,occ)$ is derived from these. The fast $\gamma$ and slow occupancy dependency is also seen in this figure.

\cref{fig:fraction_2D_after_fit} shows the fraction predicted by the fit. The outlined area in red dashes shows the area that was used as input for the fit. The fit is also applied to the outside of the fit area to show the behavior of the model on unseen data.

\cref{fig:fraction_2D_after_fit_ratio} shows the ratio of the fraction before and after the fit. In data, the deviation seems no larger than 10\% for the bulk with the edges being far off due to lower statistics (hence larger errors). \cref{sec:appendixZScores} shows figures of z-scores (significances) that take the uncertainties into account. They show no deviation in the edges.

\cref{fig:fraction_of_gamma_factor} shows the 2D histograms before and after the fit in slices of track occupancy and $\gamma$ factor, respectively. The upper sub-figures show $p_\textrm{HT}$ in occupancy slices of 10\% up to 60\%. The left points in the two sub-figures show the onset of TR production for muons. For the most part, the data points conform well to the dashed lines from the fit. The electrons on the right has less statistics but for 20-40\% occupancy, the points follow the dashed lines. The lower sub-figures show $p_\textrm{HT}$ as a function of occupancy. The fitted model is drawn for two choices of $\gamma$ that follow the electron and muon trends quite well. It is important to note that no selection on $\gamma$ is made, so the muons from the onset on the upper sub-figure all go into the lower sub-figures, and that the dashed line is not fitted to the points but drawn from the fitted model.

\cref{fig:fraction_2D} and \cref{fig:fraction_2D_after_fit_ratio} show a sudden, small jump in $p_\textrm{HT}$ in simulation down the middle of the left cluster. This phenomenon is pointed at in \cref{fig:fraction_of_gamma_factor}. It is also seen in some other gas/parts in simulation and real data, though most can be explained by statistical fluctuations. This systematic effect is not understood.

Overall, judging from the figures sliced in $\gamma$ and occupancy (\cref{fig:fraction_of_gamma_factor}), the model is quite well fitted to points with low uncertainty. The fits converge, so the model describes the behavior well. The only point of criticism would be that the model slightly overestimates the HT fraction for muons at $\gamma=10^3$ in simulation.

\section{Correction factors (CFs)}
\label{sec:CF}
The significant drivers of the PID tool are the $\gamma$ factor and the track occupancy. As the previous figures show, they model the overall behavior very well. However, corrections to the electron probabilities of the hits are needed. These correction factors (CFs) were factorized out of the likelihood function. To be able to do this, the assumption is that the CFs will modify ever so slightly (at most $\mathcal{O}(1)$) the electron probability of each hit.

The CFs are calculated as follows for each gas and part individually. The average HT fractions are filled into TProfiles\footnote{Histograms that show the average value per bin instead of the sum of values per bin; \url{https://root.cern.ch/doc/master/classTProfile.html}}, and the HT fractions as a function of SL, ZR, and TW are filled into each their own TProfiles as well. The CFs are scaled by $1/$avg (cf. \cref{eq:CF_def}) and then used in \cref{eq:pHT}. Only tracks with momenta up to $80$ GeV are used. The previous limit was $50$ GeV. This threshold is put in to limit the $\gamma$ dependency that would otherwise follow into the correction factors which are supposed to be independent (cf. \cref{eq:pHT}).

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[page=1,width=0.24\textwidth]{figures/trt/data/CFelectrons.pdf}
	\includegraphics[page=1,width=0.24\textwidth]{figures/trt/mc/CFelectrons.pdf}
	\includegraphics[page=1,width=0.24\textwidth]{figures/trt/data/CFmuons.pdf}
	\includegraphics[page=1,width=0.24\textwidth]{figures/trt/mc/CFmuons.pdf}
	\\
	\includegraphics[page=4,width=0.24\textwidth]{figures/trt/data/CFelectrons.pdf}
	\includegraphics[page=4,width=0.24\textwidth]{figures/trt/mc/CFelectrons.pdf}
	\includegraphics[page=4,width=0.24\textwidth]{figures/trt/data/CFmuons.pdf}
	\includegraphics[page=4,width=0.24\textwidth]{figures/trt/mc/CFmuons.pdf}
	\\
	\includegraphics[page=7,width=0.24\textwidth]{figures/trt/data/CFelectrons.pdf}
	\includegraphics[page=7,width=0.24\textwidth]{figures/trt/mc/CFelectrons.pdf}
	\includegraphics[page=7,width=0.24\textwidth]{figures/trt/data/CFmuons.pdf}
	\includegraphics[page=7,width=0.24\textwidth]{figures/trt/mc/CFmuons.pdf}
	\caption{Correction factor (first row SL, second row TW, and third row ZR) for the barrel. The first two columns are electrons and the last two columns are muons. Columns 1 and 3 are data and columns 2 and 4 are simulation.}
	\label{fig:CFs}
\end{figure*}

Figures for the three CFs are shown for the barrel in \cref{fig:CFs}. The remaining CFs for the endcaps are attached in \cref{sec:appendixCorrectionFactors}. The SL correction shows that the transition from short to long straws at ${SL=9}$ affects simulation much more than data. The TW correction also shows a great difference for muons between data and simulation at around $2.0$ mm. The ZR correction is mostly flat.

Raising the threshold to $80$ GeV will include the muons on the rising part of the onset function as seen in \cref{fig:app80GeVLines} in \cref{sec:appendixROCWith80GeVLines}. Analysis shows that the high momentum limit leads to little occupancy dependency of the CFs; \cref{sec:appendixCFcorr} shows the CFs for low, medium, and high occupancy (all with equal number of entries). All figures show little discrepancy with the exceptions being low statistics (especially for endcap B and argon in general) and CF(TW) for muons in the barrel (\cref{fig:appendixCFcorr_4}). The dependency on occupancy for CF(TW) for muons will lead to slightly sub-optimal performance. However, \cref{fig:rocsAllCFs_8} in \cref{sec:appendixROCForAdditionalCFs} shows that truncating TW at $2.0$ mm reduces performance.

$TW>2.0$ mm for a hit is unphysical, as the hit will be outside the straw tube. These unphysical hits may stem from uncertainty on the track position or from delta-rays knocked into the tube from nearby tracks.

The conclusion therefore is that, for optimal performance, all correction factors should be used and in the full range of values. The ROC curve in \cref{fig:rocs} in the next section will confirm this.

\section{Final performance}
\label{sec:final}
\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[page=7,width=0.48\textwidth]{figures/trt/data/rocs.pdf}
	\includegraphics[page=7,width=0.48\textwidth]{figures/trt/mc/rocs.pdf}\\
	\includegraphics[page=8,width=0.48\textwidth]{figures/trt/data/rocs.pdf}
	\includegraphics[page=8,width=0.48\textwidth]{figures/trt/mc/rocs.pdf}
	\caption{Upper: Electron and muon counts of electron probabilities for the previous and new tuning. Lower: ROC curves for the previous calibration as well as the new calibration with different choices of CF combinations. This figure is for the whole detector and not just xenon straws in the barrel. See \cref{sec:appendixCalibResultsAllGasesAllParts} for ROC curves for the different parts and gases. The background efficiency reduction at 95\% signal efficiency for the new calibration with all CFs compared to the old calibration is shown in the figure as percentage points and percentage.}
	\label{fig:rocs}
\end{figure*}
\noindent $p_\mathrm{HT}(\gamma, occ)$ and the CFs have been calculated. \cref{fig:fraction_of_gamma_factor} shows the tuning without correction factors. The tool is applied back on the same sample from which the tuning is derived.

The probabilities given by the tool (using all CFs) are in the two upper sub-figures of \cref{fig:rocs} along with the predictions of the previous tuning in dashed lines. The upper left sub-figure shows the count of electrons in data now correctly going down at very low electron probability. The upper right sub-figure also shows the muons in simulation now correctly going down at very high electron probability.

From the probabilities, ROC curves have been calculated for a few CF combinations. These are shown in the lower sub-figures of \cref{fig:rocs}. A zoomed-in window is shown to help tell the slight improvement given by the incrementally added CFs. See \cref{sec:appendixCalibResultsAllGasesAllPartsROCCurves} for ROC curves for the different parts and gases. For more combinations of CFs, see \cref{sec:appendixROCForAdditionalCFs}. As shown in the figure, with all CFs, the new calibration reduces the background efficiency by 1.45 percentage points (3.38\%) in data and 3.31 p.p. (6.22\%) in simulation at 95\% signal efficiency.

The significant increase in performance in simulation is not due to the additional CFs (cf. \cref{sec:appendixProbAllCFs}). Rather, the performance was previously heavily degraded by the rising muon count at high electron probability, which lead to a worse background reduction at high signal efficiency in the ROC curves. The new calibration does not have this issue.

The calculation of $p_\mathrm{HT}$ (the fraction) is done by dividing all HT hits by all LT hits per bin. Another approach is to divide the HT hits by LT hits for each track, and then taking the average for each track in each bin. Performing this tuning on fractions derived per track shows very similar performance for gas/parts with great amount of statistics but leads to more noisy data when very little statistics is available. See \cref{sec:appendixtrackbasedpHT} for an in-depth explanation and figures.

\section{Additional analysis}
%\emph{
%	\small
	This section will summarize different concerns regarding the calibration and provide additional figures.
%}
%\minitoc
\label{sec:additionalanalysis}

\subsection{ROC curves for different occupancies}
\label{sec:ROCforDifferentOcc}
\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[page=2,width=0.48\textwidth]{figures/trt/data/rocsDifferentOccs.pdf}
	\includegraphics[page=2,width=0.48\textwidth]{figures/trt/mc/rocsDifferentOccs.pdf}\\
	\includegraphics[page=8,width=0.48\textwidth]{figures/trt/data/rocsDifferentOccs.pdf}
	\includegraphics[page=8,width=0.48\textwidth]{figures/trt/mc/rocsDifferentOccs.pdf}
	\caption{ROC curves for the previous calibration as well as the new calibration with all CFs. The new calibration is also shown in three approximately equal occupancy slices. As expected, lower occupancy leads to a greater performance. The upper sub-figure is for the barrel only, while the lower sub-figure is for the full detector.}
	\label{fig:rocsDifferentOccs}
\end{figure*}
\noindent The performance of the PID as shown in \cref{fig:rocs} is for all occupancies. It is known that higher occupancy gives a higher baseline HT fraction and thus lower performance. In \cref{fig:rocsDifferentOccs}, the calibrated PID is applied to three occupancy slices: $occ<24$\%, $24<occ<32$\%, and $occ>32$\%. The three occupancy slices are chosen such that they each contain approximately one third of the entries in data. When more statistics at higher pileup is available, these ranges can be increased. See \cref{fig:nLTvsEtaPrTrk} in \cref{sec:appendixCalibResultsAllGasesAllPartsROCCurves} for the reason behind the $\eta$ slices used in the ROC curves. This analysis can be used to predict the PID performance as a function of increasing occupancy.

%\clearpage
\subsection{Applying simulation-tuned calibration on data}
\label{sec:MCondata}
\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[page=2,width=0.48\textwidth]{figures/trt/data/rocs.pdf}
	\includegraphics[page=2,width=0.48\textwidth]{figures/trt/mcondata/rocs.pdf}\\
	\includegraphics[page=8,width=0.48\textwidth]{figures/trt/data/rocs.pdf}
	\includegraphics[page=8,width=0.48\textwidth]{figures/trt/mcondata/rocs.pdf}
	\caption{ROC curves for the new calibration tuned in data and applied on data (left, for reference) and tuned in simulation and applied on data (right). Even with the additional CFs, tuning in simulation leads to performance worse than the previous calibration and the data-tuned calibration.}
	\label{fig:rocsForMCOnData}
\end{figure*}
\noindent The previous calibration was made entirely in simulation and applied on both simulated and real data samples. To see the performance difference between tuning in simulation and applying on data against tuning in data and applying on data, the simulation-tuned calibration was applied on the data samples from which the ROC curves in \cref{fig:rocsForMCOnData} have been derived. See \cref{sec:appendixCalibResultsMCOnData} for ratios of the 2D histograms before and after fitting.