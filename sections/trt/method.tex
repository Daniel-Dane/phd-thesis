\chapter{Selections}
\emph{\small
	In this section, the event pre-selection and lepton selections will be outlined, including the cut-flow diagrams. Afterwards, the output histograms and variables will be explained.
}
\minitoc
\label{sec:methods}
%\section{Samples}
\label{sec:samples}

~\\\noindent
%The calibration needs unbiased and uncalibrated electrons, since the tool will be applied early on in the reconstruction stage. To accomplish this, 
The TRT group produces their own samples, listed below for completeness\footnote{The "trt" tags are documented internally, \url{https://twiki.cern.ch/twiki/bin/view/Atlas/TrtxAOD}.}:

\begin{fullwidth}
\footnotesize
group.det-indet.00303832.physics\_Main.daq.TRTxAOD\_Z.f716\_trt098-00\_EXT0 \\\noindent
group.det-indet.00304178.physics\_Main.daq.TRTxAOD\_Z.f716\_trt098-01\_EXT0 \\\noindent
group.det-indet.00304337.physics\_Main.daq.TRTxAOD\_f716\_Ztnp3\_EXT0 \\\noindent
group.det-indet.361106.PowhegPythia8EvtGen\_AZNLOCTEQ6L1\_Zee.recon.TRTxAOD.e3601\_s2876\_r7886\_trt093-00\_EXT0 \\\noindent
group.det-indet.361107.PowhegPythia8EvtGen\_AZNLOCTEQ6L1\_Zmumu.recon.TRTxAOD.e3601\_s2876\_r7886\_trt093-04\_EXT0
\end{fullwidth}
%
%Data samples:
%\begin{verbatim}
%
%\end{verbatim}
%
%Simulation samples:
%{
%\scriptsize
%\begin{verbatim}
%
%\end{verbatim}
%}

\section{Event and lepton selections}
\label{sec:selections}
The selections are split into event pre-selections (event-level selections) and lepton selections (selections on the leptons being selected for). For any electron pair passing the final selection, the probe is kept. For any muon pair passing the final selection, both are kept. All combinations of pairs are attempted, and thus pairs with switched labels are also accepted if they pass the selections. Even when a pair has been accepted in the event, new pairs of (other) leptons are still accepted after this, also with switched labels.

The pre-selection is listed in \cref{tab:eventsel}, and the electron and muon selections are listed in \cref{tab:leptonsel}. Some of the selections will be detailed in the paragraphs below. Numbers for the selections in the tables can be seen in \cref{tab:cutflow}, which uses shorter names for the selections.% The short names match the labels in \cref{fig:ElCutflow} and \cref{fig:MuCutflow}.

\subsection{Event pre-selections}
\begin{table}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\begin{tabular}{l}
			\toprule
			Remove duplicated events \\
			{[Data] Event cleaning} \\
			{[Data] Pass GRL} \\
			Any \emph{lepton} trigger passed$^1$ \\
			At least two \emph{leptons} in the event \\
			\bottomrule
		\end{tabular}
	\end{center}
	\caption{Event pre-selections. The \emph{lepton} corresponds to the lepton being selected for, not both. The selections are detailed in the text. $^1$Trigger selection is not applied for simulated data because of reconstruction bugs at the time of sample production.}
	\label{tab:eventsel}
\end{table}

The event pre-selections are listed in \cref{tab:eventsel}. The selection skips new events if a previous event with the same event number has already been processed. Sometimes in the past, events were duplicated in production, so this selection ensures that duplicates are not used (in the case that the duplicated events can be identified by their event number). Event cleaning refers to the usual veto on events due to problematic regions of the detector or incomplete events (cf.~\cref{sec:rec:eventquality}). The good runs list for the 2016 data is,
data16\_13TeV.periodAllYear\_DetStatus-v88-pro20-21\_DQDefects-00-02-04\_PHYS\_StandardGRL\_All\_Good\_25ns.xml.

\begin{margintable}
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\scriptsize
		\begin{tabularx}{\linewidth}{l}
			\toprule
			\multicolumn{1}{c}{electron channel} \\
			HLT\_e24\_lhmedium\_L1EM18VH \\
			HLT\_e24\_lhmedium\_L1EM20VH \\
			HLT\_e60\_lhmedium \\
			HLT\_e120\_lhloose \\
			HLT\_e5\_lhvloose \\
			HLT\_e12\_lhvloose\_L1EM10VH \\
			HLT\_e12\_lhvloose\_nod0\_L1EM10VH \\
			HLT\_e15\_lhvloose\_L1EM7 \\
			HLT\_e20\_lhvloose \\
			HLT\_e20\_lhvloose\_L1EM12 \\
			HLT\_e20\_lhvloose\_nod0 \\
			HLT\_e24\_lhmedium\_nod0\_L1EM20VH \\
			HLT\_e24\_lhtight\_nod0\_ivarloose \\
			HLT\_e26\_lhtight\_nod0\_ivarloose \\
			HLT\_e26\_lhtight\_smooth\_ivarloose \\
			HLT\_e28\_lhmedium\_nod0\_L1EM20VH \\
			HLT\_e28\_lhtight\_nod0\_ivarloose \\
			HLT\_e28\_lhtight\_smooth\_ivarloose \\
			HLT\_e50\_lhvloose\_L1EM15 \\
			HLT\_e60\_medium \\
			HLT\_e60\_lhmedium\_nod0 \\
			HLT\_e140\_lhloose\_nod0 \\
			HLT\_e300\_etcut \\
			~ \\
			\multicolumn{1}{c}{muon channel} \\
			HLT\_mu26\_imedium \\
			HLT\_mu26\_ivarmedium \\
			HLT\_mu28\_imedium \\
			HLT\_mu28\_ivarmedium \\
			HLT\_mu40 \\
			HLT\_mu50 \\ \bottomrule 
		\end{tabularx}
		\caption{The single lepton triggers that have been used for the calibration. See \cref{sec:reconstruction} for the naming convention.}
		\label{tab:selections:triggers}
	\end{center}
\end{margintable}

Finally, all triggers for the lepton being selected for are checked. \cref{tab:selections:triggers} contains all regular electron triggers (even prescaled to increase electron count) and all unprescaled muon triggers for the years 2015 and 2016. If any trigger passes, the event is pre-selected, given that the event contains at least two of the leptons being selected for. For simulated data, no trigger selection was applied due to bugs in the sample production.

\subsection{Lepton selections}
With the pre-selection passed, all the leptons of a given type are paired in all combinations. For each pair, one candidate is tested as the tag and the other as the probe. As soon as the tag or the probe fails a selection, the next pair is tested. The selections in \cref{tab:leptonsel} are detailed below.

Truth-matching of electrons is done by using the truth information supplied by an algorithm during reconstruction. Prompt, FSR and bremsstrahlung electrons are all accepted. Due to a bug in the sample, no truth-matching is done for muons.

The $p_\mathrm{T}$ selections are set to $5$ GeV for electrons and muons in order to keep efficiency high\footnote{The ATLAS Electron and Photon Performance Group uses $25$ GeV and $15$ GeV for tags and probes, respectively, in their tag and probe framework. Their framework was the baseline of this work.}. The invariant mass selection has an indirect selection on the momentum that anyway indirectly raises the lower momentum limit. The $p_\mathrm{T,trk}>0.25\cdot \pT$ selection ensures that the tracks of electrons passing with minimum \pT still have a decent $p_\mathrm{T,trk}$.

The silicon hits are the sum of Pixel and SCT hits. All hits include dead sensors.

The LLH ID includes all variables for electron PID, while the "cut-based" ID has fewer variables and looser selections for looser PID. The "cut-based" loose PID does not include TRT information.

The electron probe ID and isolation selections at the bottom of the table are additional selections compared to the selections in the framework by the ATLAS Electron and Photon Performance Group to further clean the electron sample.

%Numbers for the selections in \cref{tab:leptonsel} can be seen in \cref{tab:cutflow1,tab:cutflow2}, which use shorter names for the selections. The short names match the labels in \cref{fig:ElCutflow} and \cref{fig:MuCutflow}.

\begin{table*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\begin{tabular}{ll}
			\toprule
			\textbf{Electron channel}                                                             & \textbf{Muon channel}                                                                  \\ \midrule
			{[}Simulation{]} Both truth-matched                                                   & {[}Simulation{]} Both truth-matched$^1$                                                \\
			$80 < m_{ee} < 100$ GeV                                                               & Both are combined muons with $\ge 2$ precision hits                                    \\
			Both pass author                                                                      & $80 < m_{\mu\mu} < 100$ GeV                                                            \\
			Tag $p_\mathrm{T}>5$ GeV                                                              & At least one match trigger object                                                      \\
			Probe $p_\mathrm{T}>5$ GeV                                                            & Both $\eta<2.0$                                                                        \\
			Probe $p_\mathrm{T,trk}>0.25\cdot p_\mathrm{T}$                                       & Both $p_\mathrm{T}>5$ GeV                                                              \\
			Both $\eta<2.0$                                                                       & Both $\mathrm{hits_{pixel}}>=1,\ \mathrm{hits_{silicon}}>=7,\ \mathrm{hits_{TRT}>=15}$ \\
			Both pass OQ                                                                          & Both pass tight track iso, $p_\mathrm{T}^\mathrm{varcone30}/p_\mathrm{T} < 0.06$       \\
			Tag match trigger object                                                              & Both pass tight calo iso, $E_\mathrm{T}^\mathrm{topocone20}/p_\mathrm{T} < 0.06$       \\
			Tag pass tight track iso, $p_\mathrm{T}^\mathrm{cone20}/p_\mathrm{T} < 0.06$          & Opposite sign (OS)                                                                     \\
			Tag pass tight calo iso, $E_\mathrm{T}^\mathrm{topocone20}/p_\mathrm{T} < 0.06$       &                                                                                        \\
			Tag $\mathrm{hits_{pixel}}>=1,\ \mathrm{hits_{silicon}}>=7,\ \mathrm{hits_{TRT}>=15}$ &                                                                                        \\
			Tag pass tight LLH ID                                                                 &                                                                                        \\
			Opposite sign (OS)                                                                    &                                                                                        \\
			Probe pass same hits and non-LLH loose ID                                             &                                                                                        \\
			Probe tight track iso                                                                 &                                                                                        \\
			Probe tight calo iso                                                                  &                                                                                        \\ \bottomrule
		\end{tabular}
	\end{center}
	\caption{Lepton selections for electrons (left) and muons (right) in the order that they are applied. Some selections are detailed in the text. $^1$Muon truth-matching is not applied because of reconstruction bugs at the time of sample production.}
	\label{tab:leptonsel}
\end{table*}

\subsection{Cut-flow for both selections}
The cut-flow diagrams can be found in \cref{fig:Cutflow}. For both figures, the red bins show the number of \emph{events} passing the \emph{event pre-selection}, and blue bins show the number of \emph{leptons} passing the \emph{lepton selection}. Note that the "InitialElectronZ" and "InitialMuonZ" show every pair before any selection. They therefore get a contribution of $n(n-1)$ per event with $n$ being the number of leptons. Since the label switch is allowed, all electrons are tried as both tags and probes. If the switch was not allowed, the contribution would be $n(n-1)/2$. Since these are mostly low \pT pileup, the first selection will clear out the majority of the lepton pairs. The raw numbers are given in \cref{tab:cutflow}.

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.45\textwidth]{figures/trt/data/cutflowZee.pdf}
	\includegraphics[width=0.45\textwidth]{figures/trt/mc/cutflowZee.pdf}
	\includegraphics[width=0.45\textwidth]{figures/trt/data/cutflowZmm.pdf}
	\includegraphics[width=0.45\textwidth]{figures/trt/mc/cutflowZmm.pdf}
	\caption{Cut-flow diagram for selections. The red histogram is the number of events passing the event pre-selection and the blue is the number of leptons passing their respective selections.}
	\label{fig:Cutflow}
%	\label{fig:ElCutflow}
%	\label{fig:MuCutflow}
\end{figure*}

%\begin{figure*}[htbp]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
%
%	\caption{Cut-flow diagram for muon selection. The red histogram is the number of events passing the event pre-selection and the blue is the number of muons passing the muon selection.}
%
%\end{figure*}

\begin{pagetable}%[H]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\renewcommand{\arraystretch}{1.2}%
	\small
	\begin{center}
%		\captionsetup[subtable]{position = below}%
%		\captionsetup[table]{position=top}%
		\subfloat[Electrons, data]{%{0.3\linewidth}%
	%		\centering%
			\begin{tabular}{llll}
				\toprule
	%			\multicolumn{4}{c}{Data, electrons}   \\
				Cut                                      & Probes   & Eff.   & Acc. eff. \\ \midrule
				InitialEvent                             & 1953402  &        &           \\
				RemoveDuplicates                         & 1807815  &        &           \\
				EventCleaning                            & 1806534  &        &           \\
				GRL                                      & 1806360  &        &           \\
				TriggerFired                             & 725917   &        &           \\
				EnoughLeptons                            & 721161   &        &           \\[0.5ex]\hdashline\noalign{\vskip 1ex}
				InitialElectronZ                         & 15994269 &        &           \\
				-                                                   &        &           \\
				MassWindow                               & 789362   &        &           \\
				BothAuthor                               & 789362   & 100.00 & 100.00    \\
				TagPt                                    & 748174   & 94.78  & 94.78     \\
				ProbePt                                  & 714111   & 95.45  & 90.47     \\
				ProbeAtLeastRelPt                        & 607149   & 85.02  & 76.92     \\
				TagEta                                   & 510486   & 84.08  & 64.67     \\
				ProbeEta                                 & 432711   & 84.76  & 54.82     \\
				BothOQ                                   & 430250   & 99.43  & 54.51     \\
				TagTrigger                               & 255597   & 59.41  & 32.38     \\
				TagTrkIso                                & 240406   & 94.06  & 30.46     \\
				TagCaloIso                               & 224465   & 93.37  & 28.44     \\
				TagTrk                                   & 193578   & 86.24  & 24.52     \\
				TagIDTight                               & 178654   & 92.29  & 22.63     \\
				OS                                       & 158166   & 88.53  & 20.04     \\
				ProbeQuality                             & 110369   & 69.78  & 13.98     \\
				ProbeTrkIso                              & 107763   & 97.64  & 13.65     \\
				ProbeCaloIso                             & 100383   & 93.15  & 12.72     \\ \bottomrule
				%
			%\caption{electrons in data}         &          &
			\end{tabular}%
		}%
		\,%
		\subfloat[Electrons, simulation]{%{0.3\linewidth}
	%		\centering%
			\begin{tabular}{llll}
				\toprule
	%			\multicolumn{4}{|c|}{Simulation, electrons} &  \\
				Cut                                               & Probes  & Eff.   & Acc. eff. \\ \midrule
				InitialEvent                                      & 603426  &        &           \\
				RemoveDuplicates                                  & 603426  &        &           \\
				-                                                 &         &        &           \\
				-                                                 &         &        &           \\
				-                                                 &         &        &           \\
				EnoughLeptons                                     & 530642  &        &           \\[0.5ex]\hdashline\noalign{\vskip 1ex}
				InitialElectronZ                                  & 5555704 &        &           \\
				BothTruthmatching                                 & 649596  &        &           \\
				MassWindow                                        & 399638  &        &           \\
				BothAuthor                                        & 399638  & 100.00 & 100.00    \\
				TagPt                                             & 399491  & 99.96  & 99.96     \\
				ProbePt                                           & 399344  & 99.96  & 99.93     \\
				ProbeAtLeastRelPt                                 & 361351  & 90.49  & 90.42     \\
				TagEta                                            & 307853  & 85.20  & 77.03     \\
				ProbeEta                                          & 269437  & 87.52  & 67.42     \\
				BothOQ                                            & 267735  & 99.37  & 66.99     \\
				TagTrigger                                        & 267735  & 100.00 & 66.99     \\
				TagTrkIso                                         & 261204  & 97.56  & 65.36     \\
				TagCaloIso                                        & 223747  & 85.66  & 55.99     \\
				TagTrk                                            & 187276  & 83.70  & 46.86     \\
				TagIDTight                                        & 140981  & 75.28  & 35.28     \\
				OS                                                & 139867  & 99.21  & 35.00     \\
				ProbeQuality                                      & 110974  & 79.34  & 27.77     \\
				ProbeTrkIso                                       & 108588  & 97.85  & 27.17     \\
				ProbeCaloIso                                      & 95524   & 87.97  & 23.90     \\ \bottomrule
				%
			%\caption{electrons in data}                  &
			\end{tabular}%
		}%
		\\[25pt]
		\subfloat[Muons, data]{%{0.3\linewidth}%
		%		\centering%
		\begin{tabular}{llll}
			\toprule
			%			\multicolumn{4}{|c|}{Data, muons} &  \\
			Cut                                    & Probes  & Eff.  & Acc. eff. \\ \midrule
			InitialEvent                           & 1953402 &       &           \\
			RemoveDuplicates                       & 1807815 &       &           \\
			EventCleaning                          & 1806534 &       &           \\
			GRL                                    & 1806360 &       &           \\
			TriggerFired                           & 426292  &       &           \\
			EnoughLeptons                          & 425775  &       &           \\[0.5ex]\hdashline\noalign{\vskip 1ex}
			InitialMuonZ                           & 2186928 &       &           \\
			quality                                & 553615  &       &           \\
			MassWindow                             & 383286  &       &           \\
			trigger                                & 382925  & 99.91 & 99.91     \\
			eta                                    & 286866  & 74.91 & 74.84     \\
			pt                                     & 286463  & 99.86 & 74.74     \\
			trackQuality                           & 279798  & 97.67 & 73.00     \\
			MuonTrkIso                             & 270278  & 96.60 & 70.52     \\
			MuonCaloIso                            & 250736  & 92.77 & 65.42     \\
			OS                                     & 250292  & 99.82 & 65.30     \\ \bottomrule
		\end{tabular}%
		}%
		\,%
		\subfloat[Muons, simulation]{%{0.3\linewidth}
			%		\centering%
			\begin{tabular}{llll}
				\toprule
				%			\multicolumn{4}{|c|}{Simulation, muons} &  \\
				Cut                                          & Probes  & Eff.   & Acc. eff. \\ \midrule
				InitialEvent                                 & 511614  &        &           \\
				RemoveDuplicates                             & 511614  &        &           \\
				-                                                 &         &        &           \\
				-                                                 &         &        &           \\
				-                                                 &         &        &           \\
				EnoughLeptons                                & 430469  &        &           \\[0.5ex]\hdashline\noalign{\vskip 1ex}
				InitialMuonZ                                 & 4810508 &        &           \\
				quality                                      & 698389  &        &           \\
				MassWindow                                   & 463507  &        &           \\
				trigger                                      & 463507  & 100.00 & 100.00    \\
				eta                                          & 335984  & 72.49  & 72.49     \\
				pt                                           & 335554  & 99.87  & 72.39     \\
				trackQuality                                 & 325725  & 97.07  & 70.27     \\
				MuonTrkIso                                   & 309651  & 95.07  & 66.81     \\
				MuonCaloIso                                  & 285078  & 92.06  & 61.50     \\
				OS                                           & 284937  & 99.95  & 61.47     \\ \bottomrule
			\end{tabular}%
		}%
	\end{center}
	\caption{Cut-flow table for (a) electrons in data, (b) electrons in simulation, (c) muons in data, and (d) muons in simulation. The efficiency of a selection is only given for particles passing all previous selections. The dashed lines separate the event and lepton selections.}%
	\label{tab:cutflow}
\end{pagetable}%

%\begin{table*}%[H]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
%	\captionsetup[subtable]{position = below}%
%	\captionsetup[table]{position=top}%
%	\subfloat[Data]{%{0.3\linewidth}%
%%		\centering%
%		\begin{tabular}{llll}
%			\toprule
%%			\multicolumn{4}{|c|}{Data, muons} &  \\
%			Cut                                    & Probes  & Eff.  & Acc. eff. \\ \midrule
%			InitialEvent                           & 1953402 &       &           \\
%			RemoveDuplicates                       & 1807815 &       &           \\
%			EventCleaning                          & 1806534 &       &           \\
%			GRL                                    & 1806360 &       &           \\
%			TriggerFired                           & 426292  &       &           \\
%			EnoughLeptons                          & 425775  &       &           \\[0.5ex]\hdashline\noalign{\vskip 1ex}
%			InitialMuonZ                           & 2186928 &       &           \\
%			quality                                & 553615  &       &           \\
%			MassWindow                             & 383286  &       &           \\
%			trigger                                & 382925  & 99.91 & 99.91     \\
%			eta                                    & 286866  & 74.91 & 74.84     \\
%			pt                                     & 286463  & 99.86 & 74.74     \\
%			trackQuality                           & 279798  & 97.67 & 73.00     \\
%			MuonTrkIso                             & 270278  & 96.60 & 70.52     \\
%			MuonCaloIso                            & 250736  & 92.77 & 65.42     \\
%			OS                                     & 250292  & 99.82 & 65.30     \\ \bottomrule
%		\end{tabular}%
%	}%
%	\,%
%	\subfloat[Simulation]{%{0.3\linewidth}
%%		\centering%
%		\begin{tabular}{llll}
%			\toprule
%%			\multicolumn{4}{|c|}{Simulation, muons} &  \\
%			Cut                                          & Probes  & Eff.   & Acc. eff. \\ \midrule
%			InitialEvent                                 & 511614  &        &           \\
%			RemoveDuplicates                             & 511614  &        &           \\
%			-                                                 &         &        &           \\
%			-                                                 &         &        &           \\
%			-                                                 &         &        &           \\
%			EnoughLeptons                                & 430469  &        &           \\[0.5ex]\hdashline\noalign{\vskip 1ex}
%			InitialMuonZ                                 & 4810508 &        &           \\
%			quality                                      & 698389  &        &           \\
%			MassWindow                                   & 463507  &        &           \\
%			trigger                                      & 463507  & 100.00 & 100.00    \\
%			eta                                          & 335984  & 72.49  & 72.49     \\
%			pt                                           & 335554  & 99.87  & 72.39     \\
%			trackQuality                                 & 325725  & 97.07  & 70.27     \\
%			MuonTrkIso                                   & 309651  & 95.07  & 66.81     \\
%			MuonCaloIso                                  & 285078  & 92.06  & 61.50     \\
%			OS                                           & 284937  & 99.95  & 61.47     \\ \bottomrule
%		\end{tabular}%
%	}%
%	\caption{Cut-flow table for (a) data and (b) simulation. The efficiency of a selection is only given for particles passing all previous selections. The dashed lines separate the event and lepton selections.}%
%	\label{tab:cutflow2}
%\end{table*}%

% After selection, there are X (Y) electron probes in data (simulation) and X (Y) muons in data (simulation), respectively. The low electron efficiency is due to harsh cuts that are meant to give high electron purity.

\FloatBarrier
\section{Preparation prior to analysis}
\label{sec:preanalysis}
%After the selections, histograms and the ntuple tree are filled.

The usual event information and particle kinematics are saved in the output, including the following used for recalculating the probabilities with the new tuning. The probe track variables are: trackP, trackEta, trackOccupancy, electronProbabilityAth. The probe track hit variables are: HighThreshold, GasType, TRTPart, StrawLayer, TrackToWire, ZToR. \emph{electronProbabilityAth} is the TRT electron probability using the current calibration. This variable is shown in some figures as 'previous calibration based on 2015 Simulation'.

2D Histograms, with $\gamma$ factor and track occupancy as axes, are filled for each gas and TRT part. All hits are filled in the LT histogram, while only HT are filled into HT histograms. To calculate $p_\mathrm{HT}$, the two histograms are divided. The average and per SL, TW, and ZR values of $p_\mathrm{HT}$ are also saved, from which the correction factors are derived. Finally, a few histograms for the appendix are also made. These include histograms with the HT fraction calculated per track and the correction factors as a function of occupancy.

The uncertainty of the fraction is recalculated for data. The binomial uncertainty for fractions is used:
\begin{equation}
	\sigma^2 = f(1-f)/n_\mathrm{LT}, \quad f=n_\mathrm{HT} / n_\mathrm{LT}.
\end{equation}

Bins in the 2D fraction histogram with fewer than 20 HT hits are removed. The simulation weights ("MC weights" given by the generators) have been normalized in the very beginning due to being very high. Now, bins with few hits and no hits are weight-wise closer. Events with negative weights are skipped to ease the computation of some histograms. The amount of simulation with negative weights is approximately 2 orders magnitudes smaller.% The weights, after normalization, can be seen in \cref{fig:Weights}.

Following these selections, approximately 1.6 electron probes are saved per event when at least one electron probe passes. See \cref{fig:ProbesPerOcc}. In data, much more so than in simulation, the occupancy (and hence the pileup) has a great influence on the selection efficiency of electrons (see \cref{sec:trttheory} for definitions of the different occupancies). The stark difference between data and simulation makes data-driven calibration important for optimal performance.

%When the samples have been processed, the histograms are reprocessed from the ntuples. The reprocessing adds the following steps which are not included in the initial processing:
%\begin{itemize}
%	\item Events with negative weights are removed. These events amount to approximately $1.5$\% of the total number of events.
%	\item Simulation weights are normalized to 1 by dividing them with 1941.
%\end{itemize}
%
%In order to asses the weights, these events must be included in the ntuples.

% \begin{figure}[htbp]
% 	\begin{center}
% 		\includegraphics[page=1,width=0.48\textwidth]{figures/trt/data/weights.pdf}
% 		\includegraphics[page=1,width=0.48\textwidth]{figures/trt/mc/weights.pdf}\\
% 		\includegraphics[page=2,width=0.48\textwidth]{figures/trt/data/weights.pdf}
% 		\includegraphics[page=2,width=0.48\textwidth]{figures/trt/mc/weights.pdf}
% 		\caption{Event weights for real and simulated data. No limits are imposed on the axes. The simulation weights have been re-scaled by dividing by $1941$. As seen in the figure, this leads to a mean of approximately 1.} % Negative weights are multiplied by $-1$ and shown in red.
% 		\label{fig:Weights}
% 	\end{center}
% \end{figure}

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.48\textwidth]{figures/trt/data/probesPerOcc.pdf}
	\includegraphics[width=0.48\textwidth]{figures/trt/mc/probesPerOcc.pdf}
	\caption{For events with at least one electron probe passing, the average number of selected electron probes per event is plotted against the global occupancy (black), the smaller track occupancy of the one or two electron probe(s) selected per any given event (blue), and the greater track occupancy of them (red).}
	\label{fig:ProbesPerOcc}
\end{figure*}
