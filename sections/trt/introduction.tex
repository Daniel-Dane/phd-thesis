\chapter{Analysis overview}
\emph{\small
	We will obtain an overview of the calibration of the TRT PID from the theoretical to the technical aspect. The particle identification section is taken from Ref. \cite{Vasquez:2104319}.
}
\minitoc
\label{sec:trttheory}
\label{sec:intro:trt}
The performance of particle identification (PID) using the TRT is measured using LHC data recorded by ATLAS during 2016 as well as simulated data. Calibrating in data allows for the full use of the correction factors used by the TRT's electron PID tool. Calibration of the PID tool using data is feasible due to the tag and probe (T\&P) method, which provides \emph{unbiased} and reasonably pure electron candidates as input for the calibration. T\&P combines one tightly identified electron (which shall be called the tag) with one electron candidate for which no \emph{TRT-provided} ID selection is applied (called the probe). Finally, the method requires that the invariant mass of the pair is close to the Z boson mass. Since the tool to be calibrated has not been used to select the probe, the selection is said to be unbiased, and the probe is then used in the calibration. %The procedure is schematized in \cref{fig:TNP_drawing}. 
For background (non-electrons), muons are used because their mass are similar to that of pions, and they leave similar tracks in the TRT\footnote{Any effect from hadronic interactions between actual non-electrons such as pions, kaons, protons, etc. and the TRT is assumed to be minor.}. Specifically, muons from Z decays are used as they have comparable kinematics to the selected electrons and can be similarly identified using the $Z$ mass constraint.

%\begin{figure}[htbp!]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
%	\includegraphics[width=1.0\textwidth]{figures/trt/TNP.pdf}
%	\caption{Tag and probe in a nutshell. An electron pair whose invariant mass is approximately the Z mass is found. One electron is tagged by applying a tight identification requirement on it. With $m_{ee}\approx m_Z$ and tight ID requirement on the tag, the other electron candidate, the probe, is now presumed to be an electron. TODO: This figure is borrowed from an egamma presentation: Remake.}
%	\label{fig:TNP_drawing}
%\end{figure}

%While the time-over-threshold (ToT) is computable and improves the identification performance, it is as of yet not mature for analyses. Therefore, only transition radiation (TR) is used in this analysis.

This analysis is separated into the following chapters, whose contents are shortly listed as well:
\begin{myitemize}%[leftmargin=0pt,listparindent=0pt,labelwidth=0pt,itemindent=0pt]
	\item[\textbf{\nameref*{sec:methods}:}] An initial event pre-selection is applied; electron and muon pairs are separately identified, and all electron probes per event are saved; finally, some preprocessing is done.
	\item[\textbf{\nameref*{sec:calib}:}] The probes are filled into 2D histograms in bins of $\gamma$ and occupancy with the HT probability on the $z$-axis; they are fitted using the expression later in this section, correction factors (explained in this section) are calculated, and the performance is compared to the previous calibration using ROC curves; some further checks are done, including testing the occupancy-independence of the correction factors.
	\item[\textbf{\nameref*{sec:trt:discussion}:}] Several issues discovered during the calibration effort are discussed.
\end{myitemize}

Activity in the vicinity of the straws is known as track occupancy (or sometimes wrongly known as local occupancy, which is not the same). The global occupancy is an absolute measure of activity in the whole TRT. The global occupancy is roughly defined as the number of hit TRT straws out of the total number of TRT straws. The local occupancy is independently calculated in the 192 $\phi$ slices of the TRT. The track occupancy is a weighted average of the local occupancies from the $\phi$ slices that the track crosses; each slice is weighted by the ratio of hits of the track in that region to the total number of hits that make up the track. See Ref.~\cite{Schaefer:2128630} (especially Eqs. 1 and 2) for in-depth explanations of the different occupancy definitions.

\begin{figure}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.49\textwidth]{figures/trt/data/averagemuVsTrackocc.pdf}
	\includegraphics[width=0.49\textwidth]{figures/trt/mc/averagemuVsTrackocc.pdf}
	\caption{Track occupancy is plotted against $\left\langle\mu\right\rangle$ with data on the left and simulation on the right. 
		%		The high $\langle\mu\rangle$ region in the data figure is from the 2017 sample. 
		The 2D figure is for muons only with a profile along $\left\langle\mu\right\rangle$ of muons and electrons plotted on top. The muons in data are fitted with a second-order polynomial and drawn in yellow on simulation figure.}
	\label{fig:averagemu_vs_trackocc}
\end{figure}

The pileup $\left\langle\mu\right\rangle$ is related to the track occupancy through \cref{fig:averagemu_vs_trackocc}. As track occupancy began surpassing 50\% during data taking in 2017, it was crucial that the pileup-dependent behavior be well-modeled. The pileup and track occupancy of the available 2016 data as well as simulation is shown in the figure. The dashed, yellow line in the simulation sub-figure is fitted from the data on the data sub-figure. In simulation, the calculated track occupancy is consistently higher than in data. This discrepancy is not understood.

A second issue, which prompted this calibration, was that an increase in electron count at low electron probability was found. As will be shown later in \cref{fig:rocs}, the electron probabilities for both electrons and muons in data and simulation are well-behaved with the new calibration.
%The new calibration shows an increase in performance in data. It does, however, not show the increase in electron count for decreasing electron probability (\cref{fig:rocs}, red, dashed line in top-left sub-figure) which was an issue that also prompted this study.

%All code used in this analysis (T\&P as well as tuning) is available on gitlab. See \cref{sec:codelocation} for links.

\section{Particle identification}
\label{sec:PID}
Charged particles with very high $\gamma$ factors ($\gamma>1\,000$) will, with some probability, produce transition radiation (TR) when entering a medium of different dielectric permittivity. This medium is the passive radiative material in the TRT. Energetic electrons, due to their very low mass, will often be ultra-relativistic in ATLAS and therefore produce TR to a greater degree compared to heavier particles such as muons/pions (which both have similar masses approximately 200 times greater than electrons). The xenon gas in the TRT is especially sensitive to the energetic TR photons, which in turn will deposit a high amount of charge in the straw when hit, as was explained in \cref{sec:atlas:inner:trt}. Repeating the section a bit more, the TRT operates with two thresholds for charge deposition in straws. The low threshold (LT) is above noise and is sensitive to ionization deposits from passing charged particles, while the much higher high threshold (HT) is sensitive to larger deposits as well as TR. Therefore, the HT is the response to the combined effect of ionization and TR. Any track will on average produce approximately 35 LT hits in the TRT. Electrons have an approximately 30\% chance of producing an HT hit compared to non-electrons (e.g. muons/pions) at 10\%. For 35 hits, this discrepancy will give good separation power. The simple fraction $f_\mathrm{HT}$ was used for electron PID in the first half of Run 1:
\begin{align}
	f_\mathrm{HT} = \frac{n_\mathrm{HT}}{n_\mathrm{LT}},
\end{align}
where the number of HT hits are contained in the number of LT hits.

%The production of TR is strictly due to the $\gamma$ factor, but 
The HT count is influenced by the track occupancy. Thus, while the simple fraction is powerful, the lack of incorporating the occupancy as well as local corrections to HT hit probability will lead to sub-optimal performance. To improve on this, HT probabilities for electrons and non-electrons are individually constructed for single hits and combined into electron and non-electron likelihoods for a track,
\begin{align}
	\mathcal{L}^{e,\mu} = \prod_\textrm{TRT hits}
	\begin{cases}
		  p_\mathrm{HT}^{e,\mu} & \text{if HT hit} \\
		1-p_\mathrm{HT}^{e,\mu} & \text{else}        %
	\end{cases}
\end{align}

The electron probability for a track is found from the likelihood ratio,
\begin{align}
	\mathcal{P}^e=\dfrac{\mathcal{L}^{e}}{\mathcal{L}^{e}+\mathcal{L}^{\mu}}.
\end{align}

The hit probability depends mainly on the $\gamma$ factor and the track occupancy with small correction factors (CFs) factorized out:
\begin{align}
	p_\mathrm{HT}^{e,\mu} = p_\mathrm{HT}^{e,\mu}(\gamma,occ)\cdot CF(SL)\cdot CF(ZR)\cdot CF(TW).
	\label{eq:pHT}
\end{align}
Of course, the factorization assumes that the correction factors do not depend on $\gamma$ and occupancy, which will be shown to be correct for all cases except for TW>$1.8$~mm for muons in the barrel.

$CF(X)$ is computed by calculating the fraction of $p_\mathrm{HT}$ at a given $X=x$ to the average $p_\mathrm{HT}$:
\begin{align}
	CF(X=x) = \frac{p_\mathrm{HT,X}(x)}{p_\mathrm{HT,avg}}.
	\label{eq:CF_def}
\end{align}

The three correction factors are, respectively, the straw layer in the barrel/endcap, the Z distance from the xy-plane (barrel) or radial distance from the beam pipe (endcap), and the track-to-wire distance in millimeters (the straw tubes have a radius of approximately 2 mm). $p_\mathrm{HT}$ and the CFs are tuned in the barrel and two endcaps separately as well for tubes with xenon and argon, making a total of 6 $p_\mathrm{HT}^{e,\mu}$ models. $p_\mathrm{HT}$, as figures later will show, can be modeled by a sigmoid function in $\gamma$.
\newpage
The expression used for the fitting is as follows:
\begin{fullwidth}
\begin{align}
  p_\mathrm{HT}(\gamma) =
    \left\{
      \begin{array}{cl}
        p_2 + \frac{p_3}{1+\exp{(-(p_0-p_4)/p_5})} + \frac{p_3}{(1+\exp{(-(p_0-p_4)/p_5})^2)} \frac{\exp{(-(p_0-p_4)/p_5})}{p_5} (\log_{10}\gamma - p_0)
               &\mbox{if}~ \log_{10}\gamma < p_0\\
        p_2 + \frac{p_3}{1+\exp{(-(p_1-p_4)/p_5})} + \frac{p_3}{(1+\exp{(-(p_1-p_4)/p_5})^2)} \frac{\exp{-(p_1-p_4)/p_5}}{p_5} (\log_{10}\gamma - p_1)
               &\mbox{if}~ \log_{10}\gamma > p_1 \\
        p_2 + \frac{p_3}{1 + \exp{(-(\log_{10}\gamma-p_4)/p_5})}
               &\mbox{if}~ p_0 < \log_{10}\gamma < p_1
      \end{array}
    \right.
\end{align}
\end{fullwidth}

The first line describes the behavior at low $\gamma$ with no TR and is linear in ln$(\gamma)$. The second line describes the very high $\gamma$ particles with enough TR to be consistently above the HT and is also linear. The third line describes particles at the TR turn-on in the medium $\gamma$ regime and is modeled by a sigmoid. The rather long expressions for the linear parts are for continuity of the function and their first derivatives between the two points, $p_0$ and $p_1$. The equation describes the onset curve at zero occupancy. The dependency on occupancy can be modeled by a second order polynomial following the "Anatoli constraint". To model the occupancy dependency, the following substitutions must be made:
\begin{align}
	p_1 &\rightarrow p_1 + p_6 \cdot occ \\
	p_4 &\rightarrow p_4 + p_7 \cdot occ \\
	p_\mathrm{HT}(\gamma, occ) &= p_\mathrm{HT}(\gamma) + (1 - p_\mathrm{HT}(\gamma))(p_8\cdot occ + p_9 \cdot occ^2)
\end{align}
