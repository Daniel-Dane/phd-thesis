\section{Correction factors (CFs)}
\label{sec:appendixCorrectionFactors}
In this section, all three CFs (SL, ZR, TW) are plotted for electrons (top sub-figures) and muons (bottom sub-figures) with the usual data on the left and simulation on the right. The CFs are separated into barrel (main text) as well as endcap A and endcap B (this appendix) hits. The track-to-wire (TW) correction factor was capped at to $1.0$ for $TW \ge 2.0$ mm in the previous calibration. The reason for this has been uncertainty regarding the behavior in data, seeing as the radius of a TRT tube is $2.0$ mm. The rising CF for $TW>2.0$ mm is also seen in data, and the main text (also \cref{sec:appendixROCForAdditionalCFs}) has shown that not capping at $2.0$ mm improves performance.

\foreach \i in {2,3,5,6,8,9}{
	\begin{figure}[!htbp]
		\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
		\centering
		\includegraphics[page=\i,width=0.49\textwidth]{figures/trt/data/CFelectrons.pdf}
		\includegraphics[page=\i,width=0.49\textwidth]{figures/trt/mc/CFelectrons.pdf}\\
		\includegraphics[page=\i,width=0.49\textwidth]{figures/trt/data/CFmuons.pdf}
		\includegraphics[page=\i,width=0.49\textwidth]{figures/trt/mc/CFmuons.pdf}
		\caption{Correction factor (SL, TW, or ZR) for the endcap A or endcap B. Electrons are on the top sub-figures, while muons are on the bottom sub-figures.}
		\label{fig:CFs_\i}
	\end{figure}
}
