\chapter{Methods}
\emph{
	\small
	In this section, the following will be outlined: Used datasets, event pre-selection, and lepton selections. Afterwards, the output histograms and variables will be explained.
}
\minitoc
\label{sec:methods}

\section{Samples}
\label{sec:samples}
TODO: Expand on what this means. Move the table to appendix.

\begin{table*}[htbp]
	\centering
	\small
	\begin{tabular}{l}
		group.det-indet.00303832.physics\_Main.daq.TRTxAOD\_Z.f716\_trt098-00\_EXT0 \\
		group.det-indet.00304178.physics\_Main.daq.TRTxAOD\_Z.f716\_trt098-01\_EXT0 \\
		group.det-indet.00304337.physics\_Main.daq.TRTxAOD\_f716\_Ztnp3\_EXT0 \\
		group.det-indet.361106.PowhegPythia8EvtGen\_AZNLOCTEQ6L1\_Zee.recon.TRTxAOD.e3601\_s2876\_r7886\_trt093-00\_EXT0 \\
		group.det-indet.361107.PowhegPythia8EvtGen\_AZNLOCTEQ6L1\_Zmumu.recon.TRTxAOD.e3601\_s2876\_r7886\_trt093-04\_EXT0
	\end{tabular}
\end{table*}
%
%Data samples:
%\begin{verbatim}
%
%\end{verbatim}
%
%Simulation samples:
%{
%\scriptsize
%\begin{verbatim}
%
%\end{verbatim}
%}

\section{Event and lepton selections}
\label{sec:selections}
The event selection is split into pre-selection with event-level cuts as well as lepton selections with cuts on the leptons being selected for. For any electron pair passing the final cut, the probe is kept. For any muon pair passing the final cut, both are kept. All combination of pairs is attempted, and thus pairs with switched labels are also accepted if they pass the cuts. New pairs of (other) leptons are still accepted after this, also with switched labels.

The pre-selection is listed in \cref{tab:eventsel}, and the electron and muon cuts are listed in \cref{tab:leptonsel}. Some of the cuts will be detailed in the paragraphs below.

\subsection{Event pre-selection}
The first pre-selection cut skips new events if a previous event with the same event number has already been processed. Sometimes, events are duplicated in production, so this cut ensures that duplicates are not used.

Event cleaning refers to the usual veto on events due to problematic regions of the detector or incomplete events.

The good runs list for the 2016 data is:
\begin{fullwidth}
\begin{verbatim}
data16_13TeV.periodAllYear_DetStatus-v88-pro20-21_DQDefects-00-02-04_PHYS_StandardGRL_All_Good_25ns.xml
\end{verbatim}
\end{fullwidth}~

Finally, all non-prescaled triggers for the lepton being selected for are checked. If any trigger passes, the event is pre-selected, given that the event contains at least two of the leptons being selected for.

For simulated data, no trigger cut was applied due to bugs in the sample production.

\begin{table}[htbp]
	\centering
	\begin{threeparttable}
	\begin{tabular}{l}
		Remove duplicated events \\
		{[Data] Event cleaning} \\
		{[Data] Pass GRL} \\
		Any non-prescaled \emph{lepton} trigger passed\tnote{1} \\
		At least two \emph{lepton}s left in the event \\
	\end{tabular}
	\begin{tablenotes}
	\item[1] Trigger selection is not applied for simulated data because of reconstruction bugs at the time of sample production.
	\end{tablenotes}
	\end{threeparttable}
	\caption{Event pre-selection. The \emph{lepton} corresponds to the lepton being selected for, not both. The cuts are detailed in the text. Numbers for these cuts can be seen in Tabs.~\cref{tab:cutflow1}--\cref{tab:cutflow2} which uses shorter names for the cuts. The short names match the labels in \cref{fig:ElCutflow} and \cref{fig:MuCutflow}.}
	\label{tab:eventsel}
\end{table}

\subsection{Lepton selection}
With the pre-selection cuts passed, all the leptons of a given type are paired in all combinations. For each pair, one candidate is tested as the tag and the other as the probe. As soon as the tag or the probe fails a cut, the next pair is tested.

Truth-matching of electrons is performed by using the truth information supplied by an algorithm during reconstruction. Prompt, FSR and bremsstrahlung electrons are all accepted. Due to a bug in the sample, no truth-matching is performed for muons.

The $p_\mathrm{T}$ cuts are set to $5$ GeV for electrons and muons in order to keep efficiency high, even though egamma uses e.g. $25$ GeV and $15$ GeV for tags and probes, respectively. The invariant mass cut has an indirect cut on the momentum which raises the effective lower momentum limit. The $p_\mathrm{T,trk}>0.25\cdot \pT$ cut ensures that the tracks of electrons passing with minimum \pT still have a decent $p_\mathrm{T,trk}$.

The silicon hits are Pixel + SCT. All hits include dead sensors.

The LLH ID includes all variables for electron PID, while the cut-based ID has fewer variables and looser cuts for looser PID. The cut-based loose PID does not include TRT information.

The electron probe ID and isolation cuts at the bottom of the table are additional cuts compared to egamma's to further clean the electron sample.

\begin{table*}[htbp!]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\begin{threeparttable}
	\begin{tabular}{ll}
		\textbf{Electron channel} & \textbf{Muon channel} \\
		{[}Simulation{]} Both truth-matched & {[}Simulation{]} Both truth-matched\tnote{1} \\
		$80 < m_{ee} < 100$ GeV & Both are combined muons with $\ge 2$ precision hits \\
		Both pass author & $80 < m_{\mu\mu} < 100$ GeV \\
		Tag $p_\mathrm{T}>5$ GeV & At least one match trigger object \\
		Probe $p_\mathrm{T}>5$ GeV & Both $\eta<2.0$ \\
		Probe $p_\mathrm{T,trk}>0.25\cdot p_\mathrm{T}$ & Both $p_\mathrm{T}>5$ GeV \\
		Both $\eta<2.0$ & Both $\mathrm{hits_{pix}}>=1,\ \mathrm{hits_{silicon}}>=7,\ \mathrm{hits_{TRT}>=15}$ \\
		Both pass OQ & Both pass tight track iso cut, $p_\mathrm{T}^\mathrm{varcone30}/p_\mathrm{T} < 0.06$ \\
		Tag match trigger object & Both pass tight calo iso cut, $E_\mathrm{T}^\mathrm{topocone20}/p_\mathrm{T} < 0.06$ \\
		Tag pass tight track iso cut, $p_\mathrm{T}^\mathrm{cone20}/p_\mathrm{T} < 0.06$ & Opposite sign \\
		Tag pass tight calo iso cut, $E_\mathrm{T}^\mathrm{topocone20}/p_\mathrm{T} < 0.06$ & \\
		Tag $\mathrm{hits_{pix}}>=1,\ \mathrm{hits_{silicon}}>=7,\ \mathrm{hits_{TRT}>=15}$ & \\
		Tag pass tight LLH ID & \\
		Opposite sign & \\
		Probe pass same hits and non-LLH loose ID & \\
		Probe tight track iso cut & \\
		Probe tight calo iso cut & \\
	\end{tabular}
	\begin{tablenotes}
		\item[1] Muon truth-matching is not applied because of reconstruction bugs at the time of sample production.
	\end{tablenotes}
	\end{threeparttable}
	\caption{Lepton selection. Cutflow table for electrons (left) and muons (right) in the order of the applied cuts. Some cuts are detailed in the text. Numbers for these cuts can be seen in Tabs.~\cref{tab:cutflow1}--\cref{tab:cutflow2} which uses shorter names for the cuts. The short names match the labels in \cref{fig:ElCutflow} and \cref{fig:MuCutflow}.}
	\label{tab:leptonsel}
\end{table*}

\subsection{Cutflow for both selections}
The cutflow diagrams can be found in \cref{fig:ElCutflow} for electrons and \cref{fig:MuCutflow} for muons. For both figures, the red bins show the number of \emph{events} passing the \emph{event pre-selection}, and blue bins show the number of \emph{leptons} passing the \emph{lepton selection}. Note that the "InitialElectronZ" and "InitialMuonZ" show every pair before any cut (and they therefore get a contribution of $n(n-1)$ per event with $n$ being the number of leptons\footnote{Since the label switch is allowed, all electrons are tried as both tags and probes. If the switch was not allowed, the expression would be halfed, that is $n(n-1)/2$.}). Since these are mostly low \pT background, the first cut will clear out the majority of the lepton pairs. The raw numbers are given in Tabs.~\cref{tab:cutflow1}--\cref{tab:cutflow2}.
The cutflow diagrams can be found in \cref{fig:ElCutflow} for electrons and \cref{fig:MuCutflow} for muons. For both figures, the red bins show the number of \emph{events} passing the \emph{event pre-selection}, and blue bins show the number of \emph{leptons} passing the \emph{lepton selection}. Note that the "InitialElectronZ" and "InitialMuonZ" show every pair before any cut (and they therefore get a contribution of $n(n-1)$ per event with $n$ being the number of leptons\footnote{Since the label switch is allowed, all electrons are tried as both tags and probes. If the switch was not allowed, the expression would be halfed, that is $n(n-1)/2$.}). Since these are mostly low \pT background, the first cut will clear out the majority of the lepton pairs. The raw numbers are given in Tabs.~\cref{tab:cutflow1}--\cref{tab:cutflow2}.

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.48\textwidth]{figures/trt/data/cutflowZee.pdf}
	\includegraphics[width=0.48\textwidth]{figures/trt/mc/cutflowZee.pdf}
	\caption{Cutflow diagram for electron selection. The red histogram is the number of events passing the event pre-selection and the blue is the number of electrons passing the electron selection.}
	\label{fig:ElCutflow}
\end{figure*}

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.48\textwidth]{figures/trt/data/cutflowZmm.pdf}
	\includegraphics[width=0.48\textwidth]{figures/trt/mc/cutflowZmm.pdf}
	\caption{Cutflow diagram for muon selection. The red histogram is the number of events passing the event pre-selection and the blue is the number of muons passing the muon selection.}
	\label{fig:MuCutflow}
\end{figure*}

\begin{table*}%[H]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\captionsetup[subtable]{position = below}%
	\captionsetup[table]{position=top}%
	\subfloat[][]{%{0.3\linewidth}%
		\centering%
		\begin{tabular}{| l | l | l | l |}%
		\hline%
			\multicolumn{4}{|c|}{Data, electrons} \\
			Cut & Probes & Eff. & Acc. eff. \\ \hline
			InitialEvent & 1953402 & & \\
			RemoveDuplicates & 1807815 & & \\
			EventCleaning & 1806534 & & \\
			GRL & 1806360 & & \\
			TriggerFired & 725917 & & \\
			EnoughLeptons & 721161 & & \\ \hdashline
			InitialElectronZ & 15994269 & & \\
			MassWindow & 789362 & & \\
			BothAuthor & 789362 & 100.00 & 100.00 \\
			TagPt & 748174 & 94.78 & 94.78 \\
			ProbePt & 714111 & 95.45 & 90.47 \\
			ProbeAtLeastRelPt & 607149 & 85.02 & 76.92 \\
			TagEta & 510486 & 84.08 & 64.67 \\
			ProbeEta & 432711 & 84.76 & 54.82 \\
			BothOQ & 430250 & 99.43 & 54.51 \\
			TagTrigger & 255597 & 59.41 & 32.38 \\
			TagTrkIso & 240406 & 94.06 & 30.46 \\
			TagCaloIso & 224465 & 93.37 & 28.44 \\
			TagTrk & 193578 & 86.24 & 24.52 \\
			TagIDTight & 178654 & 92.29 & 22.63 \\
			OS & 158166 & 88.53 & 20.04 \\
			ProbeQuality & 110369 & 69.78 & 13.98 \\
			ProbeTrkIso & 107763 & 97.64 & 13.65 \\
			ProbeCaloIso & 100383 & 93.15 & 12.72 \\
		\hline%
		%\caption{electrons in data}
		\end{tabular}%
	}%
	\,%
	\subfloat[][]{%{0.3\linewidth}
		\centering%
		\begin{tabular}{| l | l | l | l |}%
		\hline%
			\multicolumn{4}{|c|}{Simulation, electrons} \\
			Cut & Probes & Eff. & Acc. eff. \\ \hline
			InitialEvent & 603426 & & \\
			RemoveDuplicates & 603426 & & \\
			EnoughLeptons & 530642 & & \\ \hdashline
			InitialElectronZ & 5555704 & & \\
			BothTruthmatching & 649596 & & \\
			MassWindow & 399638 & & \\
			BothAuthor & 399638 & 100.00 & 100.00 \\
			TagPt & 399491 & 99.96 & 99.96 \\
			ProbePt & 399344 & 99.96 & 99.93 \\
			ProbeAtLeastRelPt & 361351 & 90.49 & 90.42 \\
			TagEta & 307853 & 85.20 & 77.03 \\
			ProbeEta & 269437 & 87.52 & 67.42 \\
			BothOQ & 267735 & 99.37 & 66.99 \\
			TagTrigger & 267735 & 100.00 & 66.99 \\
			TagTrkIso & 261204 & 97.56 & 65.36 \\
			TagCaloIso & 223747 & 85.66 & 55.99 \\
			TagTrk & 187276 & 83.70 & 46.86 \\
			TagIDTight & 140981 & 75.28 & 35.28 \\
			OS & 139867 & 99.21 & 35.00 \\
			ProbeQuality & 110974 & 79.34 & 27.77 \\
			ProbeTrkIso & 108588 & 97.85 & 27.17 \\
			ProbeCaloIso & 95524 & 87.97 & 23.90 \\
		\hline%
		%\caption{electrons in data}
		\end{tabular}%
	}%
	\caption{Cutflow table for data (a) and simulation (b). The efficiency is for a cut after the previous cuts. The dashed lines separate the event and lepton selections.}%
	\label{tab:cutflow1}
\end{table*}%

\begin{table*}%[H]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\captionsetup[subtable]{position = below}%
	\captionsetup[table]{position=top}%
	\subfloat[][]{%{0.3\linewidth}%
		\centering%
		\begin{tabular}{| l | l | l | l |}%
			\hline%
			\multicolumn{4}{|c|}{Data, muons} \\
			Cut & Probes & Eff. & Acc. eff. \\ \hline
			InitialEvent & 1953402 & & \\
			RemoveDuplicates & 1807815 & & \\
			EventCleaning & 1806534 & & \\
			GRL & 1806360 & & \\
			TriggerFired & 426292 & & \\
			EnoughLeptons & 425775 & & \\ \hdashline
			InitialMuonZ & 2186928 & & \\
			quality & 553615 & & \\
			MassWindow & 383286 & & \\
			trigger & 382925 & 99.91 & 99.91 \\
			eta & 286866 & 74.91 & 74.84 \\
			pt & 286463 & 99.86 & 74.74 \\
			trackQuality & 279798 & 97.67 & 73.00 \\
			MuonTrkIso & 270278 & 96.60 & 70.52 \\
			MuonCaloIso & 250736 & 92.77 & 65.42 \\
			OS & 250292 & 99.82 & 65.30 \\
			\hline%
			%\caption{electrons in data}
		\end{tabular}%
	}%
	\,%
	\subfloat[][]{%{0.3\linewidth}
		\centering%
		\begin{tabular}{| l | l | l | l |}%
			\hline%
			\multicolumn{4}{|c|}{Simulation, muons} \\
			Cut & Probes & Eff. & Acc. eff. \\ \hline
			InitialEvent & 511614 & & \\
			RemoveDuplicates & 511614 & & \\
			EnoughLeptons & 430469 & & \\ \hdashline
			InitialMuonZ & 4810508 & & \\
			quality & 698389 & & \\
			MassWindow & 463507 & & \\
			trigger & 463507 & 100.00 & 100.00 \\
			eta & 335984 & 72.49 & 72.49 \\
			pt & 335554 & 99.87 & 72.39 \\
			trackQuality & 325725 & 97.07 & 70.27 \\
			MuonTrkIso & 309651 & 95.07 & 66.81 \\
			MuonCaloIso & 285078 & 92.06 & 61.50 \\
			OS & 284937 & 99.95 & 61.47 \\
			\hline%
			%\caption{electrons in data}
		\end{tabular}%
	}%
	\caption{Cutflow table for data (a) and simulation (b). The efficiency is for a cut after the previous cuts. The dashed lines separate the event and lepton selections.}%
	\label{tab:cutflow2}
\end{table*}%

% After selection, there are X (Y) electron probes in data (simulation) and X (Y) muons in data (simulation), respectively. The low electron efficiency is due to harsh cuts that are meant to give high electron purity.

\section{Preparation prior to analysis}
\label{sec:preanalysis}
After selection, histograms and the ntuple tree are filled.

The usual event information and particle kinematics are saved in the output ntuples, including the following used for recalculating the probabilities with the new tuning. The probe track variables are: trackP, trackEta, trackOccupancy, electronProbabilityAth. The probe track hit variables are: HighThreshold, GasType, TRTPart, StrawLayer, TrackToWire, ZToR.

\emph{electronProbabilityAth} is the TRT electron probability using the current calibration in Athena. This variable is shown in some figures as 'previous calibration based on 2015 Simulation'.

2D Histograms, with $\gamma$ factor and track occupancy as axes, are filled for each gas and TRT part. All hits are filled in the LT histogram, while only HT are filled into HT histograms. To calculate $p_\mathrm{HT}$, the two histograms are divided.

The average and per SL, TW, and ZR values $p_\mathrm{HT}$ are also saved, from which the correction factors are derived.

Finally, a few histograms for the appendix are also made. These include histograms with the HT fraction calculated per track and the correction factors as a function of occupancy.

The uncertainty of the fraction is recalculated for data. The binomial uncertainty for fractions is used:
\begin{equation}
	\sigma^2 = f(1-f)/n_\mathrm{LT}, \quad f=n_\mathrm{HT} / n_\mathrm{LT}.
\end{equation}

Bins in the 2D fraction histogram with fewer than 20 HT hits are also removed.

The simulation weights ("MC weights" given by the generators) have been normalized in the very beginning due to being very high. Now, bins with few hits and no hits are weight-wise closer. Events with negative weights are skipped to ease the computation of some histograms. The amount of simulation with negative weights is approximately 2 orders magnitudes smaller.% The weights, after normalization, can be seen in \cref{fig:Weights}.
The simulation weights ("MC weights" given by the generators) have been normalized in the very beginning due to being very high. Now, bins with few hits and no hits are weight-wise closer. Events with negative weights are skipped to ease the computation of some histograms. The amount of simulation with negative weights is approximately 2 orders magnitudes smaller.% The weights, after normalization, can be seen in \cref{fig:Weights}.

Following these cuts, approximately 1.6 electron probes are saved per event when at least one electron probe passes. See \cref{fig:ProbesPerOcc}. In data, much more so than in simulation, the occupancy (and hence the pile-up) has a great influence on the selection efficiency of electrons. The stark difference between data and simulation makes data-driven calibration important for optimal performance.
Following these cuts, approximately 1.6 electron probes are saved per event when at least one electron probe passes. See \cref{fig:ProbesPerOcc}. In data, much more so than in simulation, the occupancy (and hence the pile-up) has a great influence on the selection efficiency of electrons. The stark difference between data and simulation makes data-driven calibration important for optimal performance.

The global occupancy is an absolute measure of activity in the whole TRT related to the pile-up in much the same way as shown in \cref{fig:averagemu_vs_trackocc}. The global occupancy is roughly defined as the number of hit TRT straws out of the total number of TRT straws. The track occupancy is a weighted average of the local occupancy of 192 $\phi$ slices of the whole TRT. Each slice is weighted by the ratio of hits of the track in that region to the total number of hits that make up the track. See ATL-COM-INDET-2016-008 (especially Eqs. 1 and 2) for in-depth explanations of the different occupancy definitions.
The global occupancy is an absolute measure of activity in the whole TRT related to the pile-up in much the same way as shown in \cref{fig:averagemu_vs_trackocc}. The global occupancy is roughly defined as the number of hit TRT straws out of the total number of TRT straws. The track occupancy is a weighted average of the local occupancy of 192 $\phi$ slices of the whole TRT. Each slice is weighted by the ratio of hits of the track in that region to the total number of hits that make up the track. See ATL-COM-INDET-2016-008 (especially Eqs. 1 and 2) for in-depth explanations of the different occupancy definitions.

%When the samples have been processed, the histograms are reprocessed from the ntuples. The reprocessing adds the following steps which are not included in the initial processing:
%\begin{itemize}
%	\item Events with negative weights are removed. These events amount to approximately $1.5$\% of the total number of events.
%	\item Simulation weights are normalized to 1 by dividing them with 1941.
%\end{itemize}
%
%In order to asses the weights, these events must be included in the ntuples.

% \begin{figure}[htbp]
% 	\begin{center}
% 		\includegraphics[page=1,width=0.48\textwidth]{figures/trt/data/weights.pdf}
% 		\includegraphics[page=1,width=0.48\textwidth]{figures/trt/mc/weights.pdf}\\
% 		\includegraphics[page=2,width=0.48\textwidth]{figures/trt/data/weights.pdf}
% 		\includegraphics[page=2,width=0.48\textwidth]{figures/trt/mc/weights.pdf}
% 		\caption{Event weights for real and simulated data. No limits are imposed on the axes. The simulation weights have been re-scaled by dividing by $1941$. As seen in the figure, this leads to a mean of approximately 1.} % Negative weights are multiplied by $-1$ and shown in red.
% 		\label{fig:Weights}
% 	\end{center}
% \end{figure}

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.48\textwidth]{figures/trt/data/probesPerOcc.pdf}
	\includegraphics[width=0.48\textwidth]{figures/trt/mc/probesPerOcc.pdf}
	\caption{For events with at least one electron probe passing, the average number of selected electron probes per event is plotted against the global occupancy (black), the smaller track occupancy of the one or two electron probe(s) selected per any given event (blue), and the greater track occupancy of them (red).}
	\label{fig:ProbesPerOcc}
\end{figure*}
