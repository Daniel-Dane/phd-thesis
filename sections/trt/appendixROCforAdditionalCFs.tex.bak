\section{Performance for different CF configurations}
\label{sec:appendixROCForAdditionalCFs}
Referenced in \cref{sec:CF} and \cref{sec:final}, this appendix shows ROC curves for additional CF configurations. Importantly, truncating $CF(TW)$ at $2.0$ mm (blue vs magenta lines in \cref{fig:rocsAllCFs_2} through \cref{fig:rocsAllCFs_8}) shows a decrease in performance in data which warrants its full use in data as well.

\foreach \i in {2,4,6,8}{
	\begin{figure}[!htbp]
		\begin{center}
			\includegraphics[page=\i,width=0.48\textwidth]{figures/trt/data/rocsAllCFs.pdf}
			\includegraphics[page=\i,width=0.48\textwidth]{figures/trt/mc/rocsAllCFs.pdf}
			\caption{ROC curves for additional CF configurations. The blue line has $CF(TW)$ truncated at $2.0$ mm which shows a decrease in performance.}
			\label{fig:rocsAllCFs_\i}
		\end{center}
	\end{figure}
}
