\chapter{Results}
\emph{\small
	The fit model was examined in the previous chapter, and the NPs were found to be mostly reasonable, that is to say we saw few cases of very biased or very constrained or pulled NPs. We can now with confidence examine the post-fit plots of the mass distribution and derive upper limits on the production cross-section of our theoretical particles in the case of no discovery. This chapter will show these results in the narrow-width and large-width cases separately and finally set exclusions on parts of the phase space of the 2HDM type-I introduced in \cref{sec:theory:2HDM}, given that we know the theoretical cross-sections and branching ratios.
}
\minitoc
\label{sec:azh:results}

~\\\noindent
As mentioned in \cref{sec:stats:unblinded:llWW}, the signal was interpolated in ${10~\GeV}$ steps in the range ${200<m_H<700}$ and ${m_H+100<m_A<800~\GeV}$ yielding $1326$ SRs in $51$ $m_{4q}$ windows. The limits, given as \[ \sigma^\mathrm{AZH} = {\sigma(gg\rightarrow A)\times\mathrm{BR}(A\rightarrow ZH)\times\mathrm{BR}(H\rightarrow WW)} \] in picobarn, and their $p$-values, given by \cref{eq:fitmodel:pint}, have been calculated and will be shown in 2D as a function of the $H$ and $A$ masses for the signal hypotheses. The limits will also be shown in 1D in slices of $m_H$. Signals shown in this chapter are scaled such that ${\sigma^\mathrm{AZH}=1~\mathrm{pb}}$.

%\section{Narrow-width limits}
%\FloatBarrier
%\subsection{Post-fit (unblinded)}
\label{sec:results:llWWpostfit}
Before showing the final results, some intermediate background-only post-fit figures for $m_{4q}$, $J$, \pT of leading jet, and $\pT^{\ell\ell}$ will be shown for the SR at Level 2 (after the $J$ selection but before any $m_{4q}$ window), except $J$ that will be shown at Level 1. The fits have been done in the same way except the side-band region is not (ie. cannot be) included. The post-fit of $m_{4q}$ must agree well with data so as not to introduce any bias for some signal hypotheses. \cref{fig:results:llWWm4qPostFit:m4q} shows extremely good agreement. $J$ was used to remove background, and its pre-fit modeling at high values did show some disagreement. As can be seen in \cref{fig:results:llWWm4qPostFit:Jvar}, the post-fit shows good agreement. The two momenta in \cref{fig:results:llWWm4qPostFit:jet0pt,fig:results:llWWm4qPostFit:zpt} were used to correct some of the pre-fit mismodeling and introduce systematic uncertainties that the likelihood fit could use to correct the remaining mismodeling.

%As previously mentioned in \cref{sec:stats:unblinded:llWW}, the signal has been interpolated in $10~\GeV$ steps for the range $200<m_H<700$ and $m_H+100<m_A<800~\GeV$. This gives $1326$ signal regions.
Background-only fits to data in the SR at Level 3 using both control regions are shown in \cref{fig:results:llWWpostfits-1,fig:results:llWWpostfits-2,fig:results:llWWpostfits-3,fig:results:llWWpostfits-4} for four representative signal hypotheses. \cref{fig:results:llWWpostfits-excess} shows the most significant (defined in a moment) signal hypothesis. The figures contain the post-fit expected background and the observed data with each a signal hypothesis. \cref{app:llWW:unblindedpostfitandpullplots} contains post-fits, pull plots, and ranks for the remaining mass points for which signal was simulated.

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\subfloat[$m_{4q}$]{
			\includegraphics[width=0.48\textwidth]{figures/azh/paper/CR_llWW_mWW.pdf}
			\label{fig:results:llWWm4qPostFit:m4q}
		}
		\subfloat[$J$]{
			\includegraphics[width=0.475\textwidth]{figures/azh/paper/CR_llWW_Jvar.pdf}
			\label{fig:results:llWWm4qPostFit:Jvar}
		}
		\\
		\subfloat[$\pT^\mathrm{jet1}$]{
			\includegraphics[width=0.49\textwidth]{figures/azh/paper/CR_llWW_jet0pt.pdf}
			\label{fig:results:llWWm4qPostFit:jet0pt}
		}
		\subfloat[$\pT^{\ell\ell}$]{
			\includegraphics[width=0.475\textwidth]{figures/azh/paper/CR_llWW_ZpT.pdf}
			\label{fig:results:llWWm4qPostFit:zpt}
		}
	\end{center}
	\caption{Post-fit plots in the SR at Level 2 (except for (b) that is at Level 1) of (a) the mass $m_{4q}$ of the $H$ candidate, (b) the $J$ variable, (c) the \pT of the leading jet, and (d) the \pT of the $Z$ candidate.}
%	\label{fig:results:llWWm4qPostFit}
\end{figure*}

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[$(300,200)$]{
		\includegraphics[width=0.40\textwidth]{figures/azh/paper/SR_llWW/mA300mH200.pdf}
		\label{fig:results:llWWpostfits-1}
	}
	\subfloat[$(500,230)$]{
		\includegraphics[width=0.40\textwidth]{figures/azh/paper/SR_llWW/mA500mH230.pdf}
		\label{fig:results:llWWpostfits-2}
	}\\
	\subfloat[$(700,400)$]{
		\includegraphics[width=0.40\textwidth]{figures/azh/paper/SR_llWW/mA700mH400.pdf}
		\label{fig:results:llWWpostfits-3}
	}
	\subfloat[$(700,550)$]{
		\includegraphics[width=0.40\textwidth]{figures/azh/paper/SR_llWW/mA700mH550.pdf}
		\label{fig:results:llWWpostfits-4}
	}
	\\
	\subfloat[$(440,310)$]{
		\includegraphics[width=0.40\textwidth]{figures/azh/limits/unblinded_llWW/AZH_mA440_mH310/plots/postfit/Region_BMin0_incJet1_Y2015_DSRcutbasedJvarH310_T2_L2_distmVHcorr_J2_GlobalFit_conditionnal_mu0log.pdf}
		\label{fig:results:llWWpostfits-excess}
	}
	\caption{(a-d) The post-fit plots for four representative signal hypotheses. (e) The post-fit plot for the $(440,310)$ signal hypothesis with local significance at 2.9 sigma.}
%	\label{fig:results:llWWpostfits}
\end{pagefigure}

\FloatBarrier
%\subsection{1D limits}
\label{sec:results:llWWlimits}
%The upper limits are set on the product of $\sigma(ggF)\times BR(A\rightarrow ZH)
%%\times BR(Z\rightarrow ll)
%\times BR(H\rightarrow WW)
%%\times BR(W\rightarrow qq')^{2}
%$
%in units of pb.

The fits have then been done on the combined signal and background. % Limits on $\sigma(gg\rightarrow A)\times\mathrm{BR}(A\rightarrow ZH)\times\mathrm{BR}(H\rightarrow WW)$ are set using the CLs method (introduced in \cref{sec:azh:fitmodel}) at the $95\%$ confidence level.
No excess above $3$~sigma has been observed; the largest local (global) deviation is 2.9 (0.82) sigma at $(440,310)$. The global significance has been calculated from the largest up-crossing \cite{Vitells:2011da} of $15$ at ${m_A=750~\GeV}$ in \cref{fig:results:llWW:muhat}. The upper limit varies from \maxGGAlimllWW~pb (\maxGGAlimExpllWW~pb expected) for the
\maxGGAlimPointllWW{} signal hypothesis down to \minGGAlimllWW~pb (\minGGAlimExpllWW~pb expected) for the
\minGGAlimPointllWW{} signal hypothesis.

The limits are shown in slices of $m_H$ in \cref{fig:results:llWWunblindLimitSlices} in the usual "Brazil bands" for the narrow-width signals. The large-width signals without error bands are shown as well, and they will be further discussed in the next section. See \cref{fig:results:llWWunblindLimitSlices-c} for the 1D limit slice hosting the mass point, whose post-fit mass was shown in \cref{fig:results:llWWpostfits-excess}. See \cref{app:llWW:observedlimits} for the remaining slices. For low $m_H$ ($m_H<300$), the observed narrow-width limits dip below the expected at around ${m_A=430~\GeV}$ and then consistently overshoot after ${m_A=560~\GeV}$. This overshooting disappears for slices ${m_H=250~\GeV}$ and higher. However, the dip is visible in some fashion relative to the original position up to ${m_H=370~\GeV}$.

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[$m_H=200~\GeV$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits_new/limit_1D_mH200ggA_llWW.pdf}}
	\subfloat[$m_H=300~\GeV$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits_new/limit_1D_mH300ggA_llWW.pdf}}\\
	\subfloat[$m_H=310~\GeV$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits_new/limit_1D_mH310ggA_llWW.pdf}\label{fig:results:llWWunblindLimitSlices-c}}
	\subfloat[$m_H=400~\GeV$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits_new/limit_1D_mH400ggA_llWW.pdf}}
%	\subfloat[$m_H=500~\GeV$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits_new/limit_1D_mH500ggA_llWW.pdf}}
%	\subfloat[$m_H=600~\GeV$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits_new/limit_1D_mH600ggA_llWW.pdf}}
	%	\includegraphics[width=0.49\textwidth]{figures/azh/limits/unblinded_llWW/700.png}
	\caption{The limits on the production cross-section times branching ratios for $A$ in $m_{H}$ slices of $200$, $300$, $310$, and $400$ GeV, respectively. The ${m_H=310~\GeV}$ slice hosts the largest deviation at ${m_A=440~\GeV}$.}
	\label{fig:results:llWWunblindLimitSlices}
\end{figure*}

%\FloatBarrier
%\subsection{2D limits and p0 scan}

\label{sec:results:llWW:2DLimAndSig}
The limits for all $1326$ signal hypotheses are drawn in a 2D histogram (each bin representing an SR) in \cref{fig:results:llWWunblind2Dlimits}, which includes the significances of these limits. % are shown in \cref{fig:results:llWWunblind2Dp0scan}.
\cref{fig:results:llWWunblind2Dlimits-a,fig:results:llWWunblind2Dlimits-b} respectively show the expected and observed 95\% upper limits. The uncertainties on the limits are not shown in the 2D figures, and the reader may refer to the previous 1D slices or the $p$-values in \cref{fig:results:llWWunblind2Dlimits-c}. As noted earlier, no excess above $3$~sigma is observed, and the observed data can be considered consistent with the background-only hypothesis.

%\begin{figure}[htbp]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\begin{center}
%		\includegraphics[width=\textwidth]{figures/azh/limits/unblinded_llWW/AZH_mA440_mH310/plots/postfit/Region_BMin0_incJet1_Y2015_DSRcutbasedJvarH310_T2_L2_distmVHcorr_J2_GlobalFit_conditionnal_mu0log.pdf}
%	\end{center}
%	\caption{The post-fit plot for the $(m_A,m_H)=(440,310)~\GeV$ signal region with local significance at 2.9 sigma.}
%	\label{fig:results:llWWpostfits-excess}
%\end{figure}

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[Expected limits.]{
		\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/plot_limit_exp_llWW.pdf}
		\label{fig:results:llWWunblind2Dlimits-a}
	}
	\subfloat[Observed limits.]{
		\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/plot_limit_obs_llWW.pdf}
		\label{fig:results:llWWunblind2Dlimits-b}
	}
	\\
	\subfloat[Local $p$-values for observed limits.]{
		\includegraphics[width=0.49\textwidth]{figures/azh/paper/local_p0_observed_llWW.pdf}
		\label{fig:results:llWWunblind2Dlimits-c}
	}
	\subfloat[Sign of $\hat{\mu}$.]{
		\includegraphics[width=0.49\textwidth]{figures/azh/bla.pdf}
		\label{fig:results:llWW:muhat}
	}
	\caption{(a) The expected limits for $A$ and $H$. (b) Observed limits. (c) The local significances of the observed limits. (d) The sign of $\hat{\mu}$ for every signal hypothesis, which is used to calculate the number of up-crossings.}
	\label{fig:results:llWWunblind2Dlimits}
%	\label{fig:results:llWWunblind2Dp0scan}
\end{figure*}

\FloatBarrier
\section{Large-width and 2HDM limits}
\label{sec:azh:results:lw}
Before showing results in the 2HDM interpretation, a benchmark with fixed large widths is made. The limits for signals with 10\% and 20\% widths are shown in \cref{fig:results:llWWunblind2Dlimits-LW} in 2D and was shown in \cref{fig:results:llWWunblindLimitSlices} for different ${m_H}$ slices. The dip and overshoot seen for narrow-width signals are also visible for the large-width signals. As the width of $A$ increases, a decrease in the signal significance is to be expected, which can be seen as higher upper limits for the large width signals. The local deviations are $0.68$ sigma and $0.74$ sigma at $(440,310)$ for 10\% and 20\% width signals, respectively.

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/plot_limit_exp_llWWLW10.pdf}
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/plot_limit_obs_llWWLW10.pdf}\\
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/plot_limit_exp_llWWLW20.pdf}
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/plot_limit_obs_llWWLW20.pdf}
	\caption{Left column: The expected limits for $A$ and $H$. Right column: Observed limits. Top row: Signals of 10\% width. Bottom row: Signals of 20\% width.}
	\label{fig:results:llWWunblind2Dlimits-LW}
\end{figure*}
%sec:azh:samples:2hdm
The observed data is now interpreted in the type-I 2HDM model using the setup and settings described in \cref{sec:theory:2HDM} to derive the theoretical production cross-sections, branching ratios, and $A$ widths. Due to the analysis searching for off-alignment signals, the at-alignment constrain ${\cos(\beta-\alpha)=0}$ cannot be applied. Instead, the results will be shown as a function of $\cos(\beta-\alpha)$ and $m_A$ in slices of $\tan\beta$ and $m_H$ in \cref{fig:results:2HDM-exclusion}. The dip and overshoot are visible in the exclusion figures, especially for ${\tan\beta=0.5}$, as respective observed exclusions around $430~\GeV$ in the about $2$ sigma band of the expected exclusions and no observed exclusions for higher $m_A$ where there was expected exclusion. The majority of the parameter space that the analysis is sensitive to is already excluded by previous ATLAS analyses~\cite{HIGG-2018-57} (cf. \cref{fig:theory:limitatlas}). Since the lepton-specific type is identical to type-I in the quark sector, these results apply to the former model as well for the given $\tan\beta$ slices\footnote{For larger values, $H\rightarrow\tau\tau$ takes over.}. As this analysis has not included $b$-associated production, which is important to type-II and the flipped type, no exclusions are provided for these models.

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/llWW_type1_cba_mA_mH200_tb0p5.pdf}
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/llWW_type1_cba_mA_mH240_tb0p5.pdf}\\
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/llWW_type1_cba_mA_mH200_tb1p0.pdf}
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/llWW_type1_cba_mA_mH240_tb1p0.pdf}\\
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/llWW_type1_cba_mA_mH200_tb2p0.pdf}
	\includegraphics[width=0.49\textwidth]{figures/azh/paper/limits_llWW/llWW_type1_cba_mA_mH240_tb2p0.pdf}\\
	\caption{The expected and observed exclusion limits for $m_A$ as a function of $\cos(\beta-\alpha)$ for three $\tan\beta$ slices (values $0.5$, $1.0$, and $2.0$ for the rows from top to bottom) for $m_H=200$ (left column) and $240~\GeV$ (right column). Only type-I is shown, since the analysis has insignificant sensitivity to other types.}
	\label{fig:results:2HDM-exclusion}
\end{pagefigure}