\chapter{Fit model}
\emph{\small
	In this chapter, we will build our fit model, which we will fit to real data. We start by introducing the likelihood function with its statistical and systematic uncertainties. We will then first apply this fit model to the simulated data as well as real, blinded data to validate our model. With satisfactory results, we then apply the fit model to real, unblinded data and examine the model one last time. Actual results from the fit will be shown in the next chapter.
}
\minitoc
\label{sec:azh:fitmodel}

~\\\noindent
The nominal, simulated backgrounds were shown against real data in \cref{sec:backgroundestimation}. At that point, the expected sum of backgrounds was merely compared with data. In order to give proper expected upper limits on the production cross-section, the normalizations and shapes need to be estimated from data. These estimations are for instance $Z$+jets and \ttbar normalization factors derived from their control regions, which have been combined into one bin each; additionally, one sigma up and down variations of systematics (from detector, reconstruction, or theory) provide alternative shapes which can correct the simulated background towards data.

This is accomplished by the use of a fit model that encompasses all nominal as well as systematic information and has the flexibility to fit the simulated background to data within the boundaries given by the statistical and systematic variations.

The expected number of signal or background events is modeled by the Poisson distribution, \[ P\left( N | B \right) = \frac{{e^{ - B } B ^N }}{{N!}}, \] which gives the probability of observing $N$ events given $B$ expected background events.

To take normalization and shape systematics into account, each bin\footnote{Or each event for unbinned fits.} must also be modeled by a probability density function (pdf). One can multiply the independent pdfs to form a pdf for the ensemble. In general terms, the pdf will be of the form \cite{Cranmer:2015nia}, \[ \textrm{PDF}(B|\theta)= \textrm{Poisson}(N | B) \prod_{i={\textrm{bins}}} f(B_i | \theta), \] where $f(B_i | \theta)$ is a pdf for the expected background in bin $i$ given the vector of systematic uncertainties $\theta$.

The observed number of events $N$ is fixed to the real data recorded. Instead, the expected background events is varied until the probability calculated from the PDF is maximized. This is the maximum likelihood estimation. One constructs the \emph{likelihood function} from the pdfs, \[ L(\mu|\theta) = \prod_{i={\textrm{bins}}} {\textrm{Poisson}}\Big[N_{i}~\Big|~\mu S_i+B_{i}(\theta)\Big]\cdot G(\theta), \] where $\mu$ is a parameter of interest apart from the systematics in $\theta$ that scales the expected signal events $S_i$, and pdfs constraining the systematic uncertainties are multiplied on as $G(\theta)$. ${\mu=1}$ corresponds to the expected signal strength, and ${\mu=0}$ means no signal (ie. background-only) following the definitions in \cref{tab:nullvsalthypo}.

\begin{margintable}%[htbp]
	\begin{center}
		\begin{tabularx}{\textwidth}{p{20pt}X}
			\toprule
			Hypo. & PDF                                                           \\ \midrule
			Null  & $\textrm{Poisson}\Big[N_{i}~\Big|~B_{i}(\theta)\Big]$         \\
			Alt.  & $\textrm{Poisson}\Big[N_{i}~\Big|~\mu S_i+B_{i}(\theta)\Big]$ \\ \bottomrule
		\end{tabularx}
		\caption{The null hypothesis assumes background only. The alternative hypothesis includes signal $S$ with fitted strength $\mu$.}
		\label{tab:nullvsalthypo}
	\end{center}
\end{margintable}

%For computational reasons, the maximization is done on the logarithm of the pdf since this turns the products of the likelihood into terms.

When the likelihood function has been maximized, the best \emph{unconditional} estimate of $\theta$ becomes $\hat{\theta}$. The \emph{conditional} estimate is denoted $\hat{\hat{\theta}}$ and is found by maximizing the likelihood with one or more parameters fixed ($\mu$ in this case).

The likelihood value on its own has no meaning, but one can create a \emph{test statistic} that is a quantification of the hypothesis testing using Wilks' theorem \cite{wilks1938}. From aforementioned estimates, one can calculate the \emph{profile likelihood ratio}\footnote{Since $\hat{\mu}$ can become negative, the denominator sets $\hat{\mu}=0$ for negative $\hat{\mu}$.} \cite{Cranmer:2015nia}, \[ \lambda(\mu) = \dfrac{L(\mu|\hat{\hat{\theta}})}{L(\hat{\mu}|\hat{\theta})}, \] where $\lambda(\mu)$ has the property $0<\lambda(\mu)<1$. If the alternative hypothesis agrees with the null hypothesis, the value becomes $1$.

This ratio is used to form the test statistic, \[ q_\mu = -2\ln\lambda(\mu), \] for values $\mu\ge\hat{\mu}$. This test statistic can be converted to a $p$-value:

\begin{align}
	p_\mu = \int_{q_\mu,\textrm{obs}}^{\infty} \textrm{PDF}(q_\mu|\mu,\hat{\hat{\theta}})~dq_\mu.
	\label{eq:fitmodel:pint}
\end{align}

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	{\centering
		\includegraphics[width=\textwidth]{figures/outside/alexread_confidence_dist.pdf}
		\caption{The pdfs for the signal+background (brown dot-dashed lines) and background-only (blue dashed lines). The observed value (vertical red line) in the middle of the signal+background distribution has excluded CL$_\mathrm{b}$ but not $1-\mathrm{CL}_\mathrm{b}$ (yellow area). If the two distributions overlap further, the experiment is less sensitive to new signals. Adapted from \cite{Read:2002hq}.}
		\label{fig:CLsfig}
	}
\end{marginfigure}

This test statistic is used to establish an upper limit on $\mu$, ie. by finding $\mu$ for which $p<0.05$.
%Since low $\mu$ values can have high $p$,
Since "inferior" experiments (large backgrounds, large uncertainties) can more easily reject a signal (see \cref{fig:CLsfig}),
a more robust measure is the modified frequentist confidence method, $CL_s$ \cite{Read:2002hq}, which scales the signal+background probability by the background-only rejection, \[ CL_s=\dfrac{p_\mu}{p_0}, \] where $1-p_0$ is the $p$-value of the background-only hypothesis. $p_0$ is calculated by setting $\mu=0$ in \cref{eq:fitmodel:pint}.

The $p$-value can be converted to a $Z$ significance by reversing the one-sided $p$-value calculation of a unit Gaussian at $Z$ standard deviations \cite{Cowan:2010js}: \[ Z = \Phi^{-1}(1-p_0). \]

\section{The likelihood function}
\label{sec:azh:fitmodel:likelihood}
The fit model is implemented in a custom framework built upon RooStats \cite{Moneta:2010pm} and RooFit \cite{RooFit} that are included in the ROOT framework \cite{Brun:1997pa}. Minuit2 \cite{Minuit2,JAMES1975343} is used for the minimization of the likelihood function.

Uncertainties are added to the fit model as nuisance parameters (NPs). Shape and normalization systematics are constrained by either unit Gaussian or log-normal distributions. Statistical uncertainties are constrained by Poisson distributions. The floating normalizations for the $Z$+jets (named norm\_Z) and \ttbar (norm\_ttbar) backgrounds are constrained by their single-bin control regions. Note that the list of NPs have been reduced from the initial full list (cf. \cref{sec:unc:NPreduction}).

The likelihood function is a product of Poisson distributions in each bin with additional constraints from nuisance parameters:

\[ L\left(\mu,\alpha,\theta~|~m_A,m_H\right) = \prod_{r=\textrm{regions}}~\prod_{i={\textrm{bins}}} {\textrm{Poisson}}\Big[N_{r,i}~\Big|~\mu\times S_{r,i}(m_A,m_H,\theta)+B_{r,i}(\alpha,\theta)\Big]\cdot G(\theta), \]
where
\begin{myitemize}
	\item $\mu$, the parameter of interest, is a factor to the expected signal rate,
	\item $\alpha$ is a vector of floating scale factors for the $Z$+jets an \ttbar normalizations,
	\item $\theta$ is a vector of all NPs,
	\item $N_{r,i}$ is the number of observed events in bin $i$ in region $r$,
	\item $S_{r,i}$ is the number of expected signal events in bin $i$ in region $r$ for the mass hypothesis $(m_A,m_H)$,
	\item $B_{r,i}$ is the number of expected background events (for all processes) in bin $i$ in region $r$, and
	\item $G(\theta)$ is a product of the unit Gaussians that each constrain an NP.
\end{myitemize}

$S_{r,i}$ is scaled to the integrated luminosity, cross-section times branching ratio and selection efficiency times acceptance. $B_{r,i}$ is the sum of all simulated backgrounds but includes the scale factors for $Z$+jets and \ttbar.

The expected signal and background events in each bin are affected by the normalization and shape NPs. The process-specific normalizations naturally only affect the expected background. The product over the regions represents the signal region and the two single-bin control regions that aid in constraining the floating background normalizations.

The NPs additional to the experimental systematics are listed in \cref{tab:nuis_llWW}. The data-driven uncertainties for the background modeling were made for the sum of all backgrounds and covered in \cref{sec:llWW:bkg-estimation:zjets,sec:unc:llWW:zjetsmismodelling}.

The diboson uncertainties are derived in Ref.\footnote{This reference is an internal document. }~\cite{Ryzhov:2310214} under similar kinematic conditions. They find an uncertainty of approximately $20\%$. To account for the two additional jets of this analysis, the uncertainties are increased by adding $25\%$ in quadrature for each additional jet\footnote{This method has been used in previous analyses to extrapolate estimations to additional jets.}. With the two additional jets, the diboson systematic uncertainties are about $50\%$ uncertainty. The uncertainty for single-top cross-section is 20\% based on Ref. \cite{singletopsyst}. However, these backgrounds are very small, so the precise sizes of the uncertainties matter little.

The fit model is rather complex and includes a fair number of bins and a large number of NPs. To speed up and stabilize the fitting procedure, NPs that contribute very little are automatically pruned away before the minimization step. The level of contribution is quantified by requiring that at least one systematic bin deviates from the nominal by $1\%$, among others. "One-sided" NPs only have one variation. These are symmetrized to have equal but opposite up and down variations before the minimization step.

Some NPs, though especially JER and JES, can have large statistical fluctuations due to a limited number of simulated events, and that can lead to non-parabolic shapes near the likelihood extremum. This can be partly mitigated by the act of \emph{smoothing}. The default smoothing algorithm works on the ratio between the nominal histogram and each systematic histogram and will combine the extremum bin with the smallest $\chi^2$ between the nominal and systematic values in that bin with its right neighbor iteratively until 3 extrema remain. The signal systematics (e.g. shape and normalization systematics from the interpolation), the four EL\_EFF systematics, and the mismodeling systematics are never smoothed.

To maximize signal significance, a custom automatic rebinning method is employed as follows. The peak of the interpolated signal is identified and its bin value is saved; bins for the sum of the simulated backgrounds are likewise saved. The bins to the side of the peak, starting on the left and switching side at every step, are then added to the sum until all of the following criteria are met:
\begin{myitemize}
	\item the sum reaches at least $68.3\%$ of the total signal yield,
	\item the error on the sum of background bins in the same interval is less than $30\%$, and
	\item the summed bins span at least $10~\GeV$.
\end{myitemize}
Starting with the first non-zero background bin from the left, bins are combined until the error on the combined bins is less than a tolerance level and span at least $10~\GeV$. The tolerance is defined as $7$ times the relative error on the full background (though max. $20\%$). This is repeated until the bin contained in the combined signal peak is reached. The same is done for bins starting at the first non-zero background bin from the right. 
Finally, bins, starting at the first bin, with $0$ background are removed, until a bin with a non-zero value is met. The same is done for bins starting at the last bin.

\begin{table*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		%    \resizebox{\textwidth}{!}{%
		\begin{tabularx}{\linewidth}{llllX}
			\toprule
			Process & Parameter name                  & Description                                   & Value                                         & Effect \\ \midrule
			All     & PtV                             & $p_T^{\ell\ell}$ shape correction             & -                                             & Shape  \\
			        & Jet0pt                          & $\pT^\mathrm{lead~jet}$ shape correction      & -                                             & Shape  \\
			        & Mqqqq                           & $m_{4q}$ shape correction                     & -                                             & Shape  \\
			Z       & norm\_Z                         & $Z$+jets normalization                          & Floating                                      & Norm   \\
			Top     & norm\_ttbar                     & $t\bar{t}$ normalization                      & Floating                                      & Norm   \\
			        & stopWtNorm                      & $tW$ normalization                 & 20\%                                          & Norm   \\
			%       & stoptNorm                       & single top $t$-channel normalization          & 20\%                                          & Norm   \\
			        & stopsNorm                       & {\small single top $s$-channel normalization} & 20\%                                          & Norm   \\
			Diboson & WWNorm                          & $WW$ normalization                            & 50\%                                          & Norm   \\
			        & ZZNorm                          & $ZZ$ normalization                            & 50\%                                          & Norm   \\
			        & WZNorm                          & $WZ$ normalization                            & 50\%                                          & Norm   \\
			Signal  & {\small TheoryScaleAcc\_signal} & QCD scale variation                           & 1.5\%                                         & Norm   \\
			        & {\small TheoryTuneAcc\_signal}  & Tune variation                                & 3\%                                           & Norm   \\
			        & {\small TheoryPDFAcc\_signal}   & PDF variation                                 & $2.1 + 0.70 \times \frac{m_A}{100~\GeV}$ [\%] & Norm   \\ \bottomrule
		\end{tabularx}%
		%    }
		\caption{Nuisance parameters in addition to the experimental systematics, which were provided in the previous chapter (ie.~\cref{tab:unc:NPexplanations}).}
		\label{tab:nuis_llWW}
	\end{center}
\end{table*}

\FloatBarrier
\section{Model inspection with Asimov}
\label{sec::model_inspection}
The Asimov dataset, first formalized in Ref. \cite{Cowan:2010js}, is created from the simulated background. Instead of performing many Monte Carlo simulations to estimate the fit model's uncertainty when applied to the simulation, the model can be fit on the Asimov dataset, which is created from the sum of the nominal simulated backgrounds. The uncertainties on the model parameters can be estimated from this fit in the asymptotic limit, although the assumption is found to be valid even for somewhat small datasets \cite{Cowan:2010js}.

Pull\footnote{Defined as the fitted value subtracted from the initial value and divided by the uncertainty on the fitted value.} plots for the NPs entering the fit are shown in \cref{fig:stats:llWW:pulls} for three representative mass points and \cref{app:llWW:pullsranklimits} for the remaining mass points. The boxes in the pull figures show the significance of each pull. Due to lack of numerical accuracy, pulls that constrain extremely little might have large uncertainties, leading to fake large significances.

NPs are ranked by fixing them at their positive or negative one-sigma values one at a time and noting the difference on $\mu$. Fits leading to large $\Delta\mu$ show large \emph{impact} on the fit. \cref{fig:stats:llWW:ranks_comb} shows the impact (blue bars; upper horizontal axis) for the 25 impact-leading NPs as well as their pulls (black points; lower horizontal axis), which were shown in the previous figure. The yellow band shows the "pre-fit" impact by scaling the post-fit impact with the inverse of the pull. The NPs are ordered by the sizes of their post-fit impacts.

%The NP pulls and the highest correlations as well as their individual impact on POI are shown in following figures.
%TODO: Missing correlations plot!

%"The boxes show the significances of the pulls, capped to $\pm 3$.
%The significance of a pull is $pull/\sqrt{1 - postfiterror^2}$.
%It works well to identify which pulls are really problematic, modulo a few caveats:
%\begin{myitemize}
%	\item might not be implemented properly for the MC stat 'gamma' parameters.
%	\item for parameters which are almost not constrained, numerical accuracies can give somehow 'fake' large significances. This is sometimes the case of Jet/Etmiss NPs.
%\end{myitemize}
%The fact that relatively small pulls of parameters which are not very much constrained can be more significant than larger pulls of very well constrained parameters is underappreciated."\footnote{WSMaker email list.}

%\label{sec:llWW:pullsandranks}
%See the pulls for low mass, high mass-splitting, and high mass in \cref{fig:stats:llWW:pulls}. The ranks for the NPs are shown in \cref{fig:stats:llWW:ranks,fig:stats:llWW:ranks2}. See \cref{app:llWW:pullsranklimits} for the pulls and ranks for the remaining simulated signals.

%These blinded fits have not been updated after unblinding. The updated fit range which was decided after unblinding is not ported back to the blinded fits. The change of smoothing algorithm as well as NP averaging (discussed in the next section) have also not been ported back into these figures. Finally, the theory systematics (pdf, scale, shower) in these plots were taken from llbb (though they are much the same).

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\includegraphics[width=\textwidth]{figures/azh/limits/llWW/AZH_mA300_mH200/plots/fcc/AsimovFit_unconditionnal_mu0/NP_allExceptGammas.png}\\
		\includegraphics[width=\textwidth]{figures/azh/limits/llWW/AZH_mA600_mH300/plots/fcc/AsimovFit_unconditionnal_mu0/NP_allExceptGammas.png}\\
		\includegraphics[width=\textwidth]{figures/azh/limits/llWW/AZH_mA800_mH700/plots/fcc/AsimovFit_unconditionnal_mu0/NP_allExceptGammas.png}
	\end{center}
	\caption{The pulls of the nuisance parameters used in the Asimov fits for the samples $(300,200)$, $(600,400)$, and $(800,700)$.}
	\label{fig:stats:llWW:pulls}
\end{pagefigure}

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	%\hspace*{-1cm}
	\subfloat[$(300,200)$]{\includegraphics[width=0.49\textwidth]{figures/azh/limits/llWW/AZH_mA300_mH200/pdf-files/pulls_SigXsecOverSM_125.pdf}}
	\subfloat[$(600,300)$]{\includegraphics[width=0.49\textwidth]{figures/azh/limits/llWW/AZH_mA600_mH300/pdf-files/pulls_SigXsecOverSM_125.pdf}}
%	\caption{The post-fit ranking of the nuisance parameters used in the fits for the samples $(300,200)$ and $(600,300)$.}
%	\label{fig:stats:llWW:ranks}
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
	%\hspace*{-1cm}
	\\
%	\phantom{	\includegraphics[width=0.49\textwidth]{figures/azh/limits/llWW/AZH_mA800_mH700/pdf-files/pulls_SigXsecOverSM_125.pdf}}
	\subfloat[$(800,700)$]{\includegraphics[width=0.49\textwidth]{figures/azh/limits/llWW/AZH_mA800_mH700/pdf-files/pulls_SigXsecOverSM_125.pdf}}
	\caption{The post-fit ranking of the nuisance parameters used in the Asimov fits for the signals (a) $(300,200)$, (b) $(600,300)$, and (c) $(800,700)$.}
	\label{fig:stats:llWW:ranks_comb}
\end{pagefigure}

%Since the pulls are derived using Asimov which is MC-only, the NPs derived from fitting the mismodelling in $\pT^{\ell\ell}$ ("SysPtV"), leading jet \pT ("SysJet0pt), and $m_{4q}$ ("SysM4q") will be constrained as is seen.

The rank plots shows the absolute change on the signal strengths. \cref{tab:systematicsllWW} shows the relative uncertainties on $\hat{\mu}$ from the leading sources of systematic uncertainty for two mass points. Uncertainties having the largest impact depend on the choice of $(m_A,m_H)$. However, the jet energy scale and resolution clearly dominate the uncertainty. They are followed by simulation statistics and the systematic uncertainties derived for the background mismodeling.

\begin{table}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\begin{tabularx}{\linewidth}{llp{10pt}ll}
			\toprule
			\multicolumn{2}{c}{$(500,300)$, $0.70$~pb}  &  &         \multicolumn{2}{c}{$(700,200)$, $0.38$~pb}         \\ \cmidrule(lr){1-2}\cmidrule(lr){4-5}
			Source             & {$\Delta\hat{\mu}/\hat{\mu}$ [\%]} &  & Source             & {$\Delta\hat{\mu}/\hat{\mu}$ [\%]}                \\ \midrule
			Data stat.         & 32                     &  & Data stat.         & 33                                    \\
			Total syst.        & 42                     &  & Total stat.        & 38                          \rowspace \\
			\quad Sim. stat.   & 24                     &  & \quad Sim. stat.   & 19                                    \\
			\quad Sig. interp. & 14                     &  & \quad Sig. interp. & 12                                    \\
			\quad Bkg. model.  & 14                     &  & \quad Bkg. model.  & 16                                    \\
			\quad JES/JER      & 30                     &  & \quad JES/JER      & 23                                    \\
			\quad Theory       & 6.5                    &  & \quad Theory       & 7.6                                   \\ \bottomrule
		\end{tabularx}
		\caption{The effect of the most important sources of uncertainty on $\hat{\mu}$ at two mass points, $(500,300)$ and $(700,200)$. The signal cross-sections are taken to be the expected median upper limits and they correspond to values that are shown on the table next to the indicated mass points. "Sim. stat." stands for simulation statistics, "Sig. interp." for signal interpolation, and "Bkg. model." for the background modeling. "Theory" refers to the theoretical uncertainties on signal due to scales, tune, and PDF.}
		\label{tab:systematicsllWW}
	\end{center}
\end{table}

\FloatBarrier
\subsection{NP reduction campaign}
\label{sec:unc:NPreduction}
The production of the final histograms using all relevant systematics (including all years and all backgrounds) took initially $\mathcal{O}(\mathrm{weeks})$. To reduce the production time, a single production was made using only real and simulated data from 2015+2016 including all systematics. The systematics were ranked according to their impact on the final limit for all simulated signals. Then a number of leading systematics for each simulated signal were selected, and a union of these was chosen as the set of systematics that will be used for future productions. An important criterion for selection process was that the final limit would be insignificantly affected. It was decided that a change of $\mathcal{O}(1\%)$ in the limit, much less than the statistical uncertainty, would be tolerable.

During this campaign, a different number of leading systematics were tested. In the end, it was decided to use the union of the 15 leading systematics of each simulated signal\footnote{The size of this final set is larger than~15.}. The pruned list gives a difference of approximately 1 percent (or much less) in the final limit for most signals. The final list of systematics chosen was checkmarked in \cref{tab:unc:NPreductionInitalList}, and the impact of this reduction is shown in \cref{tab:unc:NPimpact-llWW}.

With the pruned systematics, reduced number of backgrounds (cf.~\cref{sec:azh:samples:samples}), and optimizations in the code, the full production was reduced to $\mathcal{O}(\mathrm{days})$.

\begin{table}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{tabularx}{\linewidth}{lllllX}
		\toprule
		$m_A$ & $m_H$ & All       & Pruned    & Rel. diff. & Stat. only \\ \midrule
		$300$ & $200$ & $0.2163$ & $0.2134$ & $1.35$\%   & $0.0808$  \\
		$350$ & $250$ & $0.1119$ & $0.1103$ & $1.43$\%   & $0.0580$  \\
		$400$ & $200$ & $0.1028$ & $0.1019$ & $0.93$\%   & $0.0579$  \\
		$400$ & $300$ & $0.0560$ & $0.0560$ & $0.02$\%   & $0.0360$  \\
		$500$ & $200$ & $0.0693$ & $0.0684$ & $1.27$\%   & $0.0375$  \\
		$500$ & $300$ & $0.0473$ & $0.0473$ & $0.02$\%   & $0.0349$  \\
		$500$ & $350$ & $0.0421$ & $0.0420$ & $0.10$\%   & $0.0317$  \\
		$500$ & $400$ & $0.0209$ & $0.0208$ & $0.33$\%   & $0.0181$  \\
		$600$ & $200$ & $0.0324$ & $0.0317$ & $2.36$\%   & $0.0234$  \\
		$600$ & $300$ & $0.0308$ & $0.0307$ & $0.41$\%   & $0.0243$  \\
		$600$ & $400$ & $0.0186$ & $0.0186$ & $0.13$\%   & $0.0159$  \\
		$600$ & $480$ & $0.0128$ & $0.0127$ & $0.75$\%   & $0.0105$  \\
		$700$ & $200$ & $0.0166$ & $0.0165$ & $0.61$\%   & $0.0140$  \\
		$700$ & $500$ & $0.0097$ & $0.0097$ & $0.11$\%   & $0.0086$  \\
		$800$ & $200$ & $0.0113$ & $0.0113$ & $0.18$\%   & $0.0102$  \\
		$800$ & $300$ & $0.0129$ & $0.0129$ & $0.03$\%   & $0.0115$  \\
		$800$ & $500$ & $0.0073$ & $0.0073$ & $0.02$\%   & $0.0067$  \\
		$800$ & $600$ & $0.0061$ & $0.0061$ & $0.05$\%   & $0.0056$  \\
		$800$ & $700$ & $0.0052$ & $0.0052$ & $<0.01$\%  & $0.0047$  \\ \bottomrule
	\end{tabularx}%
	\caption{The final limits for the fit model including all or a reduced list of systematics. "All" contains all systematics except for the modeling systematics, which were finalized later in the analysis. "Pruned" contains the NPs with checkmarks in \cref{tab:unc:NPreductionInitalList}. Limits with no systematics at all (stat. only) are shown for comparison.}
	\label{tab:unc:NPimpact-llWW}
\end{table}

\FloatBarrier
\section{Blinded model inspection with real data}
%FROM PREVIOUS SECTION: These blinded fits have not been updated after unblinding. The updated fit range which was decided after unblinding is not ported back to the blinded fits. The change of smoothing algorithm as well as NP averaging (discussed in the next section) have also not been ported back into these figures. Finally, the theory systematics (pdf, scale, shower) in these plots were taken from llbb (though they are much the same).

To get a better understanding of the NPs, real data in the SR at Level 2 (before any $m_{4q}$ window) has been fitted using a background-only assumption. The calculated limits and significances will not be observed. The fits are made using different lower bounds for $m_A$ to show the stability of the fits for higher masses; for a choice of $m_H$, the lower bound of $m_A$ is set to $m_H+90~\GeV$. These lower bounds are also used in the final fits. The pull plots can be found in \cref{fig:stats:llWW:CRpulls}.

%For $\ell\ell WW$, these pull plots can be found in \cref{fig:stats:llWW:CRpulls} and \cref{app:llWW:CRpulls}. In general, the mismodelling NPs and several JER/JES NPs pull to or beyond 1 sigma for $m_H=200~\GeV$ and $m_H=300~\GeV$. For $m_H>=350~\GeV$, the NPs stay within 1 sigma.
%
%Pull plots for low $m_H$ cuts show some pull and constraint for the jet and background modelling NPs. To test whether these are fluctuations, the fit is done separately on 2016, 2017, and 2018 data. See \cref{fig:stats:llWW:CRpullsYears} for $m_H=200~\GeV$ only. % and \cref{app:llWW:CRpullsYears} for the remaining $m_H$ cuts.
%All NPs but jet flavour response, jet flavour composition, and the background modelling show behaviour consistent with fluctuations. The two jet NPs are consistently constrained, and all background modelling NPs always pull in the same direction except for SysPtV in 2017.
%
%Some constrains are due to the choice of smoothing algorithm or smoothing strength, which in this case may not aggressive enough. This is evident by comparing the NPs in \cref{fig:stats:llWW:NPexample} % (see \cref{app:llWW:NPs} for full list of NPs)
%with their constraints in the CR pull plots in \cref{fig:stats:llWW:CRpulls}.
%
%A "RatioUniformKernel" is used for the alternative smoothing algorithm. See \cref{fig:stats:llWW:NPexampleAltSmoothing} for the NPs and \cref{fig:stats:llWW:CRpullsAltSmoothing} for the pulls. % (figures for slightly higher $m_H$ cuts are in \cref{app:llWW:NPsPullsAltSmoothing}).
%Using the alternative smoothing algorithm, the NPs are much less pulled.
%
%The result of the fits to the CR is seen in \cref{fig:stats:llWW:CRpostfit} which shows the post-fit figures of $m_{2\ell4q}$.

In making these fits, two major changes to the analysis have been made:
\begin{myitemize}
	\item An alternative smoothing algorithm is used because the standard smoothing algorithm was not effective or aggressive enough in smoothing fluctuations in the smaller NPs
	\item A number of NPs have had their variations symmetrized by averaging up and down because they were pulled or constrained strongly (even after switching smoothing algorithm):
	\begin{myitemize}
		\itemsep0em
		\item MUON\_SAGITTA\_RESBIAS
		\item JET\_CR\_JET\_Pileup\_RhoTopology
		\item JET\_CR\_JET\_Pileup\_OffsetNPV
		\item JET\_CR\_JET\_Pileup\_PtTerm
		\item JET\_CR\_JET\_Pileup\_OffsetMu
		\item JET\_CR\_JET\_EffectiveNP\_Mixed1
		\item JET\_CR\_JET\_EffectiveNP\_Modelling1
		\item JET\_CR\_JET\_EtaIntercalibration\_Modelling
		\item JET\_CR\_JET\_BJES\_Response
		\item JET\_CR\_JET\_Flavor\_Composition
		\item JET\_CR\_JET\_Flavor\_Response
	\end{myitemize}
\end{myitemize}

The alternative smoothing algorithm is based on the Nadaraya--Watson kernel regression estimate \cite{Nadaraya_1964,10230725049340} and applied on the ratio between each systematic histogram and the nominal histogram. Before applying the smoothing algorithm, the bins of each systematic histogram is combined so as to have at most $5\%$ uncertainty each. The "box" kernel is used. The kernel bandwidth parameter is estimated from the average of the bandwidth parameters that, after smoothing, give the smallest $\chi^2$ difference per bin, where the bin itself has been interpolated by the kernel.

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	%\hspace*{-1cm}
	\subfloat[$\min(m_A)=200+90~\GeV$]{\includegraphics[width=0.99\textwidth]
		{figures/azh/limits/CR_llWW/AZH_mA300_mH200/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}\\
	\subfloat[$\min(m_A)=300+90~\GeV$]{\includegraphics[width=0.99\textwidth]
		{figures/azh/limits/CR_llWW/AZH_mA400_mH300/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}\\
	\subfloat[$\min(m_A)=400+90~\GeV$]{\includegraphics[width=0.99\textwidth]
		{figures/azh/limits/CR_llWW/AZH_mA500_mH400/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}
	\caption{The pulls of the nuisance parameters used in the blinded observed fits to real data.}
	\label{fig:stats:llWW:CRpulls}
\end{figure*}

%\begin{figure}[htbp]
%	\centering
%	\subfloat[2016]{\includegraphics[width=0.99\textwidth]{figures/azh/limits/CR_llWW/AZH_2016_mA300_mH200/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}\\
%	\subfloat[2017]{\includegraphics[width=0.99\textwidth]{figures/azh/limits/CR_llWW/AZH_2017_mA300_mH200/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}\\
%	\subfloat[2018]{\includegraphics[width=0.99\textwidth]{figures/azh/limits/CR_llWW/AZH_2018_mA300_mH200/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}
%    \caption{The pulls of the nuisance parameters used in the CR fits to observed data for $m_H=200~\GeV$ for the years 2016, 2017, and 2018, respectively, for the $\ell\ell WW$ channel.}
%    \label{fig:stats:llWW:CRpullsYears}
%\end{figure}
%
%\begin{figure}[htbp]
%	\centering
%	\subfloat[JET\_EtaIntercalibration\_Modelling]{\includegraphics[width=0.50\textwidth]{figures/azh/limits/CR_llWW/AZH_mA300_mH200/plots/shapes/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_Z_SysJET_CR_JET_EtaIntercalibration_Modelling.eps}}
%	\subfloat[JET\_BJES\_Response]{\includegraphics[width=0.50\textwidth]{figures/azh/limits/CR_llWW/AZH_mA300_mH200/plots/shapes/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_Z_SysJET_CR_JET_BJES_Response.eps}}\\
%	\subfloat[JET\_Pileup\_OffsetMu]{\includegraphics[width=0.50\textwidth]{figures/azh/limits/CR_llWW/AZH_mA300_mH200/plots/shapes/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_Z_SysJET_CR_JET_Pileup_OffsetMu.eps}}
%	\subfloat[JET\_Pileup\_RhoTopology]{\includegraphics[width=0.50\textwidth]{figures/azh/limits/CR_llWW/AZH_mA300_mH200/plots/shapes/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_Z_SysJET_CR_JET_Pileup_RhoTopology.eps}}
%    \caption{Selected NPs for the $\ell\ell WW$ channel, for $m_H=200~\GeV$. Some show constraint in \cref{fig:stats:llWW:CRpulls}. The dashed lines are the unsmoothed distributions. The yellow, hatched area in the background is the MC statistics.}
%    \label{fig:stats:llWW:NPexample}
%\end{figure}
%
%\begin{figure}[htbp]
%	\centering
%	\subfloat[JET\_EtaIntercalibration\_Modelling]{\includegraphics[width=0.50\textwidth]{figures/azh/limits/CR_llWW/kernelsmooth/AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA300_mH200/plots/shapes/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_Z_SysJET_CR_JET_EtaIntercalibration_Modelling.eps}}
%	\subfloat[JET\_BJES\_Response]{\includegraphics[width=0.50\textwidth]{figures/azh/limits/CR_llWW/kernelsmooth/AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA300_mH200/plots/shapes/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_Z_SysJET_CR_JET_BJES_Response.eps}}\\
%	\subfloat[JET\_Pileup\_OffsetMu]{\includegraphics[width=0.50\textwidth]{figures/azh/limits/CR_llWW/kernelsmooth/AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA300_mH200/plots/shapes/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_Z_SysJET_CR_JET_Pileup_OffsetMu.eps}}
%	\subfloat[JET\_Pileup\_RhoTopology]{\includegraphics[width=0.50\textwidth]{figures/azh/limits/CR_llWW/kernelsmooth/AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA300_mH200/plots/shapes/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_Z_SysJET_CR_JET_Pileup_RhoTopology.eps}}
%    \caption{Same NPs as shown in \cref{fig:stats:llWW:NPexample}, also for $m_H=200~\GeV$, using the alternative smoothing algorithm explained in the text for the $\ell\ell WW$ channel.}
%    \label{fig:stats:llWW:NPexampleAltSmoothing}
%\end{figure}
%
%\begin{figure}[htbp] {
%		\centering
%%		\hspace*{-1cm}
%		\subfloat[$m_H=200~\GeV$]{\includegraphics[width=0.99\textwidth]{figures/azh/limits/CR_llWW/kernelsmooth/AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA300_mH200/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}\\
%		\subfloat[$m_H=300~\GeV$]{\includegraphics[width=0.99\textwidth]{figures/azh/limits/CR_llWW/kernelsmooth/AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA400_mH300/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}\\
%		\subfloat[$m_H=400~\GeV$]{\includegraphics[width=0.99\textwidth]{figures/azh/limits/CR_llWW/kernelsmooth/AZHeavyH2b3bTEST201907WW.23tagbbALimitP0muHatOBS_AZHeavyH_13TeV_23tagbbALimitP0muHatOBS_Systs_mA500_mH400/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}
%	}
%	\caption{Same pulls as shown in \cref{fig:stats:llWW:CRpulls} using the alternative smoothing algorithm explained in the text for the $\ell\ell WW$ channel.}
%	\label{fig:stats:llWW:CRpullsAltSmoothing}
%\end{figure}
%
%
%
%
%
%\begin{figure}[htbp]
%	\centering
%	%		\hspace*{-1cm}
%	\subfloat[$m_H=200~\GeV$]{\includegraphics[width=0.60\textwidth]{figures/azh/limits/CR_llWW/32-14/AZH_mA300_mH200/plots/postfit/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_GlobalFit_unconditionnal_mu1.pdf}
%	}\\
%	\subfloat[$m_H=300~\GeV$]{\includegraphics[width=0.60\textwidth]{figures/azh/limits/CR_llWW/32-14/AZH_mA400_mH300/plots/postfit/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_GlobalFit_unconditionnal_mu1.pdf}
%	}
%%	\\
%%	\subfloat[$m_H=400~\GeV$]{\includegraphics[width=0.32\textwidth]{
%%		figures/azh/limits/CR_llWW/AZH_mA500_mH400/plots/postfit/Region_BMin0_incJet1_Y2015_DCRcutbasedJvar_T2_L2_distmVH_J2_GlobalFit_unconditionnal_mu1.pdf}
%%	}
%	\caption{CxAOD 32-14 for now. Post-fit using the CR fits for the $\ell\ell WW$ channel. The text in the figures haven't been updated yet. Ignore the "Pre-fit background" in the legend for now, as it does not contain Z.}
%	\label{fig:stats:llWW:CRpostfit}
%\end{figure}

%The issue of the heavily constrained ZPtV NP can be addressed by scaling it down by the factor that it is constrained by in the NP pull plot. The scaled NP will be evaluated using purely Asimov. After reducing, we will see the constraint disappear.

%\FloatBarrier
%\subsubsection{$\ell\ell WW$}
\section{Model inspection with real data}
\label{sec:stats:unblinded:llWW}
The signal has been interpolated in $10~\GeV$ steps in the range $200<m_H<700$ and $m_H+100<m_A<800~\GeV$. This gives $1326$ signal regions.

See \cref{fig:results:llWWpulls,fig:results:llWWranks} for the pull plots and NP ranks of four representative mass points. \cref{app:llWW:unblindedpostfitandpullplots} contains post-fits, pull plots, and ranks for the remaining mass points. The NPs are not much pulled overall, and they also show little bias. The data-driven background modeling systematics do show rather large pull in several cases.

\renewcommand{\figpath}{figures/azh/limits/unblinded_llWW/AZH_mA}
\begin{pagefigure}%[thbp]
	\vspace*{-30pt}
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[$(300,200)$]{\includegraphics[width=0.95\textwidth]{\figpath 300_mH200/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}\\
	\subfloat[$(500,300)$]{\includegraphics[width=0.95\textwidth]{\figpath 500_mH300/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}\\
	\subfloat[$(700,400)$]{\includegraphics[width=0.95\textwidth]{\figpath 700_mH400/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}\\
	\subfloat[$(700,600)$]{\includegraphics[width=0.95\textwidth]{\figpath 700_mH600/plots/fcc/GlobalFit_unconditionnal_mu1/NP_allExceptGammas.pdf}}
	\caption{The unblinded pull plots.}
	\label{fig:results:llWWpulls}
\end{pagefigure}

\begin{pagefigure}%[thbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[$(300,200)$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits/unblinded_llWW/AZH_mA300_mH200/pdf-files/pulls_SigXsecOverSM_125.pdf}}
	\subfloat[$(500,300)$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits/unblinded_llWW/AZH_mA500_mH300/pdf-files/pulls_SigXsecOverSM_125.pdf}}\\
	\subfloat[$(700,400)$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits/unblinded_llWW/AZH_mA700_mH400/pdf-files/pulls_SigXsecOverSM_125.pdf}}
	\subfloat[$(700,600)$]{\includegraphics[width=0.45\textwidth]{figures/azh/limits/unblinded_llWW/AZH_mA700_mH600/pdf-files/pulls_SigXsecOverSM_125.pdf}}\\
	\caption{The unblinded post-fit ranking of the nuisance parameters used in the fits.}
	\label{fig:results:llWWranks}
\end{pagefigure}