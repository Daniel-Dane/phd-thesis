\chapter{Systematic uncertainties}
\emph{\small
	Before building our fit model, we shall go through the list of systematics that will go into it. The list contains many sources of uncertainties coming from the detector, reconstruction, and theory. We categorize the systematics by whether they affect the \emph{normalization} (rate) or \emph{shape} of the final nominal distribution.
}
\minitoc
\label{sec:systs}

%TODO: Missing Pile-up and JVT

~\\\noindent
The systematic uncertainties in this analysis can roughly be categorized by three types:
\begin{myitemize}
	\item[\textbf{detector/reconstruction (experimental):}] Systematics from detector effects and reconstruction of physical objects and their identification, isolation, momentum scale factors, etc.
	\item[\textbf{interpolation (method):}] Uncertainty on the interpolation itself as well as propagation of detector uncertainties to interpolated signals
	\item[\textbf{modeling (theoretical):}] The Monte Carlo generator with uncertainties on theoretical and phenomenological parameters
\end{myitemize}

The detector systematics will be covered in the next section, which ends with how to carry these into the interpolated signals. The narrow-width shape uncertainty has already been covered. The subsequent section covers the theoretical uncertainties on the signal only, as the modeling for the backgrounds have been derived directly from data in \cref{sec:llWW:bkg-estimation:zjets}.

In ATLAS, many systematics are calculated by groups with specific responsibilities, e.g. the groups delivering electron identification also give $\pm1\sigma$ variations of the associated systematics. The systematics are propagated through an analysis by varying the calculated (e.g. electron probability) or measured value (jet \pT) up or down by $1\sigma$ and repeating the selections. Some variations affect either whether an event passes (e.g. a nominal medium electron becomes loose for one variation) or the event weight (scale factors which do not affect the variables that are part of the selections) or both (jet \pT scaled down for one variation may lead to too few jets with $\pT>20~\GeV$).

%\newpage
\FloatBarrier
\section{Detector uncertainties}
%The Run2 CP recommendation released in summer 2018,
%as marked "Medium Run-2 paper (2019)" on the central twiki page
%\href{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/AtlasPhysicsRun2Planning}{AtlasPhysicsRun2Planning},
%and their updates till now,
%are used for all the detector and collider related uncertainties.

The detector systematics include the integrated luminosity for 2015--2018 at $1.7\%$ \cite{ATLAS-CONF-2019-021}; electron triggers \cite{Aad:2019wsl}; electron reconstruction, energy scale and resolution, identification, and isolation \cite{1908.00005}; muon triggers \cite{Aad:2020uyd}; muon identification~\cite{1603.05598,ATLAS-CONF-2020-030}; muon reconstruction and isolation \cite{1603.05598}; muon track to vertex association; and jet vertex tagging (JVT) as well as jet energy resolution (JER) and scale (JES) \cite{Aad:2020flx}.

Since the analysis is not sensitive to the electron energy resolution or scale, a simplified list of systematics is used, which combines the electron trigger, reconstruction, identification, and isolation systematics into one systematic each. The JES and JER systematics are also from a reduced list as the analysis need not be combined with other analyses or experiments.

The full list of systematics is in \cref{tab:unc:NPreductionInitalList}. Electron-specific systematics start with "EL", muon with "MUON", E/gamma (electrons) with "EG", and jet with "JET". The type of systematic can be known by identifying "ID" as identification, "Reco" as reconstruction, "Iso" as isolation, "Trig" as trigger, "scale" as scale factor\footnote{The ratio of simulated data to real data as a function of a variable, typically \pT.}, etc. A short explanation of the systematics used in this analysis is listed in \cref{tab:unc:NPexplanations}.

\begin{table*}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\small
	\begin{tabularx}{\textwidth}{lXlp{5pt}}
		\toprule
		EL\_EFF\_ID\_TOTAL\_1NPCOR\_PLUS\_UNCOR      & $\checkmark$ & JET\_CR\_JET\_Pileup\_OffsetMu                        & $\checkmark$ \\
		EL\_EFF\_Reco\_TOTAL\_1NPCOR\_PLUS\_UNCOR    & $\checkmark$ & JET\_CR\_JET\_Pileup\_OffsetNPV                       & $\checkmark$ \\
		EL\_EFF\_Iso\_TOTAL\_1NPCOR\_PLUS\_UNCOR     & $\checkmark$ & JET\_CR\_JET\_Pileup\_PtTerm                          & $\checkmark$ \\
		EL\_EFF\_Trigger\_TOTAL\_1NPCOR\_PLUS\_UNCOR & $\checkmark$ & JET\_CR\_JET\_Pileup\_RhoTopology                     & $\checkmark$ \\
		MUON\_EFF\_RECO\_SYS                         &              & JET\_CR\_JET\_EffectiveNP\_Mixed1                     & $\checkmark$ \\
		MUON\_EFF\_RECO\_STAT                        &              & JET\_CR\_JET\_EffectiveNP\_Mixed2                     &              \\
		MUON\_EFF\_RECO\_SYS\_LOWPT                  &              & JET\_CR\_JET\_EffectiveNP\_Mixed3                     &              \\
		MUON\_EFF\_RECO\_STAT\_LOWPT                 &              & JET\_CR\_JET\_EffectiveNP\_Modelling1                 & $\checkmark$ \\
		MUON\_EFF\_TrigSystUncertainty               &              & JET\_CR\_JET\_EffectiveNP\_Modelling2                 & $\checkmark$ \\
		MUON\_EFF\_TrigStatUncertainty               &              & JET\_CR\_JET\_EffectiveNP\_Modelling3                 & $\checkmark$ \\
		MUON\_EFF\_ISO\_STAT                         &              & JET\_CR\_JET\_EffectiveNP\_Modelling4                 &              \\
		MUON\_EFF\_ISO\_SYS                          &              & JET\_CR\_JET\_EffectiveNP\_Statistical1               &              \\
		MUON\_TTVA\_STAT                             &              & JET\_CR\_JET\_EffectiveNP\_Statistical2               & $\checkmark$ \\
		MUON\_TTVA\_SYS                              &              & JET\_CR\_JET\_EffectiveNP\_Statistical3               &              \\
		EG\_SCALE\_ALL                               & $\checkmark$ & JET\_CR\_JET\_EffectiveNP\_Statistical4               &              \\
		EG\_RESOLUTION\_ALL                          & $\checkmark$ & JET\_CR\_JET\_EffectiveNP\_Statistical5               &              \\
		MUON\_ID                                     & $\checkmark$ & JET\_CR\_JET\_EffectiveNP\_Statistical6               &              \\
		MUON\_MS                                     &              & JET\_CR\_JET\_EffectiveNP\_Detector1                  &              \\
		MUON\_SCALE                                  &              & JET\_CR\_JET\_EffectiveNP\_Detector2                  &              \\
		MUON\_SAGITTA\_RHO                           &              & JET\_CR\_JET\_EtaIntercalibration\_TotalStat          & $\checkmark$ \\
		MUON\_SAGITTA\_RESBIAS                       & $\checkmark$ & JET\_CR\_JET\_EtaIntercalibration\_NonClosure\_highE  &              \\
		JET\_JvtEfficiency                           & $\checkmark$ & JET\_CR\_JET\_EtaIntercalibration\_NonClosure\_negEta &              \\
		JET\_CR\_JET\_JER\_EffectiveNP\_1            & $\checkmark$ & JET\_CR\_JET\_EtaIntercalibration\_NonClosure\_posEta &              \\
		JET\_CR\_JET\_JER\_EffectiveNP\_2            &              & JET\_CR\_JET\_EtaIntercalibration\_Modelling          & $\checkmark$ \\
		JET\_CR\_JET\_JER\_EffectiveNP\_3            &              & JET\_CR\_JET\_BJES\_Response                          & $\checkmark$ \\
		JET\_CR\_JET\_JER\_EffectiveNP\_4            &              & JET\_CR\_JET\_Flavor\_Response                        & $\checkmark$ \\
		JET\_CR\_JET\_JER\_EffectiveNP\_5            & $\checkmark$ & JET\_CR\_JET\_Flavor\_Composition                     & $\checkmark$ \\
		JET\_CR\_JET\_JER\_EffectiveNP\_6            & $\checkmark$ & JET\_CR\_JET\_SingleParticle\_HighPt                  &              \\
		JET\_CR\_JET\_JER\_EffectiveNP\_7restTerm    & $\checkmark$ & JET\_CR\_JET\_PunchThrough\_MC16                      &              \\
		JET\_CR\_JET\_JER\_DataVsMC\_MC16            & $\checkmark$ &                                                       & \\ \bottomrule
	\end{tabularx}%
	\caption{The full list of systematics from detector systematics. The checkmarks indicate systematics that are kept after an evaluation in \cref{sec:unc:NPreduction}. The remaining systematics will be pruned away, as they turn out to be insignificant.}
	\label{tab:unc:NPreductionInitalList}
\end{table*}

\begin{pagetable}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\small
	\begin{tabularx}{\textwidth}{lp{45pt}X}
		\toprule
		NP                                           & Group                            & Description                       \\ \midrule
		EL\_EFF\_ID\_TOTAL\_1NPCOR\_PLUS\_UNCOR      & Electron efficiency              & Identification                    \\
		EL\_EFF\_Reco\_TOTAL\_1NPCOR\_PLUS\_UNCOR    &                                  & Reconstruction                    \\
		EL\_EFF\_Iso\_TOTAL\_1NPCOR\_PLUS\_UNCOR     &                                  & Isolation                         \\
		EL\_EFF\_Trigger\_TOTAL\_1NPCOR\_PLUS\_UNCOR &                                  & Trigger                           \\
		EG\_SCALE\_ALL                               & Electron calibration             & Energy scale                      \\
		EG\_RESOLUTION\_ALL                          &                                  & Energy resolution                 \\
		MUON\_ID                                     & Muons                            & Smearing of Inner Detector tracks \\
		MUON\_SAGITTA\_RESBIAS                       &                                  & Remaining charge-dependency after a charge-dependent scale correction \\
		JET\_JvtEfficiency                           & JVT                              & JVT efficiency                    \\
		\makecell[tl]{JET\_CR\_JET\_JER\_EffectiveNP\_1, \\ JET\_CR\_JET\_JER\_EffectiveNP\_5, \\ JET\_CR\_JET\_JER\_EffectiveNP\_6, and \\ JET\_CR\_JET\_JER\_EffectiveNP\_7restTerm}            & JER                              & Combination of measurements from in-situ dijet asymmetry measurements as well as random cones in minimum bias                                  \\
		JET\_CR\_JET\_JER\_DataVsMC\_MC16            &                                  & Uncertainty on jet response from varying jet selections                                 \\
		JET\_CR\_JET\_Pileup\_OffsetMu               & JES                              & Uncertainty on modeling the average interactions per crossing                                 \\
		JET\_CR\_JET\_Pileup\_OffsetNPV              &                                  & Uncertainty on modeling the number of primary vertices                                 \\
		JET\_CR\_JET\_Pileup\_PtTerm                 &                                  & Uncertainty on modeling the per-event \pT distribution                                 \\
		JET\_CR\_JET\_Pileup\_RhoTopology            &                                  & Uncertainty on the residual \pT dependence                                 \\
		JET\_CR\_JET\_EffectiveNP\_Mixed1            &                                  & In-situ measurements of jets in $Z$+jets, $\gamma$-jet, and multijet; Category with detector and modeling components                                 \\
		\makecell[tl]{JET\_CR\_JET\_EffectiveNP\_Modelling1, \\ JET\_CR\_JET\_EffectiveNP\_Modelling2, and \\ JET\_CR\_JET\_EffectiveNP\_Modelling3}        &                                  & Above and category with physics modeling components                                \\
		JET\_CR\_JET\_EffectiveNP\_Statistical2      &                                  & Above and category with statistics and method components                                 \\
		JET\_CR\_JET\_EtaIntercalibration\_TotalStat &                                  & Statistical uncertainties in jets with $|\eta|>0.8$                                \\
		JET\_CR\_JET\_EtaIntercalibration\_Modelling &                                  & Physics mismodeling in jets with $|\eta|>0.8$                                \\
		JET\_CR\_JET\_BJES\_Response                 &                                  & Jet response uncertainty specifically for jets initiated by $b$-quarks                                 \\
		JET\_CR\_JET\_Flavor\_Response               &                                  & Differences in the jet response for jets initiated by light quarks, $b$-quarks, and gluons \\
		JET\_CR\_JET\_Flavor\_Composition            &                                  & Uncertainty from assuming a $50\%$ quark and $50\%$ gluon composition in the sample \\ \bottomrule
	\end{tabularx}%
	\caption{Explanations of the kept systematics. The jet response is defined as $E^\mathrm{reco}/E^\mathrm{truth}$. From \cite{Bittrich:2719126,Aaboud:2257300,Aad:1705576}. Additional JER information in \url{https://indico.cern.ch/event/752759/contributions/3119136/attachments/1706979/2750689/JetETmiss\_JERRecommendation\_20180829.pdf}.}
	\label{tab:unc:NPexplanations}
\end{pagetable}

%%bsubsection{Pileup}
%%The simulated events are re-weighted in order to match the condition of multi-interaction
%%per bunch crossing (pileup) in real data that were taken after simulation~\cite{TWIKI_pileup}.
%%The relevant uncertainty is modelled with 1 NP.

\FloatBarrier
%\subsection{Interpolation of detector uncertainties for signal}
\subsection{Signal detector uncertainties interpolation}
The signals are modeled by the DSCB. The systematic variations of the simulated signals are fitted with the DSCB with fixed tail parameters and floating mean and $\sigma$. The fitted means are close to the original values, while $\sigma$ has shown significant variations. For each systematic variation, the largest difference in $\sigma$ across all simulated signals will be used for all interpolated signals. The values used are shown in
\cref{tab:signal:detector-uncertainties-llWW}.

%The variations are found to be much smaller compared to the shape interpolation and the acceptance interpolation uncertainties.

\begin{table}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\small
		\begin{tabularx}{\linewidth}{Xll}
			\toprule
			Systematic                                              & up [\%] & down [\%] \\ \midrule
			MUON\_SAGITTA\_RESBIAS                                  & 2.1     & -1.9      \\
			MUON\_ID                                                & -0.9    & 1.3       \\
			JET\_JvtEfficiency                                      & -0.1    & 3.5       \\
			JET\_CR\_JET\_Pileup\_RhoTopology                       & -2.7    & 7.1       \\
			JET\_CR\_JET\_Pileup\_PtTerm                            & 1.3     & -1.5      \\
			JET\_CR\_JET\_Pileup\_OffsetNPV                         & 2.5     & -2.2      \\
			JET\_CR\_JET\_Pileup\_OffsetMu                          & -1.6    & 2.8       \\
			JET\_CR\_JET\_JER\_EffectiveNP\_7restTerm               & 3.7     & -0.9      \\
			JET\_CR\_JET\_JER\_EffectiveNP\_6                       & 2.3     & -0.7      \\
			JET\_CR\_JET\_JER\_EffectiveNP\_5                       & 3.3     & -0.8      \\
			JET\_CR\_JET\_JER\_EffectiveNP\_4                       & 2.6     & -0.6      \\
			JET\_CR\_JET\_JER\_EffectiveNP\_3                       & 4.8     & -1.2      \\
			JET\_CR\_JET\_JER\_EffectiveNP\_2                       & 5.1     & 0.3       \\
			JET\_CR\_JET\_JER\_EffectiveNP\_1                       & 5.4     & 0.8       \\
			JET\_CR\_JET\_Flavor\_Response                          & 3.0     & -3.8      \\
			JET\_CR\_JET\_Flavor\_Composition                       & -5.4    & 8.2       \\
			JET\_CR\_JET\_EtaIntercalibration\_Modelling            & -1.5    & 1.9       \\
			JET\_CR\_JET\_EffectiveNP\_Modelling1                   & -3.6    & 5.9       \\
			JET\_CR\_JET\_EffectiveNP\_Mixed1                       & 1.4     & -0.5      \\
			JET\_CR\_JET\_BJES\_Response                            & -1.0    & 2.8       \\
			EL\_EFF\_ID\_TOTAL\_1NPCOR\_PLUS\_UNCOR & -0.1    & 2.5       \\
			EG\_SCALE\_ALL                                          & 1.1     & -0.6      \\
			EG\_RESOLUTION\_ALL                                     & 2.3     & -0.5      \\ \midrule
			Total in quadrature                                     & 14.2    & 14.9      \\ \bottomrule
		\end{tabularx}
		\caption{Detector systematics and the variation of $\sigma$ of the DSCB.}
		\label{tab:signal:detector-uncertainties-llWW}
	\end{center}
\end{table}

\FloatBarrier
\section{Modeling uncertainties on signal}
The modeling of simulated backgrounds was covered by a data-driven method introduced in \cref{sec:llWW:bkg-estimation:zjets}. However, theoretical uncertainties will need to be derived for signal. These are split into three parts: scale, tune, and PDF.

\FloatBarrier
\subsection{Scale}
\label{sec:unc:llWW:scale}
The re-normalization ($\mu_R$) and factorization ($\mu_F$) scales are separately varied up and down by a factor of two. The largest difference with respect to the nominal values is taken as an estimate of the uncertainty due to the choice of scale.

The studied samples contain truth level signal events. The following mass points have been generated with $100\,000$ events each: $(300,200)$, $(400,250)$, $(500,200)$, $(550,450)$, $(600,400)$, $(700,200)$, and $(800,500)$.
The naming convention of the samples is shown in \cref{tab:rf-samples}.

\begin{margintable}%[htbp]
	\normalsize
	\begin{center}
		\begin{tabularx}{\linewidth}{Xll}
			\toprule
			Sample & $\mu_R$              & $\mu_F$              \\ \midrule
			r0f0   & $\mu_{R}$            & $\mu_{F}$            \\
			r0fD   & $\mu_{R}$            & $\frac{1}{2}\mu_{F}$ \\
			r0fU   & $\mu_{R}$            & $2\mu_{F}$           \\
			rDf0   & $\frac{1}{2}\mu_{R}$ & $\mu_{F}$            \\
			rDfD   & $\frac{1}{2}\mu_{R}$ & $\frac{1}{2}\mu_{F}$ \\
			rUf0   & $2\mu_{R}$           & $\mu_{F}$            \\
			rUfU   & $2\mu_{R}$           & $2\mu_{F}$           \\ \bottomrule
		\end{tabularx}
		\caption{List of $\mu_{R}$ and $\mu_{F}$ samples. (r0f0) is the nominal sample.
			\label{tab:rf-samples}}
	\end{center}
\end{margintable}

A truth level event selection that imitates the reconstruction level event selection is done. The selection does not include efficiencies from lepton identification and isolation requirements. The selections before the $J$ variable selection are applied are shown in \cref{tab:Var-samples-llWW}.

\begin{table}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\begin{center}
		\begin{tabularx}{\linewidth}{lX}
			\toprule
			Cut              & Selection                                                            \\ \midrule
			$A$ boson        & Event contains exactly one $A$ boson                                 \\
			Jet multiplicity & Event contains at least 4 jets                                       \\
			Lepton $p_{T}$   & $\pT>30,~15~\GeV$ for leading and sub-leading                        \\
			Pseudorapidity   & $|\eta|<2.47$ (electrons), \;$|\eta|<2.5$ (muons)                    \\
			$Z$ boson mass   & $80 < m_{\ell \ell} < 100~\GeV$                                      \\
			Jet kinematics   & $p_T>40,~30,~20,~20~\GeV$ and $|\eta|<2.4$ for the four leading jets \\ \bottomrule
		\end{tabularx}
		\setfloatalignment{b}
		\caption{Truth level events selections.}
		\label{tab:Var-samples-llWW}
%	\end{center}
\end{table}
The signal acceptance is defined as $\varepsilon = N_\mathrm{after~selection} / N_\mathrm{before~any~selection}$, where $N_\mathrm{before~any~selection}$ counts events passing the second cut.

The variation in signal efficiency between the nominal and a varied sample is defined as $(\varepsilon_{Var} - \varepsilon_{Nom} )/ \varepsilon_{Nom}$, where
$\varepsilon_{Var}$ and $\varepsilon_{Nom}$ are the efficiencies of varied and
nominal samples, respectively. \cref{fig:Scale-vs-mA-llWW} shows the variations as a function of $m_{A}$ for all varied samples. The overall uncertainty is taken as the absolute envelope of the curves, which comes out as $\pm 1.5 \%$. No clear shape dependence is found.

\begin{figure}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	{\centering
		\includegraphics[width=\textwidth]{figures/azh/systematics/llWW/Scale.pdf}
		\caption{Scale variation as a function of $m_{A}$. The naming scheme in the legend was explained in \cref{tab:rf-samples}.}
		\label{fig:Scale-vs-mA-llWW}
	}
\end{figure}

\FloatBarrier
\subsection{Tune (ISR/FSR/MPI)}
\label{sec:unc:llWW:tune}
Monte Carlo generators use sets of parameters, named \emph{tunes}, to adjust showering, hadronization, etc. The effects on the signal acceptance by varying the parameters responsible for ISR, FSR, and MPI will be used to derive systematic uncertainties on signals' sensitivity to these variations. There are many parameters to vary, so a subset has been derived, which still covers the observables \cite{ATL-PHYS-PUB-2014-021}. These variations are sensitive to the following:
\begin{myitemize}
	\item Var1: The underlying event
	\item Var2: Jet shapes and substructure
	\item Var3a-c: Extra jets (ISR/FSR)
\end{myitemize}

Samples with same number of events and for the same mass points are generated for this as was generated for the scale study. The same truth selection is applied. 
%The nominal tune sample in different $(m_{A},m_{H})$ pairs after the last stage has been applied. 
The variations of Var1, Var2, Var3a, Var3b, and Var3c as well as the quadratic sum of positive and negative variations are given in \cref{fig:Scale-vs-mA-tune-llWW} as a function of $m_{A}$.

\begin{figure}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	{\centering
		\includegraphics[width=\textwidth]{figures/azh/systematics/llWW/Tune.pdf}
		\caption{Tune variation in percent as a function of $m_{A}$. The black, dashed lines represent the quadratic sum of positive and negative variations.}
		\label{fig:Scale-vs-mA-tune-llWW}
	}
\end{figure}

Var3c varies $\alpha_s$ for ISR jets in a large range of $0.115-0.140$. This has a relatively large effect on the signal acceptance. The fall in efficiency happens during the jet selection stage ("ResolveJetsdRW2" in \cref{tab:syst:model:tune:efflist}), because the signal jets are not among the 5 \pT-leading jets.

The tune uncertainties are the quadratic sum of the positive and the negative variations, yielding $\pm 3 \%$.

\begin{table}[thbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{tabular}{llll}
		\toprule
		Cut                 & Nom        & Var3cU     & Rel. diff. {[}\%{]} \\ \midrule
		beforeTruthAZHCheck & 100000.000 & 100000.000 & 0                   \\
		beforeLeptonPt      & 97825.000  & 97833.000  & -0.0081772          \\
		beforeLeptonEta     & 86450.000  & 86280.000  & 0.197033            \\
		beforeZMass         & 81802.000  & 81681.000  & 0.148137            \\
		before1stJetMul     & 73294.000  & 73298.000  & -0.00545717         \\
		beforeJetPreSel     & 72197.000  & 72368.000  & -0.236292           \\
		before2ndJetMul     & 72197.000  & 72368.000  & -0.236292           \\
		beforeJetPt         & 71565.000  & 71813.000  & -0.345341           \\
		beforeResolvingJets & 71559.000  & 71805.000  & -0.342595           \\
		ResolveJetsPreSel   & 71559.000  & 71805.000  & -0.342595           \\
		ResolveJetsdRW1     & 63083.000  & 62918.000  & 0.262246            \\
		ResolveJetsdRW2     & 44526.000  & 43495.000  & 2.37039             \\
		ResolveJetsH2       & 38815.000  & 37812.000  & 2.6526              \\
		ResolveJetsW1W2mass & 37085.000  & 35981.000  & 3.06829             \\
		Before Jvar         & 37085.000  & 35981.000  & 3.06829             \\ \bottomrule
	\end{tabular}
	\caption{The number of events passing the truth selection for the $(800,500)$ mass point for the nominal setup and for the Var3c up variation. The relative difference is defined as $(n_{Nom} - n_{Var} )/ n_{Var}$.}
	\label{tab:syst:model:tune:efflist}
\end{table}

\FloatBarrier
~
\subsection{PDF}
\label{sec:unc:llWW:pdf}
The systematic uncertainty due to the choice of PDF can be estimated by use of alternative PDF sets when generating Monte Carlo events. However, the central values for the nominal PDF set already fit data well for gluon fusion\footnote{According to internal documentation, \url{https://twiki.cern.ch/twiki/bin/viewauth/AtlasProtected/PdfRecommendations\#Computing\_PDF\_uncertainties}.}.

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	{\centering
		\includegraphics[width=\textwidth]{figures/azh/systematics/llWW/Pdf.pdf}
		\caption{The relative difference on signal acceptance as a function of $m_A$ due to the re-weighting variation using NNPDF23\_lo\_as\_0130\_qed.}
		\label{fig:PDF-rel-mA-llWW}
	}
\end{marginfigure}

The uncertainties on the central values within the PDF due to the fitting method (or training in the case of NNPDF) can be estimated from alternative \emph{eigensets}, which carry variations of the fit parameters. % (in the case of NNPDF: different neural networks trained with different random starting parameters).
The standard deviation of $100$ eigensets added in quadrature for the nominal PDF set, NNPDF23\_lo\_as\_0130\_qed, using LHAPDF~\cite{Whalley:2005nh} in following the recommendations by Ref. \cite{Butterworth:2015oua} will be the PDF uncertainty. The ratio of this uncertainty to the re-weighting factor of the nominal PDF set will be applied on an event-to-event basis during the event selection on the reconstructed data.

The re-weighting is applied to the following samples: $(300,200)$, $(350,250)$, $(400,200)$, $(500,200)$, $(600,400)$, $(700,200)$, and $(800,500)$. The effect on the acceptance is shown in  \cref{fig:PDF-rel-mA-llWW}, which shows the acceptance uncertainty due to the PDF variation as a function of $m_A$. The upper and lower variations are found to be symmetric and hence the absolute value is shown only. The following function is superimposed on the figure:
\begin{align}
\mathrm{PDF~uncertainty} [\%] = 2.1 + 0.70 \times \frac{m_A}{100~\GeV}.
\end{align}
The expression fits the values well and is used as the PDF uncertainty.

\FloatBarrier
\section{Background modeling uncertainties}
\label{sec:unc:llWW:zjetsmismodelling}
The corrections of the mismodeling have been covered in \cref{sec:llWW:bkg-estimation:zjets}. For the corrections, two systematic uncertainties were derived using half of the fit to the corrections.

The remaining modeling uncertainty for $m_{4q}$ is derived using 2015+2016 data. The disagreement between data and simulation is plotted and fitted for $m_{4q}$ in \cref{fig:syst:llWW:overview}, where the simulation is scaled to match data before-hand. The ratio is fitted with a linear expression: \[ -0.04181+0.00016 m_{4q} \] % for which the fit parameters are listed in \cref{tab:syst:llWW:modelsystpars}.

%\begin{table}[htbp]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\begin{center}
%		\begin{tabular}{ ll }
%			%			\hline
%			%			\multicolumn{4}{c}{Channel: $\ell\ell WW$} \\
%			\hline
%			Variable & Expression \\
%			$m_{4q}$ & $-0.04181+0.00016 m_{4q}$ \\
%			\hline
%		\end{tabular}
%		\caption{The fitted parameters for the ratio of $$. $x$ represents the given variable.}
%		\label{tab:syst:llWW:modelsystpars}
%	\end{center}
%\end{table}

The fit expression will be applied as a weight to the analysis and its full value used as a systematic for the limit setting, similar to the previous two mismodeling systematics that used half the value as uncertainty (\cref{sec:llWW:bkg-estimation:zjets}).% Since these NPs are derived from mismodeling, we expect them to be constrained during MC-only limit settings such as Asimov.

%The $\pT^{\ell\ell}$ mismodelling uncertainty propagated to $m_{2\ell4q}$ is shown in \cref{fig:syst:llWW:zptvariation} with its up and down variations.
%
%\begin{figure}[htbp]
%	\centering
%	\subfloat[$m_H=200~\GeV$]{	\includegraphics[width=0.60\textwidth]{figures/azh/systematics/llWW/C_2tag2pjet_0ptv_SRcutbasedJvarH200_mVH_SysZPtVllWW.png}}
%	\\
%	\subfloat[$m_H=400~\GeV$]{	\includegraphics[width=0.60\textwidth]{figures/azh/systematics/llWW/C_2tag2pjet_0ptv_SRcutbasedJvarH400_mVH_SysZPtVllWW.png}}
%	\\
%	\subfloat[$m_H=600~\GeV$]{	\includegraphics[width=0.60\textwidth]{figures/azh/systematics/llWW/C_2tag2pjet_0ptv_SRcutbasedJvarH600_mVH_SysZPtVllWW.png}}
%	\caption{The up and down variations of the $\pT^{\ell\ell}$ systematic seen in $m_{2\ell4q}$ at Level 3 for three $m_{4q}$ windows.}
%	\label{fig:syst:llWW:zptvariation}
%\end{figure}

\begin{figure}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\includegraphics[width=\textwidth]{figures/azh/systematics/llWW/syst_mqqqq.pdf}
		\caption{Ratio of data to background events for $m_{4q}$.}
		\label{fig:syst:llWW:overview}
	\end{center}
\end{figure}
