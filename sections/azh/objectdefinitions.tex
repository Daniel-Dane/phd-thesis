\chapter{Object definitions and preselection}
\emph{\small
%	This section will detail the physics objects that go into the analysis and their calibrations.
	This section will introduce the preselections that are applied during the creation of the calibrated samples. The preselection will be presented verbatim, including selections not relevant to this analysis, as the calibrated samples are used by multiple analyses and the selections that are applied affect which events are saved. Technical definitions of the object identifications, isolations, qualities, etc. were presented in \cref{sec:reconstruction}.
}
\minitoc
\label{sec:azh:obj}

~\\\noindent
As mentioned in \cref{sec:reconstruction}, the data recorded by ATLAS and reconstructed into datasets contain a significant number of irrelevant events; a reduced dataset which includes light selections on leptons and selections on the number of these will heavily reduce the number of events without a significant loss of potential signal. The derived dataset used by this analysis, \textsc{HIGG2D4}, includes light selections on the kinematics of electrons, muons, and jets, and requires that at least two leptons and one jet pass these selections.

The analysis uses fully-calibrated samples ("CxAOD" samples) made from these derived datasets. During the production of the calibrated samples, several preselections are made on objects, and these objects are used for the event preselection. The object preselections and overlap removal will be presented in this chapter. The object preselections will define different classes of preselected objects, e.g. "loose" and "signal" leptons. Finally, selections on the number of preselected objects are applied. Most notably, the event is required to contain exactly two "loose" leptons, of which at least one is a "signal" lepton, and at least two "signal" jets.

"Forward" ($|\eta|\ge2.5$) jets, large-R jets ($\dR=1.0$), and tau leptons are not part of this analysis. Yet, they do affect overlap removal and event preselection and are mentioned for completeness but without further introduction or explanation.

\section{Leptons}
\label{sec:azh:obj:el}
Basic selections are applied to all electrons and muons, including identification, kinematics, and isolation. These define the "loose" leptons. Loose leptons are then required to pass further kinematic selections to become "signal" leptons. All selections are listed in \cref{tab:azh:obj:lep}. Note that electrons in the calorimeter crack are not vetoed.

\FloatBarrier
\begin{table*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{tabularx}{\textwidth}{p{130pt}lX}
		\toprule
		Cut                           & Electron selection                   & Muon selection \\
		\midrule
		                              & \emph{Loose electrons}               & \emph{Loose muons}       \\
		Identification                & Pass LooseLH                         & Pass Loose       \\
		Pseudorapidity                & $|\eta^\textrm{BE(2)}|<2.47$ {\footnotesize (in second layer of ECAL)} & - \\
		Transverse momentum           & $\pT>7~\GeV$                         & $\pT>7~\GeV$       \\
		Impact significance           & $|d_0^\mathrm{BL,sig}|<5$            & $|d_0^\mathrm{BL,sig}|<3$       \\
		Track-vertex association      & $|\Delta z_0\sin{\theta}|<0.5$~mm    & $|\Delta z_0\sin{\theta}|<0.5$~mm        \\
		Shower shape quality          & Pass OQ                              & - \\ %\texttt{isGoodOQ(xAOD::EgammaParameters::BADCLUSELECTRON)}
		Isolation                     & Any of:                              & Any of:                      \\
			                          & \tabitem $\pT>100~\GeV$              & \tabitem $\pT>100~\GeV$   \\
			                          & \tabitem Pass Loose                  & \tabitem Pass Loose   \\
			                          & \tabitem Pass HighPtCaloOnly         & \tabitem Pass TightTrackOnly    \\
	                         	      & \emph{Signal electrons}              & \emph{Signal muons}   \rowspace \\
		Loose lepton                  & Pass loose electron selections above & Pass loose muon selections above \\
		Transverse momentum           & $\pT>25~\GeV$                        & $\pT>25~\GeV$      \\
		Pseudorapidity                & -                                    & $|\eta|<2.5$       \\
		\bottomrule
	\end{tabularx}
	\caption{Lepton preselection cut-flow. Identification and isolation WPs were defined in \cref{sec:reconstruction}.}
	\label{tab:azh:obj:lep}
\end{table*}

\FloatBarrier
\section{Jets}
Jets also undergo a number of kinematic and isolation selections. Central ($|\eta|<2.5$) and forward ($|\eta|\ge2.5$) jets are selected individually. Both jet types must then afterwards pass further selections to be considered "base" jets. Jets passing the first two base jet selections (without checking the third) are considered "preselected" jets and used in the coming overlap removal. Finally, "signal" jets are central jets that pass base jet selections. See the list of selections in \cref{tab:azh:obj:jet}.

\begin{table}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{tabularx}{\textwidth}{lX}
		\toprule
		Cut                          & Selection                                    \\ \midrule
		                             & \emph{Central jets}                          \\
		Jet transverse momentum      & $\pT>20~\GeV$                                \\
		Jet pseudorapidity           & $|\eta|<2.5$                                 \\
		                             & \emph{Forward jets}             \rowspace    \\
		Fwd. jet transverse momentum & $\pT^\mathrm{fwd}>30~\GeV$                   \\
		Fwd. jet pseudorapidity      & $2.5\le|\eta^\mathrm{fwd}|<4.5$              \\
		                             & \emph{Base jets}                \rowspace    \\
		Track-vertex association     & Pass JVT Medium                              \\
		Central or forward jet       & Pass central or forward selections above     \\
		Bad jet cleaning             & Pass LooseBad                                \\
		                             & \emph{Signal jets}                 \rowspace \\
		Central and base jet         & Pass central and base jet selections above   \\ \bottomrule
	\end{tabularx}
	\caption{Jet preselection cut-flow. Note that forward jets always pass JVT, and no forward JVT selection is applied. Jets are "preselected" after passing the first two base jet selections.}
	\label{tab:azh:obj:jet}
\end{table}

\FloatBarrier
\section{Overlap removal}
\label{sec:azh:obj:or}
Objects are reconstructed by separate algorithms using the same detector information; tracks and clusters are {\bfseries not} removed when an algorithm has reconstructed an object from these. Therefore, it is necessary to decide which two objects to use if they are reconstructed from the same tracks, clusters, or other information in the same direction. This process is known as \emph{overlap removal}\footnote{More information in this presentation internal to ATLAS, \url{https://indico.cern.ch/event/631313/contributions/2683959/attachments/1518878/2373377/Farrell_ORTools_ftaghbb.pdf}.}. For the \DeltaR calculation, rapidity is used. Only jets passing "preselection" and leptons passing "loose" are considered for overlap removal.

The procedure is summarized in \cref{tab:azh:obj:OR}. For steps that are not simple \DeltaR selections, the details are laid out here. The removal process is done in the order given in the table; any object passing the criteria is tested at the next step.

If two electrons share the same GSF\footnote{See \cref{sec:rec:el} for the definition.} track and either the first electron is ambiguous and the second is not\footnote{This criterion is not specified in any documentation but is applied in the code: \url{https://gitlab.cern.ch/atlas/athena/-/blob/21.2/PhysicsAnalysis/AnalysisCommon/AssociationUtils/Root/EleEleOverlapTool.cxx}, line 192.} or the $\pT$ of the first is lower than that of the second, the first electron is removed.

%If a muon was reconstructed using the inner detector and calorimeter (and hence not the muon spectrometer) it is of type calo-tagged. 
If a CT\footnote{See \cref{sec:rec:mu} for the definition.} muon shares its ID track with an electron, the muon is removed.

If an electron shares its original ID track with a muon, the electron is removed.

If a jet has fewer than three ghost-matched\footnote{Tracks are \emph{ghost-matched} to jets, if the tracks are within the area of the jet when it is formed.} tracks and either the muon ID track was ghost-matched to the jet or the \DeltaR between them is less than $0.2$, the jet is removed.

% TODO: use \newline instead of \makecell
\begin{table}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{tabularx}{\textwidth}{llX}
		\toprule
		Reject      & Against  & Criteria                                                                                                     \\
		(first)     & (second) &                                                                                                              \\ \midrule
		electron    & electron & Shared GSF track AND (first electron ambiguous while second not OR $\pT^\mathrm{first}<\pT^\mathrm{second}$) \\
		tau         & electron & $\DeltaR < 0.2$                                                                                              \\
		tau         & muon     & $\DeltaR < 0.2$                                                                                              \\
		muon        & electron & Muon is of type CT AND shared ID track                                                                       \\
		electron    & muon     & shared ID track                                                                                              \\
		jet         & electron & $\DeltaR < 0.2$                                                                                              \\
		electron    & jet      & $\DeltaR < 0.4$                                                                                              \\
		jet         & muon     & ${\mathrm{NumTrack} < 3}$ AND (ghost-matched OR ${\DeltaR < 0.2}$)                                           \\
		muon        & jet      & $\DeltaR < \min(0.4, 0.04 + 10~\GeV/\pT^\mathrm{muon})$                                                      \\
		jet         & tau      & $\DeltaR < 0.2$                                                                                              \\
		\makecell[l]{large-R\\jet} & \makecell[l]{electron\\~} & \makecell[l]{$\DeltaR < 1.0$\\~}                                                                                              \\ \bottomrule
	\end{tabularx}
\caption{Overlap removal steps. The steps are done from top to bottom.}
\label{tab:azh:obj:OR}
\end{table}

The official overlap removal contains more steps, but they are not relevant to this analysis (ie. rejecting photons).% While taus and large-R jets are also not used in this analysis, they are saved in the calibrated samples.

\FloatBarrier
\section{Event preselection}
Objects have been preselected and overlaps have been removed. The event must pass the standard event quality selections and contain a number of leptons and jets as listed in \cref{tab:azh:obj:event} to pass the preselection.

% TODO: use \newline instead of \makecell
\begin{table}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\begin{tabularx}{\textwidth}{lX}
		\toprule
		Cut                            & Selection \\
 
		\midrule
		Good Runs List        & Pass GRL       \\
		Primary vertex        & Has primary vertex       \\
		Event cleaning        & Pass event cleaning \\
		Bad jet veto          & Veto event if a jet passes the selections in \cref{sec:rec:eventquality} but fails jet cleaning (LooseBad) \\
%		Bad jet veto          & Veto event if any jet passing all of below fails bad jet cleaning (LooseBad): \\
%		                      & \tabitem $\pT>20~\GeV$ \\
%		                      & \tabitem $|\eta|<2.8$ \\
%		                      & \tabitem Pass JVT \\
%		                      & \tabitem Pass overlap removal \\
		\makecell[l]{Lepton\\multiplicity\\~} & \makecell[l]{The event must contain exactly two loose\\leptons passing the overlap removal of which\\at least one is also a signal lepton} \\
		Jet multiplicity & The event must either contain at least two signal jets passing overlap removal or \\
		\makecell[l]{Large-R jet\\multiplicity} & \makecell[l]{at least one large-R jet passing ${\pT > 200~\GeV}$\\and ${|\eta| < 2.5}$} \\
		\bottomrule
	\end{tabularx}
	\caption{Event preselection cut-flow. The event quality selections were introduced in \cref{sec:rec:eventquality}. Finally, the event must contain at least two leptons (mixed flavor is allowed) and a minimum number of jets (either two jets or one large-R jet).}
	\label{tab:azh:obj:event}
\end{table}