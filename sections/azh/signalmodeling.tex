\chapter{Signal modeling}
\emph{\small\let\thefootnote\relax\footnotetext{\emph{"With four parameters I can fit an elephant, and with five I can make him wiggle his trunk."\\~\\- John von Neumann}}%
	The signal was selected in \cref{sec:eventsel}. The selection was derived from and applied to simulated signal samples at representative mass points in a range that covers the search scope of this analysis. From these simulated signals, we will now derive functional representations for both the shapes and normalizations/acceptances, which we will use to construct signals for arbitrary mass points.
}
\minitoc
\label{sec:signalmodeling}

~\\\noindent
%The signal modeling will be done separately for the shapes and yields. 
The shape interpolation is done by fitting the $m_{2\ell4q}$ distributions of each simulated signal with the double-sided crystal ball (DSCB) \cite{Oreglia:1980cs} that has the following expression:
\begin{fullwidth}
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\vspace*{-10pt}
	\begin{align}
	f_{DSCB}(x;\mu, \sigma, n_1, a_1, n_2, a_2) &= \frac{e^{-\frac{1}{2}a_1^2}}{\Big[   \frac{|a_1|}{n_1} \Big( \frac{n_1}{|a_1|} - |a_1| - \frac{x - \mu}{\sigma}   \Big)   \Big]^{n_1}} & \mathrm{for} \qquad & \frac{x-\mu}{\sigma} < -a_1 \nonumber \\
	&= e^{-\frac{1}{2} \left ( \frac{x-\mu}{\sigma}\right ) ^2} & \mathrm{for} \qquad & -a_1 \le \frac{x-\mu}{\sigma} < a_2  \nonumber \\%\label{eqn:DSCB} \\
	&= \frac{e^{-\frac{1}{2}a_2^2}}{\Big[   \frac{|a_2|}{n_2} \Big( \frac{n_2}{|a_2|} - |a_2| + \frac{x - \mu}{\sigma}   \Big)   \Big]^{n_2}} & \mathrm{for} \qquad & \frac{x-\mu}{\sigma} \ge a_2 \nonumber
	\end{align}
\end{fullwidth}
~

The DSCB has a Gaussian core (with the usual mean $\mu$ and width $\sigma$) with power-law tails (parameters $a_1$, $a_2$, $n_1$, $n_2$). The points at which the Gaussian core joins the power-law tails are controlled by the $a$ parameters, and the sizes of these tails are controlled by the $n$ parameters. The parameters of this function (except for the mean $\mu$) will be interpolated from the best-fit values obtained by individual fits to the simulated signals. After this, the interpolated shapes will be compared to the original shapes, and a shape uncertainty will be derived.

Large-width signals will be convoluted with a modified Breit-Wigner to properly account for the natural widths of these signals.

The yield interpolation will be done on the weighted number of signal events passing the selection\footnote{The interpolation does not use the yield from the shape fits in order not to correlate the two.} using thin-plate splines \cite{Duchon1976} in the plane spanned by $m_A$ and $m_H$.

The detector systematics for the interpolated signals will be explained in the next chapter.

\section{Shape interpolation}
\label{sec:signalmodeling:shape}
The fitting procedure will be done in multiple steps, detailed in the list below. All simulated signals are fitted at each step. After each step, some tail parameters are fixed, and the simulated signals are fitted again. While any of the tail parameters are being fitted for, the mean of the fit is fixed to $m_A$. After the tail parameters have been found, the mean is left floating. The normalization is always left floating, and the width is left floating until the tail parameters have been fixed and the width itself has been interpolated\footnote{Some may disagree on this approach and want that the width of the Gaussian core be properly determined first. If the fits allow all parameters to float, one may argue that the fitted $\sigma$ parameter may not represent the width of the Gaussian core; instead, one should first fit the core within a narrow mass range (where the wide tails are not seen by the fit), fix the $\sigma$ parameter, and then fit for the tail parameters. This procedure was also tested, but it was difficult to interpolate the tail parameters.}.

The fit procedure is as follows:
\begin{myenumerate}
	\item A fully floating fit with the DSCB is done on each simulated signal. The fitted distributions are shown in \cref{fig:signal:fitsllWWcutbased} and the tail parameters are shown in \cref{fig:signal:tailDSBC-llWW}.
	\item $n_1$ shows a clear trend that is modeled by an exponential for mass-splittings greater than $100$~GeV, while $n_1$ for $\Delta M=100$~GeV and $n_2$ are fixed to constants.
	\item With $n_1$ and $n_2$ fixed, a second fit is done with the remaining parameters floating. The new tail parameters are shown in \cref{fig:signal:tailDSBC-llWW-semi}.
	\item $a_1$ is constant for $\Delta M=100$~GeV and otherwise follows a second-order polynomial in both $\Delta M$ and $m_H$. $a_2$ follows a first-order polynomial in both $\Delta M$ and $m_H$.
	\item With the tail parameters fixed (\cref{fig:signal:tailDSBC-llWW-fixed}), the signal is fitted a final time, now leaving the mean floating.
	\item The core parameters, $\mu$ and $\sigma$, are shown in \cref{fig:signal:core-llWW} as a function of the mass-splitting. The evolution of $\sigma$ shows an additional dependency on $m_H$. This dependency has been fitted and shown in the figure for slices in $m_H$ and listed in \cref{tab:signal:core-parameters-llWW}.
\end{myenumerate}

The expressions for the interpolated fit parameters are listed in \cref{tab:signal:DSCB-tail-llWW-cutbased}. Since there are no simulated signals with $\Delta M$ between $100$ and $120~\GeV$, this region will be covered by the $\Delta M=100~\GeV$ parameters.

%The signal mass resolution is shown in \cref{fig:signal:resolution-llWW}. It is defined by the half width at half maximum (HWHM) divided by the generated $A$ mass. The gives a resolution of up to 4 percent.

%The involved procedure is required to find interpolatable values of the tail parameters. Take for instance the simulated signal with masses $(400, 200)$~GeV. In the fully-floating fit (\cref{fig:signal:tailDSBC-llWW}), this signal sample converges on a very different set of tail parameters. After forcing new values for $n_1$ and $n_2$, the sample converges on values similar to the remaining samples without compromising the fit; this is seen by comparing $a_1$ and $a_2$ between \cref{fig:signal:tailDSBC-llWW} and \cref{fig:signal:tailDSBC-llWW-semi}.

% TODO: Remove title and put mass inside figure
\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[$m_A = 300$ \GeV, $m_H = 200$ \GeV]{
		\includegraphics[width=0.48\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/fits/mA300_mH200_log_cutbased.pdf}
		\includegraphics[width=0.48\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/fits/mA300_mH200_cutbased.pdf}
	}
	\\
	\subfloat[$m_A = 400$ \GeV, $m_H = 200$ \GeV]{
		\includegraphics[width=0.48\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/fits/mA400_mH200_log_cutbased.pdf}
		\includegraphics[width=0.48\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/fits/mA400_mH200_cutbased.pdf}
	}
	\\
	\subfloat[$m_A = 700$ \GeV, $m_H = 200$ \GeV]{
		\includegraphics[width=0.48\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/fits/mA700_mH200_log_cutbased.pdf}
		\includegraphics[width=0.48\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/fits/mA700_mH200_cutbased.pdf}
	}
	\caption{Fully-floating fits of $m_{2\ell4q}$ using the DSCB for different simulated signals. Logarithmic scale on the left and linear scale on the right. The pull is defined as the fit subtracted by the simulated data divided by the uncertainty of the simulated data.
	}
	\label{fig:signal:fitsllWWcutbased}
\end{pagefigure}

\begin{figure}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[$a_1$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/cb/a1_mAmH_cutbased.pdf}
	}
	\subfloat[$a_2$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/cb/a2_mAmH_cutbased.pdf}
	}
	\\
	\subfloat[$n_1$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/cb/n1_mAmH_cutbased.pdf}
	}
	\subfloat[$n_2$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_floating_scaled_withwindow/cb/n2_mAmH_cutbased.pdf}
	}
	\caption{Best-fit values of $a_1$, $a_2$, $n_1$, and $n_2$ for the DSCB fit to the simulated signals as a function of the mass-splitting when all parameters are allowed to float in the fit.}
	\label{fig:signal:tailDSBC-llWW}
\end{figure}

\begin{figure}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[$a_1$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow_semi/cb/a1_mAmH_cutbased.pdf}
	}
	\subfloat[$a_2$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow_semi/cb/a2_mAmH_cutbased.pdf}
	}
	\\
	\subfloat[$n_1$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow_semi/cb/n1_mAmH_cutbased.pdf}
	}
	\subfloat[$n_2$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow_semi/cb/n2_mAmH_cutbased.pdf}
	}
	\caption{Best-fit values of $a_1$ and $a_2$ for the DSCB fit to the simulated signals as a function of the mass-splitting when $n_1$ and $n_2$ have been fixed.}
	\label{fig:signal:tailDSBC-llWW-semi}
\end{figure}

\begin{figure}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[$a_1$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/cb/a1_mAmH_cutbased.pdf}
	}
	\subfloat[$a_2$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/cb/a2_mAmH_cutbased.pdf}
	}
	\\
	\subfloat[$n_1$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/cb/n1_mAmH_cutbased.pdf}
	}
	\subfloat[$n_2$]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/cb/n2_mAmH_cutbased.pdf}
	}
	\caption{Fixed tail parameters for the DSCB fit to the simulated signals as a function of the mass-splitting.}
	\label{fig:signal:tailDSBC-llWW-fixed}
\end{figure}

\begin{table}[htbp!]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\begin{center}
		\begin{tabularx}{\linewidth}{lll}
			\toprule
			Parameter & for $\Delta M < 120$ \GeV &                for $\Delta M \ge 120$ \GeV \\ \midrule
			$\mu$     &                  Floating &                                 Floating \\
			$\sigma$  &                  Floating &                                 Floating \\
			$a_1$     &                      4.61 &       $\max(0.5,-1.277+0.001856\Delta M$ \\
			          &                           &                $-0.0000001761\Delta M^2$ \\
			          &                           & $+0.007455 m_{4q}-0.000005514 m_{4q}^2)$ \\
			$a_2$     &    \multicolumn{2}{c}{$0.1917+0.0006275\Delta M+0.001149 m_{4q}$}    \\
			$n_1$     &                       100 &     $23.68\exp(-0.01156 \Delta M)+1.752$ \\
			$n_2$     &                      3.98 &                                     4.20 \\ \bottomrule
		\end{tabularx}
		\caption{Expressions for the interpolated parameters of the DSCB function.}
		\label{tab:signal:DSCB-tail-llWW-cutbased}
%	\end{center}
\end{table}
\begin{table}[htbp!]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\begin{center}
		\small
		\begin{tabularx}{\linewidth}{llllll}
			\toprule
			$p_0$     & $p_1$    & $p_2$     & $p_3$                   & $p_4$                  & $p_5$                  \\ \midrule
			$-0.3746$ & $0.0595$ & $-0.0665$ & $-2.041 \times 10^{-5}$ & $3.603 \times 10^{-5}$ & $5.280 \times 10^{-6}$ \\ \bottomrule
		\end{tabularx}
		\caption{The expression for the core parameter $\sigma$ fitted with a second-order polynomial in $m_A$ and $m_H$ incl. a cross-term:
			$\sigma(x) = p_0 + p_1 m_{2\ell4q} + p_2 m_{4q} + p_3 m_{2\ell4q}^2 + p_4 m_{4q}^2 + p_5 m_{2\ell4q} m_{4q}$, in \GeV.}
		\label{tab:signal:core-parameters-llWW}
%	\end{center}
\end{table}


% TODO: Move legend to the side so it's not covered
\begin{figure*}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/cb/mu_mAmH_cutbased.pdf}
	\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/cb/sigma_mAmH_cutbased.pdf}
	\caption{Left: The mean $\mu$ with mass points of the same $m_H$ connected by lines. Right: The width $\sigma$ as a function of the mass-splitting. The fit to $\sigma$ comes from \cref{tab:signal:core-parameters-llWW}.
	}
	\label{fig:signal:core-llWW}
\end{figure*}

%\begin{figure*}[htpb]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
%	\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/hwhm_mAmH_cutbased.pdf}
%	\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/hwhmDivMa_mAmH_cutbased.pdf}
%	\caption{Left: The half width at half maximum (HWHM) of the simulated signals. Right: The HWHM divided by their generated $A$ mass, giving the mass resolution. The resolution is up to 4 percent. The most narrow signals have widths of about $1~\GeV$ (with $m_A=800~\GeV$) which gives a resolution close to 1 per mille.}
%	\label{fig:signal:resolution-llWW}
%\end{figure*}

\FloatBarrier
The shape uncertainties are shown in \cref{fig:signal:llWW:inter-unc} for some signals and in \cref{app:llWW:closure} for the remaining. The procedure for deriving these uncertainties is as follows. The interpolated parameters ($\sigma$, $a_1$, $a_2$, $n_1$, and $n_2$) are varied randomly within $10\%$ of their interpolated values for the particular mass points, each time producing a new curve. Then, from this set of curves, the largest and smallest variations per $m_{2\ell4q}$ bin are chosen to define the up and down variations of the shape systematic. The variations enclose the cores well with some deviations in some $\Delta M=100~\GeV$ mass points and some deviations in the tails for several other mass points. Since the signal will be binned such that $68\%$ of the core is in one bin during the likelihood fitting stage (more on this in~\cref{sec:azh:fitmodel:likelihood}), deviations in the tails matter little as \cref{fig:signal:llWW:inter-unc-LB} shows.

\begin{figure*}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm100_mA300mH200.pdf}
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm150_mA500mH350.pdf}
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm200_mA400mH200.pdf}
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm300_mA700mH400.pdf}
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm400_mA600mH200.pdf}
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm600_mA800mH200.pdf}
	\caption{The simulated signals compared against interpolated signals along with their interpolation uncertainties.}
	\label{fig:signal:llWW:inter-unc}
\end{figure*}

\begin{figure*}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm100_mA300mH200_LB.pdf}
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm100_mA800mH700_LB.pdf}
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm300_mA700mH400_LB.pdf}
	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/fitsunc/dscb_dm600_mA800mH200_LB.pdf}
	\caption{Some of the mass points from~\cref{fig:signal:llWW:inter-unc}. The interpolated signals have been rebinned such that the core contains $68\%$ of the signal following \cref{sec:azh:fitmodel:likelihood}.}
	\label{fig:signal:llWW:inter-unc-LB}
\end{figure*}

%\begin{figure*}[htpb]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
%	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/nw_shape_syst_mA300mH200_4.pdf}
%	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/nw_shape_syst_mA500mH350_4.pdf}
%	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/nw_shape_syst_mA400mH200_4.pdf}
%	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/nw_shape_syst_mA700mH400_4.pdf}
%	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/nw_shape_syst_mA600mH200_4.pdf}
%	\includegraphics[width=0.45\textwidth]{figures/azh/signal/llWW/nw_shape_syst_mA800mH200_4.pdf}
%	\caption{The shape uncertainty on the interpolated signals for some mass points.}
%	\label{fig:signal:bla}
%\end{figure*}

%\begin{figure*}
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
%	\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/nw_shape_syst_mA600mH400_4.pdf}
%	\caption{bla}
%	\label{fig:signal:bla}
%\end{figure*}

\FloatBarrier
\subsection{Large-width signals}
\label{sec:sig-large-width}

For the 2HDM models, the natural width of the $A$ boson can be as large as~$35\%$, depending on the $\alpha$ and $\beta$ parameters of the models (cf.~\cref{sec:theory:2HDM}). The Gaussian cores (or the FWHM) of the narrow-width signals are on the order of a few percent\footnote{Compare this to the naive standard deviation of the signal distributions that are up to $17\%$, for $(580,200)$ in this case, because of the long tails.}. It is therefore necessary to convolve the signal distributions with a Breit-Wigner distribution to properly model their natural widths.

Only the natural width of $A$ is considered, since the analysis is searching for the $A\rightarrow ZH$ decay. It would require additional optimization of the $m_{4q}$ windows, if $H\rightarrow ZA$ decay were to be considered as well in this large-width scenario.

The interpolated signals are convolved with a modified Breit-Wigner. This was shown to agree with simulated large-width signals in the previous iteration of this analysis (Ref. \cite{LastPaper} for paper, Ref. \cite{Sun:2235744} for internal note). The modified Breit-Wigner is defined as follows:
\begin{fullwidth}
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\vspace*{-10pt}
	\begin{align*}
		\mathrm{BW}(m_{2\ell 4q}) = \begin{cases}
			0 & \mathrm{if}~m_{2\ell 4q}\le m_H-20~\GeV \\
			\dfrac{2}{\pi} \dfrac{\Gamma^2 m_A^2}{\left(m_{2\ell 4q}^2-m_A^2\right)^2 + \Gamma^2 m_A^2} \cdot \mathrm{LogNormal}(m_{2\ell 4q};~0.88,~m_H-20~\GeV,~m_A), & \mathrm{otherwise}
		\end{cases}
	\end{align*}
\end{fullwidth}
~

\noindent where $\Gamma = m_A w$ and $w$ is the width. The $\mathrm{LogNormal}(x,~\sigma,~\theta,~m)$ distribution is taken from ROOT\footnote{Found in the ROOT documentation at \url{https://root.cern.ch/doc/master/namespaceTMath.html\#a0503deae555bc6c0766801e4642365d2}.} \cite{Brun:1997pa}. The reason for the LogNormal addition is that the shape changes when the mass-splitting nears the $Z$ mass due to the constrained kinematic space. The log-normal shape can be seen in \cref{fig:llbbtheoryfig}.

The interpolated shape uncertainty for the large-width signals should then derived from the set of convolutions as is the case for the narrow-width signals. However, it would require many CPU hours to compute the convolutions (thousands) per bin (20) per mass point (1326) per width (20) for each variation (2) of the large-width shape systematic. Instead, the narrow-width shape systematics are convolved with the modified Breit-Wigner. The results for three widths are shown in \cref{fig:signal:llbb:large-width}.

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\includegraphics[width=\textwidth]{figures/azh/fit_500_400_10.pdf}
	\caption{${m_{\ell\ell bb}}$ using truth particles generated from ${A\rightarrow{}ZH\rightarrow\ell\ell{}bb}$ with masses $(500,400)$ and $10\%$ width. The distribution has been fitted with the modified Breit-Wigner distribution where the fit parameters have been constrained to be within a factor of $1/2$ and $2$ of their initial values. The log-normal shape is apparent.}
	\label{fig:llbbtheoryfig}
\end{marginfigure}

\begin{figure}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
	\subfloat[]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/largewidth/lw_shape_syst_mA600mH400_4_5.0.pdf}}
	\subfloat[]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/largewidth/lw_shape_syst_mA600mH400_4_15.0.pdf}}
	\\
	\subfloat[]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/largewidth/lw_shape_syst_mA600mH400_4_25.0.pdf}}
	\subfloat[]{
		\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/largewidth/lw_comparison_mA600mH400_4.pdf}}
	\setfloatalignment{b}
	\caption{(a-c) Interpolated large-width signals with shape uncertainties. (d) The interpolated large-width signals compared to the interpolated narrow-width signal.}
	\label{fig:signal:llbb:large-width}
\end{figure}

\section{Acceptance interpolation}
\label{sec:signal-acceptance-interpolation}
The yields in the SRs at Level 3 for all simulated signals will like-wise be interpolated to find normalizations for arbitrary mass points. The normalizations are interpolated using the thin-plate spline. The yields of the simulated signals are placed in a 2D grid as a function of their simulated masses and fit with the spline. However, fake points are added to aid the fit in order to suppress extrapolation errors at the boundaries. The fake points are made in the following way:
\begin{myenumerate}
	\item The bottom row signals (with the lowest $m_H$) have their yields duplicated and inserted $50~\GeV$ below
	\item The right column signals (with the highest $m_A$) have their yields duplicated and inserted $50~\GeV$ to the right
	\item To the left of the left-most and above the top-most points, yields are duplicated similar to the previous two bullets, unless these points give $\Delta M < 100~\GeV$
	\item In-between every point, a new point is made that uses the average of its two closest neighbors; using the left and right points for horizontal fakes as well as the lower and upper points for the vertical fakes
	\item Lastly, in-between the points on the diagonal, a new point is made using the average of its two closest diagonal neighbors
\end{myenumerate}

The yields drop significantly for $\Delta M \lesssim 150~\GeV$. Therefore, extra care is taken such that points from further away do not copy their yields towards low $\Delta M$. The fake point to the left of $(500,350)$ is discarded as this would place a high yield at $\Delta M=100~\GeV$ where the yield drops off much too quickly for the fake point to fit in. The point $(600,480)$ is also used for the diagonal point interpolations, which is otherwise only done for $\Delta M=100~\GeV$ points.

After adding the fake points, the grid is fitted with the thin-plate spline. To estimate an uncertainty on this spline, the yield from one mass point is left out to make a spline of N-1 points. The yield at the left-out mass point is then interpolated from the new spline and the relative uncertainty is calculated. This is repeated for all simulated signals. The differences between the "raw" and interpolated yields will show the power of the splines. The addition of the fake points in-between the real data points will yield very small differences in areas with little variation. For the final spline that is used later on in the analysis, all yields are used.

\begin{figure*}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\begin{center}
		\subfloat[]{\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/yieldComparisons/spline_overview_cutbased.pdf}\label{fig:signal:acceptance-interpolation-llWW}}
		\subfloat[]{\includegraphics[width=0.49\textwidth]{figures/azh/signal/llWW/llWW_fixed_scaled_withwindow/yieldComparisons/spline_500_300_cutbased.pdf}\label{fig:signal:acceptance-interpolation-500-300-llWW}}
%	\end{center}
	\caption{(a) Results of the two-dimensional thin-plate spline signal yield interpolations. The $z$-axis gives the relative differences between the raw and interpolated yields for points that are left out of the spline. The real data points are drawn in bigger boxes and annotated with their differences. The sign is kept; negative values mean overestimated yields. The smaller boxes are the fake points explained in the text. (b) The result of applying the thin-plate spline that was made to interpolate the $(500,300)$ data point. There is very little variation between the different splines, so only this one is shown.}
\end{figure*}

The uncertainties of the fits are shown in \cref{fig:signal:acceptance-interpolation-llWW}. The spline-fit works very well for the bulk of the data. For low $\Delta M$ and for low $m_A,m_H$, there are some significant differences. The points $(300,200)$ and $(400,200)$ are overestimated by $3.7\%$ and $3.6\%$, respectively. The greatest difference, $3.7\%$, is taken as the uncertainty on the normalization interpolation for all mass points. This value is much larger than the standard deviation of differences in percent (at $1.5$) and is conservative for most of the mass space.

Adding all the fake points to help the spline may make it less smooth. The spline for which the yield from the $(500,300)$ point was left out is shown in \cref{fig:signal:acceptance-interpolation-500-300-llWW}. This spline is used to interpolate the whole grid. The interpolation is smooth.

Areas with low relative uncertainty in \cref{fig:signal:acceptance-interpolation-llWW} (say, the middle parts of the triangle) correspond to areas of little color change in \cref{fig:signal:acceptance-interpolation-500-300-llWW}, meaning relatively flat yields. Conversely, at the edges and corners of the triangle, larger relative uncertainties correspond to larger changes in the yield.

This shows that the spline is relatively stable, and the larger uncertainties are in areas where the yields change drastically. This was concluded using the spline that was made by leaving out the 500,300 point, but one could use any of the “N-1” splines to reach this conclusion since they will produce figures that are very similar to \cref{fig:signal:acceptance-interpolation-500-300-llWW}.
