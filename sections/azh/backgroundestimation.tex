\chapter{Background modeling}
\emph{\small
	Now that the event selection is finalized, we will want to consider the pre-fit figures. In this chapter, we will examine the accuracy of the simulated background processes and correct the mismodeling in the transverse momentum of the reconstructed $Z$ boson as well as the transverse momentum of the leading jet.
}
\minitoc
\label{sec:backgroundestimation}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                Background estimation
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%\section{Background estimation}
\label{sec:llWW:bkg-estimation}
~\\\noindent
Figures of various variables will be shown at Levels 1 through 3 for the SR. The variables are either used in the likelihood fit later (ie. the reconstructed $A$ and $H$ masses), used to correct mismodeling, or part of the background reduction. The \pT of the $\ell\ell$ system is shown to verify the modeling after the corrections applied later in \cref{sec:llWW:bkg-estimation:zjets}. The modeling of $J$ is shown as well, since this variable was used for background reduction. During the work on reducing the mismodeling, the masses of the $W$ candidates as well as the \pT of the $H$ candidate were examined but showed no modeling issues. The variable list is:
\begin{myitemize}
	\item{\makebox[2cm]{$m_{2\ell4q}$\hfill} mass of the $A$ candidate before and after mass scaling}
	\item{\makebox[2cm]{$m_{4q}$\hfill} mass of the $H$ candidate}
	\item{\makebox[2cm]{$\pT^{\ell\ell}$\hfill} \pT of the $Z$ candidate}
	\item{\makebox[2cm]{$J$\hfill} background-reducing variable}
	\item{\makebox[2cm]{$m_{W_1}$, $m_{W_2}$\hfill} masses of the $W$ candidates}
	\item{\makebox[2cm]{$\pT^{4q}$\hfill} \pT of the $H$ candidate}
\end{myitemize}

For Levels 1 and 2, only the unscaled $m_{2\ell4q}$ is shown\footnote{Since there is no $H$ hypothesis yet.}. $J$ is shown for Level 1 and replaced by $\pT^{4q}$ in the Level 2 figure. For level 3, only blinded $m_{2\ell4q}$ is shown, unscaled and scaled.

\cref{fig:llWW:pfit-llbb,fig:llWW:pfit-ptv,fig:llWW:pfit-jvar-llbb,fig:llWW:pfit-jvar-ptv,fig:llWW:pfit-signal-log} show logarithmically and linearly scaled histograms of the data and simulated background yield with the background components color-coded. The background components are ordered by yield\footnote{This order is decided from these figures and fixed for all subsequent figures.} with the main background $Z$+jets shown on top and followed by $\ttbar$, dibosons, and an insignificant amount of single-top production. The relative differences between data and the sum of backgrounds are shown in the panel below. The systematic uncertainties entering this panel have been symmetrized through random sampling. The shape systematic is normalized to the nominal yield during the random sampling.

% as follows. For each systematic, draw $10\,000$ random numbers from a unit Gaussian. For positive numbers, add the difference between the systematic up-variation and nominal histogram in that bin multiplied by the random number  ($(h_{\mathrm{sys},i}-h_{\mathrm{nom},i})\cdot n$) to $b_i$. For negative numbers, do the same using $|n|$ instead. For the shape systematic, make $b_i^\mathrm{shape}=b_i \sum_i (h_{\mathrm{nom},i}/b_i)$.

The figures quote results for reduced $\chi^2$ two-sample and binned Kolmogorov-Smirnov (KS) tests, which are produced by the plotting code. The reduced $\chi^2$ is a poor metric for testing histogram compatibility\footnote{Strictly speaking, the test is a measure of certainty for whether two histograms originate from a common distribution.}, and the KS test cannot be done reliably on binned data\footnote{Read also ROOT's own remarks on the matter at, \url{https://root.cern.ch/doc/master/classTH1.html\#aeadcf087afe6ba203bcde124cfabbee4}.} \cite{cowan1998statistical}. Regardless, taking the quoted reduced $\chi^2$ and KS at face-value, the agreement between data and background is poor when only taking statistical uncertainty into account, and the agreement becomes suspiciously good (reduced ${\chi^2 \ll 1}$, ${\mathrm{KS}=1}$) when systematics are taken into account. This is either due to overestimated uncertainties (to cover many analyses, ATLAS prefers conservative estimates for its uncertainties, which may then be constrained by the likelihood fit) or a happy coincidence\footnote{For a back-of-the-envelope calculation, a $\chi^2$ of $3$ with $15$ degrees of freedom gives $p=99.96\%$ which one would expect $0.04\%$ of the time for data that fits the model.}.

%For some signal samples, the systematics for the $\ttbar$ shapes in $\pT^{\ell\ell}$ and mass rank highly, and hence a dedicated CR is needed even if the contribution is low. The normalisation for the minor backgrounds are taken from MC like for the $\ell\ell bb$ analysis. %The normalisations for Z+jets and $\ttbar$ are also taken from data during the fitting procedure.

Additional figures for the signal leptons and jets are shown in \cref{app:llWW:kinematicvariables} at Levels 1 and 2.

The figures show great agreement between real and simulated data. $m_{4q}$ in \cref{fig:llWW:pfit-llbb} shows a slightly growing disagreement towards lower values but is still within the systematic uncertainty at $10\%$ disagreement. $J$ in \cref{fig:llWW:pfit-ptv} shows actual disagreement at $J>0.6$. This is, however, in a region of very little background and signal as was shown in \cref{fig:azh:eventsel:allJvar}.

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\renewcommand{\figpath}{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_CRcutbased/}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_mVH_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_mVH.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_mWW_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_mWW.eps}
	\caption{The $m_{2\ell4q}$ and $m_{4q}$ mass distributions at Level 1. Logarithmic scale on the left and linear scale on the right.}
	\label{fig:llWW:pfit-llbb}
\end{figure*}

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	% Level 1
	\renewcommand{\figpath}{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_CRcutbased/}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_pTV_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_pTV.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_Jvar_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_Jvar.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_mW1_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_mW1.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_mW2_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbased_mW2.eps}
	\caption{The distributions for $p_T^{\ell\ell}$, $J$ variable, and $W$ candidate masses at Level 1. Logarithmic scale on the left and linear scale on the right.}
	\label{fig:llWW:pfit-ptv}
\end{pagefigure}

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	% Level 1
	\renewcommand{\figpath}{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_CRcutbasedJvar/}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_pTV_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_pTV.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_pTWW_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_pTWW.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_mW1_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_mW1.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_mW2_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_mW2.eps}
	\caption{The distributions for $p_T^{\ell\ell}$, $p_T^{4q}$, and $W$ candidate masses at Level 2. Logarithmic scale on the left and linear scale on the right.}
	\label{fig:llWW:pfit-jvar-ptv}
\end{pagefigure}

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	% Level 2
	\renewcommand{\figpath}{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_CRcutbasedJvar/}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_mVH_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_mVH.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_mWW_Log.eps}
	\includegraphics[width=0.48\textwidth]{\figpath C_2tag2pjet_0ptv_CRcutbasedJvar_mWW.eps}
	\caption{The $m_{2\ell4q}$ and $m_{4q}$ mass distributions at Level 2. Logarithmic scale on the left and linear scale on the right.}
	\label{fig:llWW:pfit-jvar-llbb}
\end{pagefigure}

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering 
	\subfloat[(unscaled) $m_{2\ell4q}$ in $m_H = 300$~\GeV{} window]{\includegraphics[width=0.48\textwidth]{figures/azh/llWW/prefit_plots/600_300_2tag2pjet_0ptv_SRcutbasedJvarH300/C_2tag2pjet_0ptv_SRcutbasedJvarH300_mVH.eps}}
	\subfloat[$m_{2\ell4q}$ in $m_H = 300$~\GeV{} window]{\includegraphics[width=0.48\textwidth]{figures/azh/llWW/prefit_plots/600_300_2tag2pjet_0ptv_SRcutbasedJvarH300/C_2tag2pjet_0ptv_SRcutbasedJvarH300_mVHcorr.eps}}
	\caption{The $m_{2\ell4q}$ distribution at Level 3. (b) The masses of the $\ell\ell$ and $4q$ systems are scaled to the masses of the $Z$ and the $H$ bosons, respectively. Linear scale plots are shown.}% to ease comparison of signal shapes.}
	\label{fig:llWW:pfit-signal-log}
\end{pagefigure}



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                Z+jets background
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\FloatBarrier
%\newpage
\section{Mismodeling}
\label{sec:llWW:bkg-estimation:zjets}
%The shape of the Z+jets background is taken from simulation, and the normalization is taken from the side-bands outside the $m_{4q}$ window of each SR.

%A scale factor after all event-level cuts is also calculated and shown in \cref{tab:llWW:HardestJvarAcceptEffBkg}.

The analysis suffers from mismodeling that is known\footnote{See Ref. \cite{1709.10264} or the following internal presentation on the matter, \url{https://indico.cern.ch/event/796353/contributions/3309032/attachments/1791020/2917889/2019_02_05_WJetsModelling_ColinearVeto.pdf}.} for some region of phase space in Sherpa's $Z$+jets samples. \cref{fig:llWW:Zjets:mismodelling} shows the uncorrected $p_T^{\ell\ell}$, \pT of the \pT-leading jet of the event, $m_{4q}$, and $m_{2\ell4q}$ with the corrected figures for comparison. The two transverse momenta are corrected by deriving a ratio between real and simulated data in a signal-poor control region and applying these corrections to the signal region. The corrections will be derived using the full set of backgrounds, as similar issues are seen for the \ttbar sample. The effects of the corrections on \ttbar will be covered in the next section. The effects on the diboson background cannot be examined in an isolated region, but given that the diboson samples have been generated using the same setup as for $Z$+jets, it is reasonable to assume a similar mismodeling. In any case, the dibosons (and \ttbar for that matter) are small compared to $Z$+jets and may not affect the likelihood fit with or without prior corrections.

%On top of the Z+jets mismodelling in Sherpa samples that is corrected by reweighting the \pT of the Z, the large number of selected jets lead to further mismodelling issues (especially for high-\pT jets). \cref{fig:appllWW:disagreementforXnumberofseljets} in \cref{app:llWWLowMassDisagreementChecks} compares $m_{4q}$ for regions with exactly 3, 4, or 5 selected jets ($m_{qqq}$ for 3 jets), where the \pT-leading jets are used to reconstruct the invariant mass. Here, it can be seen how the mismodelling grows with the number of selected jets.

It is possible to define an orthogonal control region to the SR with an inverted $J$ selection, that is to say $J<0.3$. The correction was derived using 2015+2016 data only. However, using data from all years shows little difference.

The correction procedure is as follows. The event is required to pass all event-level selections (like Level 1) after which the $J<0.3$ selection is applied. In this AntiJvar CR, $p_T^{\ell\ell}$ is plotted with $50$ bins from $0$ to $500~\GeV$, which is rebinned such that each bin has at least $2000$ events in data\footnote{The number, $2000$, is a compromise between having too few and too many bins but the exact value is arbitrarily chosen.}. The ratio of real to simulated data is taken and the ratio is applied bin-by-bin as weight to the simulation. Afterwards, the \pT of the \pT-leading jet of the event is plotted from $40$ to $1000~\GeV$ with $960$ bins\footnote{Remember, the event selection requires the leading jet to have $\pT>40~\GeV$.} ($1~\GeV$ wide bins), and rebin in the same manner as before. The small bin width is due to the more complex shape at low values as opposed to $p_T^{\ell\ell}$ that shows a monotonically increasing disagreement. The ratio is then like-wise multiplied bin-by-bin to the simulation weight.

The result of applying the corrections to the AntiJvar CR can be seen in \cref{fig:llWW:Zjets:CRAntiJvar}. The ratios are fitted with envelopes, and half of the fit values are applied as systematics for the mismodeling. The corrections and the fits can be seen in \cref{fig:llWW:Zjets:mismodellingZpTJet0ptRatio}, which also shows ratio for $\pT^{\ell\ell}$ using data from all years. The fit expressions and their fitted parameters are listed in \cref{tab:llWW:Zjets:mismodellingZpTJet0ptRatio}.

After applying the corrections derived in the AntiJvar CR, the mismodeling is greatly reduced in the corrected variables in the SR. For other variables, the disagreement in especially $m_{2\ell4q}$ in \cref{fig:llWW:Zjets:mismodelling} is noticeably reduced to well within the systematic band.

\begin{table}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\begin{tabular}{ ll }
			%			\hline
			%			\multicolumn{4}{c}{Channel: $\ell\ell WW$} \\
			\hline
			Variable & Expression \\
			$p_T^{\ell\ell}$ & $\begin{cases}
				0.065,                                 & \text{if } p_T^{\ell\ell} < 90~\GeV \\
				0.0941176-0.0003235294 p_T^{\ell\ell}, & \text{otherwise}
			\end{cases}$ \\
			$\pT^\mathrm{lead~jet,~evt.}$ & $\exp(-1.6-1.5\cdot 10^{-5} \pT^\mathrm{lead~jet,~evt.}) + 0.024$ \\
			\hline
		\end{tabular}
		\caption{The fitted parameters for the ratio of real to simulated data for $p_T^{\ell\ell}$ and $\pT^\mathrm{lead~jet,~evt.}$.}
		\label{tab:llWW:Zjets:mismodellingZpTJet0ptRatio}
	\end{center}
\end{table}

\FloatBarrier
\begin{pagefigure}%[hbtp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	{\centering
		\renewcommand{\figpath}{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_CRcutbasedJvar/C_2tag2pjet_0ptv_CRcutbasedJvar_}
		\includegraphics[width=0.48\textwidth]{\figpath pTVUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath pTV_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath actualeventjet0ptUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath actualeventjet0pt_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath mWWUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath mWW_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath mVHUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath mVH_Log.eps}
		\caption{Uncorrected $p_T^{\ell\ell}$, \pT of the leading jet of the event, $m_{4q}$, and $m_{2\ell4q}$ at Level 2 on the left. On the right, the corrected figures are shown for comparison. Logarithmic scales are shown.}
		\label{fig:llWW:Zjets:mismodelling}
	}
\end{pagefigure}

\begin{pagefigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	{\centering
		\renewcommand{\figpath}{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_CRcutbasedAntiJvar/C_2tag2pjet_0ptv_CRcutbasedAntiJvar_}
		\subfloat[]{\begin{tabular}[b]{c}\includegraphics[width=0.48\textwidth]{\figpath pTVUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath pTV_Log.eps}\\
		\includegraphics[width=0.48\textwidth]{\figpath actualeventjet0ptUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath actualeventjet0pt_Log.eps}\end{tabular} \label{fig:llWW:Zjets:CRAntiJvar}}
		\\
		\subfloat[]{\includegraphics[width=0.48\textwidth]{figures/azh/llWW/mismodelling/syst_ptv_allyears.png}
		\includegraphics[width=0.48\textwidth]{figures/azh/llWW/mismodelling/syst_jet0pt.png}\label{fig:llWW:Zjets:mismodellingZpTJet0ptRatio}}
		\caption{(a) The $p_T^{\ell\ell}$ (top) and \pT of the \pT-leading jet of the event (bottom) in the AntiJvar CR before (left) and after (right) applying the corrections detailed in the text. Note, no scale factor has been derived for this region and hence the ratio is not centered about $0$, and the ratio is not perfectly flat due to different binning. (b) The ratio of real to simulated data for $p_T^{\ell\ell}$ and the \pT of the \pT-leading jet in the event in the AntiJvar CR. The ratios are fitted with functions that envelope the points.}
		
	}
\end{pagefigure}

%Please note that, in the SR, the first two bins of the uncorrected \pT of the \pT-leading jet (\cref{fig:llWW:Zjets:mismodelling}) deviate significantly compared to the next few bins. This deviation is not seen in the 3-jet CR in \cref{fig:llWW:Zjets:3jetCR}, which means that the \pT of the \pT-leading jet will not be corrected in the first two (and tallest) bins, and the derived systematic will be underestimated for these two bins. 

%In \cref{fig:llWW:Zjets:mismodelling}, it can also be seen that $p_T^{\ell\ell}$ is largely unaffected by the correction (especially at low values). $p_T^{\ell\ell}$ is usually corrected for (Sherpa) Z+jets samples and was also considered for the $\ell\ell WW$ channel. However, no suitable control region was found, and it would not correct the mismodelling any better than using the leading jet \pT even with perfect flattening of the MC/data ratio. All this is covered in \cref{app:llWWmismodellingfigures}.

%The correction procedure is as follows. After all event-level cuts and the Jvar cut ("Level 2"), MC background is scaled to match the integral of data. The ratio is shown in \cref{fig:llWW:Zjets:mismodellingZpTJet0ptRatio}. The correction is applied bin-by-bin to MC. The ratio is fitted with a second order polynomial, and half of it is applied as a systematic on the shape. A mirror of the fit is used for the down variation of the systematic.

%The mismodelling issue were presented to the Weak Boson Processes subgroup of the Physics Modelling Group (PMG). While the \pT(jet0) correction derived using a 3-jet CR does not address the underlying issue of the mismodelling, PMG allowed it to serve as a proxy for the mismodelling for pragmatic reasons. For the lower-mass disagreement, PMG suggested that we make sure this is not due to missing backgrounds (e.g. multijets); see \cref{app:llWWLowMassDisagreementChecks} for these checks. Since then, we now instead derive corrections for $p_T^{\ell\ell}$ and \pT(jet0) using the AntiJvar CR which greatly reduces the lower-mass disagreement.

%Addressing the mismodelling in the very first bin of $m_{2\ell4q}$ ($200$--$240~\GeV$), the difference is outside the systematics band. We have investigated if this is due to missing background and found that this was not the case (\cref{app:llWWLowMassDisagreementChecks}), which is also supported by the fact that we see a similar effect in the TCR (\cref{fig:appllWW:disagreementforXnumberofseljetsTCR} in the appendix). Studies of MC generators have found that especially Sherpa underestimates the amount of events at low invariant mass (see for example lines 440-442 of ATLAS-STDM-2017-27-001), but as $\ttbar$ (which is based on PowhegPythia) in the TCR suffers a similar effect, this is perhaps not the culprit.

%Finally, we see that the effect is at a point where the step in statistics is very large (the next bin has more than 10 times the statistics), and thus if some resolution (or bias) effect is not modelled correctly, this could explain it. The JES and JER systematics (that haven't been pruned) are of course included in the systematics calculation, but these are based on more general considerations and could be less accurate in this special case.

%To test if this is the reason for the difference in the particular bin, we have tried different binwidths. The resulting distributions can be found in \cref{fig:llWW:Zjets:mllWWfordifferentbinwidths}. As can be seen, data moves outside the systematics band at $250~\GeV$ and below. Coincidentally, for the case with $50~\GeV$ binwidth, no disagreement outside the systematics band is seen.

%\begin{figure}
%	\centering
%	\renewcommand{\figpath}{figures/azh/llWW/rebinned_prefit_plots/}
%	\includegraphics[width=0.48\textwidth]{\figpath bkg_5_2tag2pjet_0ptv_CRcutbasedJvar/C_2tag2pjet_0ptv_CRcutbasedJvar_mVH.eps}
%	\includegraphics[width=0.48\textwidth]{\figpath bkg_10_2tag2pjet_0ptv_CRcutbasedJvar/C_2tag2pjet_0ptv_CRcutbasedJvar_mVH.eps}
%	\includegraphics[width=0.48\textwidth]{\figpath bkg_20_2tag2pjet_0ptv_CRcutbasedJvar/C_2tag2pjet_0ptv_CRcutbasedJvar_mVH.eps}
%	\includegraphics[width=0.48\textwidth]{\figpath bkg_30_2tag2pjet_0ptv_CRcutbasedJvar/C_2tag2pjet_0ptv_CRcutbasedJvar_mVH.eps}
%	\includegraphics[width=0.48\textwidth]{\figpath bkg_50_2tag2pjet_0ptv_CRcutbasedJvar/C_2tag2pjet_0ptv_CRcutbasedJvar_mVH.eps}
%	\includegraphics[width=0.48\textwidth]{\figpath bkg_60_2tag2pjet_0ptv_CRcutbasedJvar/C_2tag2pjet_0ptv_CRcutbasedJvar_mVH.eps}
%	\includegraphics[width=0.48\textwidth]{\figpath bkg_70_2tag2pjet_0ptv_CRcutbasedJvar/C_2tag2pjet_0ptv_CRcutbasedJvar_mVH.eps}
%	\includegraphics[width=0.48\textwidth]{\figpath bkg_80_2tag2pjet_0ptv_CRcutbasedJvar/C_2tag2pjet_0ptv_CRcutbasedJvar_mVH.eps}
%	\caption{$m_{2\ell4q}$ after all event-level cuts and the Jvar cut ("Level 2") with different choices of binning. The bin width is shown in the label of the y-axis. At approximately $250~\GeV$ and below, the MC/data disagreement moves outside the systematics band.}
%	\label{fig:llWW:Zjets:mllWWfordifferentbinwidths}
%\end{figure}
%
%\begin{figure}
%	\centering
%	\subfloat[2016]{
%		\includegraphics[width=0.48\textwidth]{figures/azh/llWW/prefit_plots/2016/C_2tag2pjet_0ptv_CRcutbasedJvar_eventjet0ptUnc.eps}
%		\includegraphics[width=0.48\textwidth]{figures/azh/llWW/prefit_plots/2016/C_2tag2pjet_0ptv_CRcutbasedJvar_eventjet0pt.eps}
%	}
%	\\
%	\subfloat[2017]{
%		\includegraphics[width=0.48\textwidth]{figures/azh/llWW/prefit_plots/2017/C_2tag2pjet_0ptv_CRcutbasedJvar_eventjet0ptUnc.eps}
%		\includegraphics[width=0.48\textwidth]{figures/azh/llWW/prefit_plots/2017/C_2tag2pjet_0ptv_CRcutbasedJvar_eventjet0pt.eps}
%	}
%	\\
%	\subfloat[2018]{
%		\includegraphics[width=0.48\textwidth]{figures/azh/llWW/prefit_plots/2018/C_2tag2pjet_0ptv_CRcutbasedJvar_eventjet0ptUnc.eps}
%		\includegraphics[width=0.48\textwidth]{figures/azh/llWW/prefit_plots/2018/C_2tag2pjet_0ptv_CRcutbasedJvar_eventjet0pt.eps}
%	}
%	\caption{Leading jet \pT at Level 2 before and after the correction for different years.}
%	\label{fig:llWW:jet0ptsplityear}
%\end{figure}

%In \cref{fig:llWW:jet0ptsplityear}, the \pT of the leading jet is plotted for the different years. The mismodelling at low values is identical across the years, and hence pile-up can not be the leading effect behind the data/MC disagreement.

%In conclusion, due to the high level of disagreement at $250~\GeV$ and below for $m_{2\ell4q}$, the analysis shall henceforth start at $250~\GeV$, where data lies within the systematics band. This is not an issue for the analysis since even the smallest data point (300,200) has very little of its tail below $250~\GeV$.

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%
%                Modelling of \ttbar
%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\FloatBarrier
\section{\ttbar modeling}
\label{sec:llWW:bkg-estimation:ttbar}
%Much of this section will be repetition from the $\ell\ell bb$'s \cref{sec:llbb-bkg-estimation:ttbar}.

%The $\ttbar$ shape is taken from simulation and its normalization is fitted to a dedicated TCR.

It is possible to define a region pure in \ttbar, in which the modeling of the \ttbar process can be examined. This region will also be used to correct the \ttbar normalization in the likelihood fit. Neither can be done for the dibosonic backgrounds. The TCR is defined in the same way as the signal region, with the difference that an $e\mu$ pair is required instead of a same-flavor $\ell\ell$ pair.

%The TCR is pure in \ttbar. A top scale factor is calculated for demonstration before the fit. The scale factor was shown in \cref{tab:llWW:CutbasedJvarAcceptEffBkg}.

$J$ at Level 1 is plotted in \cref{fig:llWW:topcr-2tag-2cutbased}. $m_{e\mu4q}$, $m_{4q}$, $p_T^{e\mu}$ at Level 2 are plotted in \cref{fig:azh:backgroundestimation:ttbar:mVH,fig:azh:backgroundestimation:ttbar:mqqqq,fig:azh:backgroundestimation:ttbar:pTemu}. The unscaled and scaled $m_{e\mu4q}$ at Level 3 are plotted in \cref{fig:azh:backgroundestimation:ttbar:SRL3mVH,fig:azh:backgroundestimation:ttbar:SRL3mVHcorr}. All figures contain the corrections derived in the previous section. All figures except for \cref{fig:azh:backgroundestimation:ttbar:mqqqq} show great agreement. The disagreement in \cref{fig:azh:backgroundestimation:ttbar:mqqqq} towards lower values was also seen in the uncorrected $m_{4q}$ of \cref{fig:llWW:Zjets:mismodelling}, which suggests that the derived correction was not adequate for the \ttbar process, although the systematic uncertainty is slightly smaller in this regions compared to the SR.

The effects of the corrections can be examined in \cref{fig:llWW:tcr:mismodelling}, which shows the two corrected momenta as well as the masses of the $4q$ and $e\mu 4q$ systems. While an inadequate correction is seen at about $100~\GeV$ in $\pT^{e\mu}$, the two masses and especially $m_{e\mu 4q}$ are somewhat corrected.

\begin{figure*}[htpb]
%	\vspace*{-30pt}
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\renewcommand{\figpath}{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_topemucrCRcutbased/}
		\includegraphics[width=0.45\textwidth]{\figpath C_2tag2pjet_0ptv_topemucrCRcutbased_Jvar_Log.eps}
		\includegraphics[width=0.45\textwidth]{\figpath C_2tag2pjet_0ptv_topemucrCRcutbased_Jvar.eps}
		\caption{The TCR distribution for $J$ at Level 1.}
		\label{fig:llWW:topcr-2tag-2cutbased}
	\end{center}
\end{figure*}

\begin{pagefigure}%[htpb]
	\vspace*{-30pt}
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	{\centering
		\renewcommand{\figpath}{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_topemucrCRcutbasedJvar/}
		\subfloat[(unscaled) $m_{e\mu4q}$]{\includegraphics[width=0.43\textwidth]{\figpath C_2tag2pjet_0ptv_topemucrCRcutbasedJvar_mVH_Log.eps}\includegraphics[width=0.43\textwidth]{\figpath C_2tag2pjet_0ptv_topemucrCRcutbasedJvar_mVH.eps}\label{fig:azh:backgroundestimation:ttbar:mVH}}
		\\
		\subfloat[$m_{4q}$]{\includegraphics[width=0.43\textwidth]{\figpath C_2tag2pjet_0ptv_topemucrCRcutbasedJvar_mWW_Log.eps}\includegraphics[width=0.43\textwidth]{\figpath C_2tag2pjet_0ptv_topemucrCRcutbasedJvar_mWW.eps}\label{fig:azh:backgroundestimation:ttbar:mqqqq}}
		\\
		\subfloat[$\pT^{e\mu}$]{\includegraphics[width=0.43\textwidth]{\figpath C_2tag2pjet_0ptv_topemucrCRcutbasedJvar_pTV_Log.eps}\includegraphics[width=0.43\textwidth]{\figpath C_2tag2pjet_0ptv_topemucrCRcutbasedJvar_pTV.eps}\label{fig:azh:backgroundestimation:ttbar:pTemu}}
		\\
		\subfloat[(unscaled) $m_{e\mu4q}$ in $m_H = 300$~\GeV{} window]{\includegraphics[width=0.43\textwidth]{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_topemucrSRcutbasedJvarH300/C_2tag2pjet_0ptv_topemucrSRcutbasedJvarH300_mVH.eps}\label{fig:azh:backgroundestimation:ttbar:SRL3mVH}}
		\subfloat[$m_{e\mu4q}$ in $m_H = 300$~\GeV{} window]{\includegraphics[width=0.43\textwidth]{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_topemucrSRcutbasedJvarH300/C_2tag2pjet_0ptv_topemucrSRcutbasedJvarH300_mVHcorr.eps}\label{fig:azh:backgroundestimation:ttbar:SRL3mVHcorr}}
		\caption{(a-c) The TCR distributions for $m_{2\ell4q}$, $m_{4q}$, and $\pT^{e\mu}$ at Level 2. (d-e) $m_{2\ell4q}$ at Level 3; on the right, the masses of the $\ell\ell$ and $4q$ systems are scaled to the masses of the $Z$ and the $H$ bosons, respectively. Logarithmic scale on the left and linear scale on the right.}
%		\label{fig:llWW:topcr-2tag-1cutbased}
	}
\end{pagefigure}

\begin{pagefigure}%[hbtp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	{\centering
		\renewcommand{\figpath}{figures/azh/llWW/prefit_plots/bkg_2tag2pjet_0ptv_topemucrCRcutbasedJvar/C_2tag2pjet_0ptv_topemucrCRcutbasedJvar_}
		\includegraphics[width=0.48\textwidth]{\figpath pTVUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath pTV_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath actualeventjet0ptUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath actualeventjet0pt_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath mWWUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath mWW_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath mVHUnc_Log.eps}
		\includegraphics[width=0.48\textwidth]{\figpath mVH_Log.eps}
		\caption{The TCR distributions for uncorrected $p_T^{\ell\ell}$, \pT of the leading jet of the event, $m_{4q}$, and $m_{2\ell4q}$ at Level 2 on the left. On the right, the corrected figures are shown for comparison. Logarithmic scales are shown.}
		\label{fig:llWW:tcr:mismodelling}
	}
\end{pagefigure}