\chapter{Analysis overview}
\emph{\small
%	This analysis was introduced in broad strokes in the introduction (on \cpageref{sec:intro:azh}). 
	In this chapter, we will define several concepts that will be used throughout the analysis. Then we outline the analysis from the signal signature and objects used, through the event selection and background reduction, before entering the fit model and upper limits on the production cross-section as well as the 2HDM interpretation. By the end of this chapter, the reader will have an understanding of the workflow and overall process of the analysis. All details will, of course, be presented in the respective chapters.
	% TODO: Was it introduced in broad strokes?
}
\minitoc
\label{sec:azh:samples}
~\\\noindent
The motivation for searching for the $A\rightarrow ZH$ signature stems from the need for a strong first-order phase transition in order to have electroweak baryogenesis in the early universe, and 2HDMs are able to explain the (level of) baryon asymmetry that exists today \cite{2HDMbase:1106.0034}. The choice of signal signature was motivated in \cref{sec:theory:2HDM} by examining \cref{fig:theory:2hdmheatmap}. A previous analysis \cite{LastPaper} used data recorded by ATLAS from 2015 and 2016 to set exclusions on the models from Ref. \cite{Dorsch:2014qja}. Only the $H\rightarrow bb$ channel was considered since this dominates very near alignment.

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/azh/paper/Feynman_Diagram_AZHllWW_llqqqq.pdf}
	\caption{The Feynman diagram for the process and channel considered in this analysis. From our paper.}
	\label{fig:azh:samples:feynman}
\end{marginfigure}

A second iteration of the analysis using the full Run 2 data has been done. In this analysis, models further away from alignment have been considered. $H\rightarrow WW$ takes over as the dominant decay channel in this regime, as was also discussed in \cref{sec:theory:2HDM}. As such, this analysis looks for the following signal signature:
\begin{myitemize}
%	\setlength\itemsep{0pt}
	\item A hadronically decaying\footnote{Only the fully-hadronic $WW$ decay is considered. The semi-leptonic $WW$ decay was also part of this analysis in its early stages. Due to low personpower, the semi-leptonic channel fell through when the fully-hadronic was becoming mature.} $WW$ pair (diagram in \cref{fig:azh:samples:feynman}) that comes from an $H$ boson decay where the $H$ mass range covers\footnote{Although the mass of the $H$ boson is assumed to be larger than that of the $h$ boson and can therefore be, say, $130~\GeV$, the lower limit for the $H$ mass is set to $200~\GeV$. Below this limit, effects from being near the kinematic limit of the $W$ bosons would complicate the analysis, and especially moving below $160~\GeV$ (which would mean one off-shell $W$) would require careful analysis. This would lead to this phase space being treated separately at every point of the analysis chain. It was then decided to start at $200~\GeV$ due to low personpower. A future analysis must consider moving the limit to $130~\GeV$.} %. At $m_H=130~\GeV$, the branching ratio for $H\rightarrow WW$ is only half of the BR at $m_H=160~\GeV$.}
	$200 < m_H < 700$~\GeV;
	\item an $\ell\ell$ pair ($\ell = e$ or $\mu$) that comes from a $Z$ boson decay; and
	\item the $2\ell4q$ system that comes from the $A\to ZH$ decay where ${300 < m_A < 800~\GeV}$.
\end{myitemize}

The notation used in this analysis to refer to the additional Higgs bosons is given in \cref{tab:intro:azh:def}. When referring to the theoretical or simulated particles or when defining selection windows, the capital letters are used for bosons; when referring to the reconstructed particle candidates, the particle systems (e.g. $2\ell4q$) are used. $q$ will refer to both quarks and jets reconstructed from quark-initiated showers. Jargon (especially local to this analysis) is explained in \cref{tab:intro:azh:explanations}.

\begin{pagetable}%[htpb]
	\renewcommand{\arraystretch}{1.2}%
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\subfloat[Particle names]{
			\begin{tabularx}{\linewidth}{llX}
				\toprule
				Particle & Reconstructed & Note                                               \\ \midrule
				$A$      & ${2\ell4q}$   & Neutral, CP-odd, chosen to be heavier than $H$     \\
				$H$      & ${4q}$        & Neutral, CP-even, heavier than $h$                 \\
				$H^\pm$  & -             & Charged, CP-even                                   \\
				$h$      & -             & Neutral, CP-even, assumed to be the SM Higgs boson \\
	%			Electron & $e$           &                                                    \\
	%			Muon     & $\mu$         &                                                    \\
				Lepton   & $\ell$        & Curly-l; electron or muon                          \\ \bottomrule
			\end{tabularx}%
			\label{tab:intro:azh:def}
		}
		\\[50pt]
		%  by a distribution increasingly penalizing values away from the initial value
		\subfloat[Jargon]{
			\begin{tabularx}{\linewidth}{p{120pt}X}
				\toprule
				Phrase                                              & Meaning                                                                                                                                                                                                                                                             \\ \midrule
				Fit                                                 & The act as well as the result of minimizing the (negative log-)likelihood.                                                                                                                                                                                            \\
				Floating, constrained, and\newline fixed parameters & Parameters in a model may be floating (free, unconstrained by any function), constrained (limited in range), or fixed (constant, possibly parameterized in other variables). \\
				Simulated signal                                    & Signal events generated by a Monte Carlo generator and passed through Geant4 and the ATLAS reconstruction.                                                                                                                                                          \\
				Interpolated signal                                 & Signal events generated from fits derived from the simulated signals.                                                                                                                                                                                               \\
				Window                                              & A selection on a variable with both lower and upper thresholds, ie. a window on x is $a<x<b$, where $a$ and $b$ are the lower and upper thresholds, respectively.                                                                                                   \\
				Simulated and real data                             & "Data" always refers to real data recorded by ATLAS but is prepended by "real" when simulation is mentioned in the same sentence.                                                                                                                                   \\
				$(300,200)$                                         & The masses, in \GeV, of the simulated or reconstructed signal bosons, $A$ and $H$, respectively.                                                                                                                                                                    \\ \bottomrule
			\end{tabularx}%
			\label{tab:intro:azh:explanations}
		}
	\end{center}
	\caption{(a) The Standard Model and additional 2HDM Higgs particles along with names for their reconstructed objects. (b) Jargon that the reader will come across in reading this analysis.}
	
\end{pagetable}

The event is selected by identifying a pair of electrons or muons, for which the \pT-leading lepton has triggered the event, and at least four $R=0.4$ jets\footnote{Jets were defined in \cref{sec:rec:jet}.}, from which the $H$ candidate will be formed. In the mass range considered, the jets initiated by the final state quarks do not merge until about or above the maximum mass of $A$ considered. Therefore, the analysis does not include boosted objects reconstructed with large-R jets. %The method of selection of the four signal jets among at least four jets in an event is further motivated in \cref{sec:azh:samples:topology}, which will explain how to resolve the jet combinatorics.

The signal signature consists of a real $Z$ boson and four jets. $Z$+jets will therefore be a significant irreducible background. Minor backgrounds include $\ttbar$ and $VV$.

\begin{figure}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\subfloat[Signal with large mass-splitting.]{\includegraphics[width=95pt]{figures/azh/drawings/signal_topology.pdf}}
		\hspace{8pt}
		\subfloat[$Z$+jets, main background.]{\includegraphics[width=75pt]{figures/azh/drawings/background_topology.pdf}}
		\hspace{8pt}
		\subfloat[Signal with small mass-splitting.]{\includegraphics[width=120pt]{figures/azh/drawings/signal2_topology.pdf}}
		\caption{The topologies for large mass-splitting signals, $Z$+jets, and small mass-splitting signals, respectively. In the laboratory frame, the decay products of $Z$ and $H$ are "back to back" for signals with large mass-splittings (small \dR between quarks) and much less so for signals with low mass-splittings. The $Z$ will recoil against its jets; the topology will be in-between the signals.}
		\label{fig:azh:samples:topology}
	\end{center}
\end{figure}

\begin{marginfigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	%\hspace*{-1cm}
	\includegraphics[width=\textwidth]{figures/azh/drawings/H2.pdf}
	\caption{A drawing of $H_2$ for signals with three different mass-splittings with the background overlaid in orange.}
	\label{fig:azh:samples:H2drawing}
\end{marginfigure}

The signal topology is shown in \cref{fig:azh:samples:topology}. It depends on the mass-splitting \({\Delta M=m_A-m_H}\). For large mass-splittings, the momenta carried by the decay products of $Z$ or $H$ will be in the same direction as the decaying particle, meaning that the transverse momenta of the decaying particles will be close to the sum of the transverse momenta of the decay products. This is sketched in \cref{fig:azh:samples:H2drawing} for the variable $H_2$, which will be introduced in \cref{sec:azh:eventsel:resolvingcomb}. As such, depending on the \emph{reconstructed masses} of the $4q$ or $2\ell4q$ systems, the signal can be selected from the expected $H_2$ value.

Only the gluon--gluon fusion (normally shortened ggF, here shortened ggA) signal is considered for this channel. To cover the $b$-associated production (bbA), yet another jet (or two) would have had to be selected. Considering the level of mismodeling with already 4 jets\footnote{The mismodeling will be covered in \cref{sec:backgroundestimation}.}, bbA will require its own dedicated study. New Sherpa $V$+jets samples are being produced by ATLAS, which might have improved modeling at large jet multiplicity\footnote{Alternatively, it would be necessary to entirely switch generator.}.

Since bbA is not considered, and ggA mostly produces central jets, forward jets are not included in this analysis.

All quark flavors are included, meaning that $\ttbar$ will contribute less compared to the main background, $Z$+jets. If jets initiated by b-quarks were identified (to identify bbA), $\ttbar$ would contribute more and therefore $\ttbar$ specific selections could be employed. Conversely, since bbA is not considered, b-quarks could be identified and suppressed as the signal would suffer little loss due to $|V_{qb}|\approx 4\%$. This was not done in order not to introduce systematic uncertainties from b-tagging.

The four jets in the final state present some issues regarding signal selection. The jet combinatorics is resolved by considering pairs of jets and whether they originate from $W$ bosons.

After the initial event and signal selections, the background is reduced by applying a further selection, which considers the full signal topology. The background will generally have lower \pT but higher $m_{2\ell4q}$ due to the large angular separation. The signal, however, will have a higher \pT that correlates with the $m_{2\ell4q}$. This can be exploited by the $J$ variable\footnote{Pronounced "Jvar".}, which is defined as,
\begin{align*}
J \equiv \frac{ \sqrt{ p_T^2(\ell_1) + p_T^2(\ell_2) + p_T^2(q_1) + p_T^2(q_2) + p_T^2(q_3) + p_T^2(q_4) } } { m_{2\ell4q}  },
\end{align*}
where $\ell$ and $q$ are the selected leptons and jets, respectively. This variable was chosen because the signals, independent of their masses, produce very similar distributions that are separable from the background. This allows for a flat selection to be applied to any signal regardless of the $A$ and $H$ masses.

A final step of the selection is the $m_{4q}$ window. A window moving in steps of $10~\GeV$ along $m_{4q}$ selects $H$ candidates from compatible signal hypotheses, e.g. a $(600,300)$ signal will have a window around $m_{4q}=300~\GeV$. The width of the window is optimized to maximize signal significance. The window is optimized for narrow-width $H$ only, which is acceptable for large parts of the phase space as was mentioned by the end of \cref{sec:theory:2HDM}.

After the selections, the reconstructed $A$ mass, $m^{\text{cor}}_{2\ell4q}$, is fitted for each signal hypothesis. The variable $m^{\text{cor}}_{2\ell4q}$ is defined as the mass of the $2\ell4q$ system, in which the $\ell\ell$ and $4q$ four-vectors are scaled such that they reproduce the mass of the $Z$ and $H$ bosons, respectively. This rescaling is not applied before the $m_{4q}$ window because the $H$ hypothesis is not yet decided before entering a window.

%As the event selection reaches the last few selections, the signal region is becoming defined.
The signal region is defined at three levels with increasing signal significance. The levels are:
\begin{myitemize}
	\item Level 1: Initial event selection and resolved jet combinatorics
	\item Level 2: The $J$ variable selection
	\item Level 3: A $m_{4q}$ window
\end{myitemize}
The signal regions are named "CRcutbased" at Level 1, "CRcutbasedJvar" at Level 2, and "SRcutbasedJvarH300" at Level 3 for an $m_{4q}$ window at $300~\GeV$.

At levels 1 and 2, the signal is considered diluted by the background, and the analysis is as such effectively blind. Therefore, the distributions before the $m_{4q}$ windows are shown unblinded. After entering an $m_{4q}$ window, the analysis is explicitly blinded.

Several regions are defined at Level 3. Apart from the signal regions, two regions enriched with their respective backgrounds are defined. The regions are discriminated as follows:
\begin{myenumerate}
	\item Signal regions (SRs): Same-flavor leptons
	\item Side-bands: Same-flavor leptons with inverted $m_{4q}$ window
	\item Top control regions (TCRs): Different-flavor leptons
\end{myenumerate}

There is one SR, side-band, and TCR \emph{per $H$ hypothesis}. This is sketched in \cref{fig:azh:samples:regiondef}. The window will slide in steps of $10~\GeV$, and regions will therefore overlap. For the range $200<m_H<700$ and $m_H+100<m_A<800~\GeV$, this gives $1326$ signal regions with accompanying control regions. The control regions will only be used to constrain the $Z$+jets and \ttbar normalizations, which will be left floating in the eventual likelihood fit. Since only normalization information is required, the bins of the control regions will be combined into 1 bin each during the fit.

\begin{figure*}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\includegraphics[width=0.49\textwidth]{figures/azh/drawings/regiondef.pdf}
		\includegraphics[width=0.49\textwidth]{figures/azh/drawings/regiondef2.pdf}
		\caption{Left: A picture of the region definitions for one $m_{4q}$ window. The SR and TCR are within the window but have same and different lepton flavors, respectively. The side-band covers all of $m_{4q}$ outside the window. Right: A second window drawn on top of the first window (and slightly raised to help see the difference; the lepton flavors are unchanged). The SR of the first window is partly contained within the side-band and SR of the second window.}
		\label{fig:azh:samples:regiondef}
	\end{center}
\end{figure*}

This analysis is separated into the following chapters, whose contents are shortly listed as well:
\begin{myitemize}%[leftmargin=0pt,listparindent=0pt,labelwidth=0pt,itemindent=0pt]
	\item[\textbf{\nameref*{sec:azh:obj}:}] Define leptons and jets used in analysis and apply preselection during the production of calibrated input samples.
	\item[\textbf{\nameref*{sec:eventsel}:}] Apply event selections to identify signal signature; select signal jets; apply further selections using the $J$ variable to reduce background; and enter the signal regions using the $m_{4q}$ windows.
	\item[\textbf{\nameref*{sec:llWW:bkg-estimation}:}] Show various variables for the simulated backgrounds and real data; evaluate the modeling accuracy of the simulated backgrounds; and apply corrections.
	\item[\textbf{\nameref*{sec:signalmodeling}:}] Fit the simulated signals and interpolate the fit parameters; after this point, the simulated signals are replaced by these interpolated fits, which can be made for any arbitrary mass point ($m_A$,$m_H$).%, $m_{2\ell4q},m_{4q}$
	\item[\textbf{\nameref*{sec:systs}:}] List the systematic uncertainties stemming from the detector, reconstruction, and modeling.%; the list of the systematic uncertainties will be reduced before the final fits
	\item[\textbf{\nameref*{sec:azh:fitmodel}:}] Introduce the likelihood function along with its nuisance parameters; evaluate the fit using the simulated background and test in real data outside the signal region; and evaluate the fit model in the signal region with unblinded data.
	\item[\textbf{\nameref*{sec:azh:results}:}] Apply the fit model to real data; calculate limits for narrow-width and large-width signals; and interpret limits on large-width signals in 2HDM.% to set limits on some parts of the phase space
	\item[\textbf{\nameref*{sec:azh:discussion}:}] Discuss several issues discovered during the analysis work.
\end{myitemize}

\section{Data and simulated samples}
\label{sec:azh:samples:samples}
The analysis uses LHC-delivered data from $\sqrt{s}=13\, \TeV$ proton--proton collisions recorded by the ATLAS detector between 2015 and 2018 with a total of $139~$fb$^{-1}$ of usable integrated luminosity. \cref{fig:azh:samples:dataandsimul:azhtotalintegrated} shows the LHC-delivered and ATLAS-recorded data as well as actual useful data selected by the GRL \cite{DAPR-2018-01} where all parts of the detector were working.

\begin{marginfigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\begin{center}
		\includegraphics[width=\textwidth]{figures/outside/totalintegratedluminosity.pdf}
		\caption{Instantaneous luminosity delivered by LHC, recorded by the ATLAS detector, and useful for physics analysis (passing the GRL). Adapted from \cite{DAPR-2018-01}.}
		\label{fig:azh:samples:dataandsimul:azhtotalintegrated}
	\end{center}
\end{marginfigure}

Signal and background processes are simulated using various Monte Carlo generators with some generators only simulating the productions and then passing the event to others for fragmentation and hadronization simulations. The details for the specific processes will be given below. All background processes are passed through Geant~$4$~\cite{Agostinelli:2002hh} to simulate the traversal of particles through the ATLAS detector~\cite{SOFT-2010-01} after the hadronization step. Signal samples are passed through the much faster ATLFAST-II~\cite{SOFT-2010-01}, which has great accuracy and precision for the needs of the analysis\footnote{ATLFAST-II gains its speed improvement from having parameterized the response of ATLAS calorimeters without having to simulate every detail. Jet substructure, forward jets using the FCAL, among others, may be areas where ATLFAST-II is lacking, but these will not be necessary for this analysis.}, in order to save time, so that more signal samples (more $A$ and $H$ hypotheses) could be produced. Simulated events undergo \emph{digitization}, which converts the output into detector response digits, making the output of the Geant4 or ATLFAST-II identical to data in structure (with additional truth information). After this, simulated and collision events undergo the same reconstruction.

Signal, only produced from gluon--gluon fusion, is generated by MadGraph5\_aMC@NLO~$2$.$3$.$3$ \cite{Alwall:2011uj,Alwall:2014hca} at LO with parton showering handled by Pythia~$8$.$210$~\cite{Sjostrand:2014zea} with the A$14$ tune~\cite{ATL-PHYS-PUB-2014-021} and NNPDF$2$.$3$ PDF set~\cite{Ball:2012cx}.

$Z$+jets decaying leptonically (including taus) is generated by Sherpa~v.$2$.$2$.$1$ \cite{Bothmann:2019yzt} using matrix elements (ME) at next-to-leading order (NLO) for up to two partons and LO for up to four partons with Sherpa's own tune and the NNPDF$3$.$0$nnlo PDF set~\cite{Ball:2014uwa}. The ME are calculated by the Comix~\cite{Gleisberg:2008fv} and OpenLoops~\cite{Cascioli:2011va,Denner:2016kdg} libraries. The ME are matched to the Sherpa parton shower~\cite{Schumann:2007mg} using the MEPS@NLO prescription~\cite{Hoeche:2011fd,Hoeche:2012yf,Catani:2001cc,Hoeche:2009rj}. The samples are normalized to a next-to-next-to-leading order (NNLO) prediction~\cite{Anastasiou:2003ds}.

Dibosons decaying semi-leptonically ($WW$, $ZZ$, and $WZ$) are generated by the same generator as for $Z$+jets and with the same parameters.

\ttbar\ is generated by PowhegBox~v$2$~\cite{Frixione:2007nw,Nason:2004rx,Frixione:2007vw,Alioli:2010xd} at NLO with the NNPDF$3$.$0$nlo PDF set \cite{Ball:2014uwa} and the parameter\footnote{$h_\mathrm{damp}$ is one of the parameters that regulates the high-\pt\ radiation, which the \ttbar\ system recoils against.} $h_\mathrm{damp}$ set to $1.5$~$m_{\mathrm{top}}$~ \cite{ATL-PHYS-PUB-2016-020} with parton showering, hadronization, and the underlying event handled by Pythia~$8$.$230$ with the $A$14 tune and NNPDF$2$.$3$ PDF set. Bottom and charm hadron decays are simulated by EvtGen~v$1$.$6$.$0$~\cite{EvtGen}.

Top quark in association with a $W$ ($tW$) and single-top $s$-channel are generated by PowhegBox~v$2$~\cite{Re:2010bp,Nason:2004rx,Frixione:2007vw,Alioli:2010xd,Alioli:2009je} at NLO in \alphas\ using the five-flavor scheme and the NNPDF$3$.$0$nlo PDF set. Interference and overlap between \ttbar\ and $tW$ are removed through diagram removal~\cite{Frixione:2008yi}. Parton showering is handled by Pythia~$8$.$210$ with the A$14$ tune and NNPDF$2$.$3$lo PDF set. Bottom and charm hadron decays are simulated by EvtGen v1.2.0.

Inelastic proton--proton collisions are generated by Pythia $8$.$186$ with the A$3$ tune~\cite{ATL-PHYS-PUB-2016-017} and NNPDF$2$.$3$lo PDF set to simulate the pileup of the simulated samples. This pileup serves as the multiple interactions in the current bunch-crossing and bunch-crossings before and after. The pileup is normalized to the average number of interactions per bunch crossing $\left\langle\mu\right\rangle$ observed in data and is overlaid the simulated samples at the digitization step.

The only background processes used in this analysis are $Z$+jets ($>\nobreak88\%$ of total background depending on region), \ttbar+single-top (up to $7\%$), and semi-leptonic dibosons (up to $6\%$). Smaller backgrounds, such as $W$+jets, $\ttbar V$, and $Vh$\footnote{Remember, the lowercase $h$ refers to the SM Higgs boson.}, contributed less than $1.0\%$ of the total background and have been pruned from the analysis. $W$+jets would be the most significant source of fake leptons but its contribution is negligible. Multi-jet contamination is presumed negligible since two loosely identified leptons with rather large \pT are required, and the leptons must lie in the range of the $Z$ mass. This was specifically tested in the two-lepton channel of the $Vh$ analysis \cite{EXOT-2016-10}, which has an event selection similar to this analysis but looser \pT requirements on the leptons. They found negligible contributions from multi-jets after the event selection.

%On top of this, the cross-section for multi-jet events with 6 jets (where 2 of them fake the same lepton flavours) is negligible. Contributions from fakes (mainly W+jets) is also lower for this channel due to its higher cut on \pT of the sub-leading lepton.

%The backgrounds are grouped as follows:
%\begin{myitemize}
%	\setlength\itemsep{-5pt}
%	\item $Z$+jets
%	\item \ttbar
%	\item Dibosons/VV: Semi-leptonic $WW$, $WZ$ and $ZZ$.
%	\item Single-top: Single-top $s$-channel and $t$ produced in association with a $W$.
%\end{myitemize}

%\section{Signal and background topology}
%\label{sec:azh:samples:topology}


%Fix this paragraph:
%The variables used for resolving jet combinatorics were introduced in the text and used to discriminate between correctly and wrongly made combinations. The selections also help reduce background. The signal and background topologies will now be introduced and used to defend the choice of discriminating variables. The discriminating variables will thereafter be shown for correct and wrong combinations for some signals. Finally, the variables will be shown after resolving jet combinatorics for background and some signals.

%\begin{marginfigure}
%	\begin{center}
%		\includegraphics[width=0.70\textwidth]{figures/azh/drawings/signal_topology.pdf}
%		\caption{The signal topology for signals with large mass-splittings. In the laboratory frame, the decay products of Z and H are "back to back".}
%		\label{fig:appllWW:signaltopology}
%	\end{center}
%\end{marginfigure}
%
%\begin{marginfigure}
%\begin{center}
%	\includegraphics[width=0.70\textwidth]{figures/azh/drawings/signal2_topology.pdf}
%	\caption{The signal topology for signals with small mass-splittings. In the laboratory frame, the decay products of Z and H are more evenly spread out.}
%	\label{fig:appllWW:signal2topology}
%\end{center}
%\end{marginfigure}
%
%\begin{marginfigure}
%	\begin{center}
%		\includegraphics[width=0.70\textwidth]{figures/azh/drawings/background_topology.pdf}
%		\caption{For the main background, Z+jets, highly energetic Z bosons will recoil against their jet systems. Due to the spread of }
%		\label{fig:appllWW:backgroundtopology}
%	\end{center}
%\end{marginfigure}

%In \cref{fig:appllWW:H2W1dRcombinationscomparison}, $H_2$ and $\Delta R(W_1)$ for the 800,700 sample are shown for selected combinations with $m_{4q}$ within $10~\GeV$ of the simulated mass, all combinations with $m_{4q}$ within $10~\GeV$ of the simulated mass, and simply all combinations. Even within the mentioned mass window, many combinations will have widely different $H_2$ and $\Delta R(W_1)$.

%\begin{figure}[htbp]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
%	%\hspace*{-1cm}
%	\includegraphics[width=0.48\textwidth]{figures/azh/llWW/cutbased/fitres/310819/plot_H2.pdf}
%	\includegraphics[width=0.48\textwidth]{figures/azh/llWW/cutbased/fitres/310819/plot_W1dR.pdf}
%	\caption{$H_2$ and $\Delta R(W_1)$ for the 800,700 sample. In black, all the combinations that are selected by the cutbased approach and have $m_{4q}$ within $10~\GeV$ of the simulated mass are shown. In comparison, all combinations with the same mass cut are shown in green, and all combinations with no mass cuts are shown in red.}
%	\label{fig:appllWW:H2W1dRcombinationscomparison}
%\end{figure}

%Many wrong combinations have very large $\Delta R$ between the jets of their $W$ candidates, and sometimes also large masses for these%, as can be seen in \cref{fig:appllWW:W1W2manddRcombinationscomparison}.

%\begin{figure}[htbp]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
%	\centering
%	%\hspace*{-1cm}
%	\includegraphics[width=0.48\textwidth]{figures/azh/llWW/cutbased/fitres/310819/plot_W1mW2m_2.png}
%	\includegraphics[width=0.48\textwidth]{figures/azh/llWW/cutbased/fitres/310819/plot_W1dRW2dR_2.png}\\
%	\includegraphics[width=0.48\textwidth]{figures/azh/llWW/cutbased/fitres/310819/plot_W1mW2m_1.png}
%	\includegraphics[width=0.48\textwidth]{figures/azh/llWW/cutbased/fitres/310819/plot_W1dRW2dR_1.png}\\
%	\includegraphics[width=0.48\textwidth]{figures/azh/llWW/cutbased/fitres/310819/plot_W1mW2m_0.png}
%	\includegraphics[width=0.48\textwidth]{figures/azh/llWW/cutbased/fitres/310819/plot_W1dRW2dR_0.png}\\
%	\caption{The left column shows the masses of the $W$ candidates plotted against each other, and the right column shows the $\Delta R$ of their jets plotted against each other. The first row shows all combinations, the second row shows all combinations with $m_{4q}$ within $10~\GeV$ of the simulated mass, and the third row shows all combinations that are selected by the cutbased approach.}
%	\label{fig:appllWW:W1W2manddRcombinationscomparison}
%\end{figure}

%\begin{figure}[htbp]
%	{\centering
%		\includegraphics[width=0.70\textwidth]{example-image-a}
%		\caption{To come: signal and background comparison for variables that resolve jet combinatorics.}
%		\label{fig:appllWW:sigbkgcombinationforcutbasedvars}
%	}
%\end{figure}

%\section{2HDM simulations}
