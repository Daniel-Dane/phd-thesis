\chapter{Discussion}
\emph{\small
	The analysis has been completed, and we have set exclusions on the type-I 2HDM model. We will discuss some issues that were stumbled upon during the analysis work.
}
%\minitoc
\\~\\
\label{sec:azh:discussion}

\noindent The analysis is a second iteration that now uses the full Run 2 data as well as a second channel that covers 2HDM models away from alignment. The $\ell\ell bb$ channel has no further sub-channels and therefore selects directly the two leptons and $b$-tagged jets. The $\ell\ell WW$ channel presented in this analysis has to reconstruct the $W$ bosons from their decay products. The branching ratios are about one-third to leptons and two-thirds to hadrons~\cite{PDG}. Therefore, fully leptonic $WW$ decays happen in about $10$ percent of decays, while branching ratios for fully-hadronic and semi-leptonic are about $45$ percent each. At the start of the analysis, the $\ell\ell WW$ channel originally contained both the fully-hadronic and the semi-leptonic sub-channels in order to cover the about $90$ percent of the branching ratios. While the sensitivity of the fully-hadronic channel is affected by the size of the hadronic background, the semi-leptonic decay loses sensitivity due to the one neutrino. The lost sensitivities would be somewhat regained by employing the parameterized neural network (pNN)~\cite{Baldi:2016fzo} that has been implemented by other analyses~\cite{ATLAS-CONF-2020-039,CMS-PAS-HIG-17-006}.% in the ATLAS Higgs and Diboson Searches (HDBS) working group of which this analysis is a member.

Possible improvements considered but not used in the analysis in this iteration are listed below. These can also be seen as suggested alleys of improvement for future analyses.

\paragraph{Use of machine learning to remove background}
\begin{marginfigure}%[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\subfloat[Neural network with contextual features]{\includegraphics[width=\textwidth]{figures/outside/pNN_descr.pdf}\label{fig:azh:discussion:pnn_paper_perf:a}}
	%	\hspace*{10pt}
	\\
	\subfloat[Performance of pNN vs regular NN]{\includegraphics[width=\textwidth]{figures/outside/pNN_perf.pdf}\label{fig:azh:discussion:pnn_paper_perf:b}}
	\caption{(a) Individual regular NNs that are separately trained on sets of data with $\theta=\theta_a$ and $\theta=\theta_b$ can be combined into a single NN trained on the full set with $\theta$ as an input feature. (b) The ROC curves for a regular NN trained only on a single mass point and pNNs that are trained on either the full set or the full set except for the single mass point, thereby forcing it to interpolate. The performance is identical between all three cases. Both from~\cite{Baldi:2016fzo}.}
%	\label{fig:azh:discussion:pnn_paper_perf}
\end{marginfigure}
Actually, a working pNN model has been trained and evaluated on nominal simulated data for this analysis, mostly in the $\ell\ell bb$ channel. A pNN is a neural network (NN) that incorporates the context into its feature set; in this case, the network is parameterized by the signal masses $m_A$ and $m_H$, which are added along side the input features as shown in \cref{fig:azh:discussion:pnn_paper_perf:a}. The masses of all the input signal samples are added to a histogram from which random values are sampled and added to the background in order to remove any false discriminatory power on the signal masses themselves. The end result is a single network with the same power as NNs trained on individual mass points with the additional powerful ability to interpolate to unseen mass points. An example of the power of interpolation is shown in~\cref{fig:azh:discussion:pnn_paper_perf:b}.

For the architecture of the pNN in this analysis, the network is constructed by 5 layers of 800 nodes each with the ReLU activation function and $20$ percent dropout after each layer. The output layer is one node with a sigmoid function. The binary cross-entropy is used to calculate the loss, and the model is optimized by use of the stochastic gradient descent. The model has been trained with a batch size of $128$ for $30$ epochs. The input features are the kinematic variables $\pT$, $\eta$, and $\phi$ for the two leptons and three leading jets as well as the reconstructed $m_{bb}$, $m_{\ell\ell bb}$, and $m^{\text{cor}}_{\ell\ell bb}$ and the simulated masses $m_A$ and $m_H$. The performance of the pNN model is evaluated by the significance (cf.~\cref{eq:azh:eventsel:Jvar:significancedef}) calculated using wide bins of signal and background to emulate the binning mechanism presented in \cref{sec:azh:fitmodel:likelihood}. For each simulated signal, the selection on the pNN score that yields the largest significance is used. \cref{fig:azh:discussion:pnn_perf} shows the relative improvement in the significance after applying the most significant selections on the simulated $\ell\ell bb$ signals; the expected significance would be at least about $2$ times higher compared to the combined $J$ selection and $m_{bb}$ window.
\begin{figure}[htbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi
	\centering
	\includegraphics[width=\textwidth]{figures/azh/ML/new_dimful_nogbr_recomass_signiimprov_max.pdf}
	\caption{The relative improvement in significance when selecting signal events using the pNN compared to the combined $J$ selection and $m_{bb}$ window in the \llbb channel.}
	\label{fig:azh:discussion:pnn_perf}
\end{figure}

Unfortunately, due to time constraints, the pNN could not be included in later stages of the analysis, and so the actual improvements remain to be seen\footnote{However, several analyses reported similarly positive results with pNNs at the ATLAS Exotics + HDBS Workshop 2019, which ATLAS members can find at \url{https://indico.cern.ch/event/801402/}.}.

\paragraph{Issues with jet combinatorics}
The fully-hadronic $\ell\ell WW$ sub-channel has four jets in the final state, which need to be selected correctly among the pileup jets. The simplest signal selection would be to choose the 4 \pT-leading jets in the event. The more advanced technique presented in this analysis deals with the combinatorics of selecting jet pairs from each $W$ boson. As stated in \cref{sec:eventsel}, the advanced technique is better for every mass point. However, the solution is not elegant and suffers from several issues, some of which were mentioned in \cref{sec:azh:eventsel:resolvingcomb:apply}. The "correct combinations" were determined using the "base selection" in the iterative method in \cref{sec:azh:eventsel:resolvingcomb:derive} instead of using proper truth-matching. This is in part due to the lack of the necessary truth particles in the CxAOD files, insufficient truth information saved on the reconstructed objects (ie. only the absolute value of the PDG ID is saved!), and finally because truth-matching (using the available information) for the lower masses proved difficult. Instead of parameterizing the three variables $\dR(W_1)$, $\dR(W_2)$, and $H_2$ in $f(m_A,m_H)$, their windows could be derived from fits to errors bars drawn in 2D histograms. The methodology itself could also be changed from making windows from the 95\% error bars to windows that are optimized by use of selections that maximize significance as was done for the $J$ and $m_{\ell\ell}$ optimizations. The analysis considered only the 5 \pT-leading jets in the event, and more careful combinatorics resolutions could include more jets.

Actually, before the $\ell\ell WW$ signal samples used in this analysis were generated, three representative samples had been made to gauge the efficacy of the channel. These three samples were included in a simple boosted decision tree (BDT), which used correct combinations as signal and wrong combinations as background. The BDT was not included in the analysis, as it was difficult to evaluate its performance. With the full list of datasets available, a proper machine learning algorithm that deals with combinatorics must be considered, possibly even as part of the pNN.

\paragraph{Background modeling}
The issues of background modeling have been covered well. Here follows additional thoughts. The \pT mismodeling seen for the $Z$+jets samples depends strongly on the number of jets in the event. For fewer than $4$ jets with $\pT>20~\GeV$, little mismodeling is seen. Events with $5$ jets show significantly greater mismodeling compared to events with $4$ jets, and this mismodeling worsens for higher multiplicity. The correction applied to the $\pT^\mathrm{Z}$ distribution has been inclusive in the number of jets. While the new generation of $V$+jets samples will likely show better modeling, the mismodeling can be better corrected if the correction is done exclusively per number of jets. The modeling of the $J$ variable was not corrected, even as higher values showed mismodeling outside the systematics band. The variable was corrected in the $\ell\ell bb$ channel. After the fit to data, post-fit figures of $\pT^\mathrm{Z}$ and $J$ did show little disagreement between real data and simulation, but finding and correcting the mismodeling in these variables before fitting would be preferable.

\paragraph{Limited scope}
The $\ell\ell WW$ channel unfortunately only contained the ggA production. Had there been additional personpower available for the channel, the bbA production mechanism could have been included, and the results could have been interpreted in the type-II as well.

The fully-hadronic channel for ${H\rightarrow ZZ}$ was not included even though this would likely not add much work given the similarity to $WW$. Actually, the $H\rightarrow WW$ signal efficiency could have been adjusted by examining the contamination from a few representative $H\rightarrow ZZ$ without running the full analysis on many $H\rightarrow ZZ$ samples. The exclusions are therefore slightly conservative in this regard.

\paragraph{Summary}
To summarize the experiences with the $\ell\ell WW$ channel, a future iteration on this analysis should:
\begin{myitemize}
	\item Include the semi-leptonic channel,
	\item lower minimum $H$ mass from the current $200~\GeV$,
	\item use the pNN for selection,
	\item as well as, following \cref{tab:systematicsllWW} in descending order of importance,
	\begin{myitemize}
		\item use particle flow jets to decrease the JES/JER systematics\footnote{This is already required in ATLAS. Also, if memory serves, one can expect about half the JES/JER uncertainty at low \pT using the PFlow jets.},
		\item simulate more events per sample, and
		\item use newer $Z$+jets samples with less mismodeling at high jet multiplicity.
	\end{myitemize}
\end{myitemize}

The expected sensitivity for the type-I 2HDM model was presented in \cref{sec:theory:2HDM}. The exclusions on the production cross-section times the branching ratios presented in \cref{sec:azh:results:lw} are in agreement with expectations. To increase sensitivity for off-alignment searches, the proposed improvements might not even be sufficient to cover heavier $m_H$ bosons, if the branching ratios for $A{\rightarrow\ttbar}$ and possibly ${H\rightarrow hh}$ become so dominating after ${m_H=250~\GeV}$. However, the expected parameter space covers relatively low $m_H$ (cf.~\cref{fig:theory:2hdmbr}), and the improved sensitivity may still exclude higher $m_A$ and ${\tan\beta}$, which these results only moderate exclude.

Run 3 of the LHC has so far been delayed to around 2022 due to the current world situation, but will never-the-less eventually deliver about $300~\mathrm{fb}^{-1}$ of integrated luminosity with $14~\TeV$ of center-of-mass energy. The high-luminosity LHC will some day deliver ten times as much integrated luminosity. The suggested improvements and the additional integrated luminosity open the door for new rare processes, but the $A\rightarrow ZH$ channel of the 2HDM models remains a strong contestant for finding new Higgs bosons, whose existence may be used to explain that the observed baryon asymmetry is due to the electroweak phase transition in the early universe.