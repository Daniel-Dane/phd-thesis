\chapter{The ATLAS experiment}
\emph{\small
	In this chapter, we will go through a short introduction to the Large Hadron Collider (LHC), on which ATLAS is one of four main detectors. Then we will see an overview of the ATLAS detector and delve into details regarding the relevant sub-detectors for this thesis.
}
\minitoc
\label{sec:atlas}

\begin{figure*}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\subfloat[The CERN detector complex]{\includegraphics[width=0.49\linewidth]{figures/outside/cerncomplex.png}\label{fig:atlas:cerncomplex}}
	\subfloat[The ATLAS experiment]{\includegraphics[width=0.49\linewidth]{figures/outside/ATLAS_SE_Corrected7.pdf}\label{fig:atlas:atlas}}
	\caption{(a) The CERN detector complex picturing the accelerators and connected experiments. The protons in the LHC ring are created in a linear accelerator and passed to several circular accelerators before entering the LHC. Adapted from \cite{Mobs:2684277}. (b) The ATLAS detector with its sub-detectors and magnet systems. From \cite{Aad:1129811}.}
\end{figure*}

~\\\noindent % TODO: N bunches, their size, design lumi+energy, runs?
The Large Hadron Collider (LHC), located at CERN, is a circular particle accelerator that accelerates counter-rotating beams of protons to $6.5$~TeV each (or ions at lower energies) and brings them to collision in the four main experiments located on the ring \cite{Aad:2020wji}. The LHC can be seen in \cref{fig:atlas:cerncomplex}, in which it accepts accelerated particles from the SPS with energies of $450~\GeV$ in case of protons. During normal operations, the LHC cycles between injection (filling of ring), ramp (increase of energies), squeeze (reduction of beam size), stable beams (collisions which typically lasts $10$-$15$ hours), and dump followed by ramp down (the beam is diverted into the cavern and the magnets are ramped down). The protons are clumped together in \emph{bunches} with $1.1\cdot 10^{11}$ protons each. There is about $2500$ colliding bunches $n_b$ in the ring during operation. $n_b$ as well as the beam focusing $\beta^*$ at the interaction points increased during Run 2~\cite{ATLAS-CONF-2019-021}. The numbers for $n_b$ and $\beta^*$ and their influence on the luminosity and hence pileup are listed in \cref{tab:atlas:lhcdata} per year. The $50$~ns bunch spacing run of 2015 and the high-pileup run during 2017 are not included.

\begin{table}[thbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\begin{tabularx}{\textwidth}{Xllll}
		\toprule
		Parameter                                         & 2015 & 2016 & 2017 & 2018      \\ \midrule
		$n_b$                                             & 2232 & 2208 & 2544 & 2544      \\
		$\beta^*$ (m)                                     & 0.8  & 0.4  & 0.3  & 0.3--0.25 \\
		Peak $\mathcal{L}(t)$ ($10^{33}\,$cm$^{-2}$s$^{-1}$) & 5    & 13   & 16   & 19        \\
		Approximate peak $\left\langle\mu\right\rangle$   & 16   & 41   & 45   & 55        \\ \bottomrule
	\end{tabularx}
	\caption{Some beam parameters leading to larger instantaneous luminosity and therefore pileup. When the instantaneous luminosity is converted to pileup a reference inelastic cross-section of $80$~mb is assumed. From \cite{ATLAS-CONF-2019-021}.}
	\label{tab:atlas:lhcdata}
\end{table}

The ATLAS detector\footnote{ATLAS is formed from the silly backronym, A Toroidal LHC ApparatuS.} is a general-purpose detector \cite{Aad:1129811} placed on one of the four interaction points of the LHC. The cylindrically shaped detector is also forward-backward symmetric along the beamline. It consists of a middle, or \emph{barrel}, part and two end parts, named \emph{end-caps}. For the cylindrically shaped barrel, particles travel mainly transverse to the beam pipe through sub-detectors. For the end-caps, on the other hand, the sub-detectors are constructed as disks around the beam pipe, and particles will travel mostly transverse to the disks. The detector mainly consists of three sub-detectors: the inner detector that is responsible for tracking charged particles, the calorimeters that are responsible for stopping particles and measuring their energies, and finally the muon spectrometer that measures the muon momenta\footnote{Muons are the only \emph{detectable}, fundamental particles that escape the calorimeters. Neutrinos escape the detector without ever being detected directly.}. The sub-detectors and magnet systems are pictured in \cref{fig:atlas:atlas}, which shows the inner detector (Pixel detector, Semiconductor tracker (SCT), Transition radiation tracker (TRT)), the solenoid magnet, the calorimeters (LAr electromagnetic and tile calorimeters in the barrel, LAr hadronic end-cap and forward calorimeters and tile calorimeters in the end-cap and forward region), the toroid magnets, and finally the muon chambers for the muon spectrometer in the order from the interaction point and out.

Charged particles in a magnetic field will bend their trajectory towards the magnetic field lines. The inner detector and the muon spectrometer can measure the transverse momentum of a particle as this scales linearly as a function of the curvature and the strength of the magnetic field \cite{PeterHansen}. To measure energetic particles with relatively low uncertainty, a strong magnetic field is required. For this task, ATLAS has installed four magnets, which are the central solenoid between the inner detector and the electromagnetic calorimeter and three toroids around the muon spectrometers in the barrel and the two end-caps. The solenoid provides a $2$~T axial magnetic field, and the toroids provide $0.5$ and $1$~T toroidal magnetic fields for the barrel and end-cap regions, respectively \cite{Aad:1129811}.

Some sub-detectors must be cooled to lower thermally induced noise. The Pixel and SCT are cooled to $-7$~\textdegree{}C, but the TRT must be heated to stay at $20$~\textdegree{}C \cite{Aad:1129811}.

\section{The inner detector}
\begin{figure}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/ID_newTRT_d3.pdf}
	\caption{The inner detector. From \cite{Aad:1129811}.}
	\label{fig:atlas:id}
\end{figure}
The inner detector (shown in \cref{fig:atlas:id}) consists of two silicon-based trackers (the Pixel and SCT) that provide precise tracking information, which consists of precise momentum resolution and especially precise angular resolution, and a straw tube tracker (the TRT) that adds similar momentum resolution due to its distance from the interaction point and the high number of hit straws on average. For the barrel and end-caps, the silicon trackers cover up to $|\eta|<2.5$, while the TRT covers up to $|\eta|<2.0$.

The silicon-based trackers can also measure the impact parameter of a track, which aids the identification of heavy quarks and $\tau$ leptons. The TRT can detect transition radiation (TR) from high $\gamma$ particles, which is used in electron identification.

%\FloatBarrier
\subsection{The Pixel and Semiconductor trackers}
\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/FigID11alast.pdf}
	\caption{The layers of the sub-detectors of the inner detector. The IBL is not drawn. From \cite{Aad:1129811}.}
	\label{fig:atlas:id2}
\end{marginfigure}
The Pixel detector consists of $1744$ individual pixel sensors with $47232$ pixels each, totaling approximately $80$ million readout channels. The sensors are made of $n$-type wafers \cite{Aad:1129811}. The minimum pixel is $50 \times 400$ \textmu{}m$^2$ in $R-\phi \times z$. The modules are arranged in three layers in the barrel and three disks in each end-cap. The Pixel detector is placed close to the beam pipe; the first layer in the barrel is only $50$~mm from the beamline (cf. \cref{fig:atlas:id2}). The Pixel modules have en intrinsic accuracy of $10 \times 115$ \textmu{}m$^2$ in $R-\phi \times z$ in the barrel and in $R-\phi \times R$ in the end-caps \cite{Aad:1129811}.

% IBL radius
The Pixel additionally contains the Inner B-Layer (IBL) placed between the beam-pipe and the first layer of the Pixel detector. The IBL contributes significantly to the tracking performance as well as the identification of heavy quarks and $\tau$ leptons.

In the barrel, the SCT sensors use strips of silicon parallel to the beamline with another set of strips crossing at a stereo angle of $40$~mrad. The strip pairs are placed in four double layers in the barrel. In the end-caps, the strips run radially with the second set of strips again crossing at an angle of $40$~mrad. There are a total of $6.3$ million readout channels. The SCT modules have en intrinsic accuracy of $17 \times 580$ \textmu{}m$^2$ in $R-\phi \times z$ in the barrel and in $R-\phi \times R$ in the end-caps \cite{Aad:1129811}.

\FloatBarrier
\subsection{The Transition radiation tracker}
\label{sec:atlas:inner:trt}
\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\subfloat[2015]{\includegraphics[width=\linewidth]{figures/outside/fig_01.png}}
	%	\hspace*{1em}
	\\
	\subfloat[2016]{\includegraphics[width=\linewidth]{figures/outside/fig_02.png}}
	\caption{Gas configuration in the TRT modules for the years 2015 and 2016. From \cite{ManjarresRamos:2202124}.}
	\label{fig:atlas:trtgasperyear}
\end{marginfigure}
The TRT is a gas-filled straw tube tracker. The tubes are $4$~mm diameter wide, $70$~\textmu{}m thin-walled \emph{straw tubes} of coated polymers with $31$~\textmu{}m diameter gold-plated tungsten wires in the center \cite{trtdoc,Aaboud:2017odu}. The wall is held at approximately $-1500$~V with respect to the grounded wire. In the barrel, the approximately $140$~cm long straws are parallel to the beamline and are split into two parts at $\eta=0$. In the end-caps, the straws are placed radially in the wheels with a length of $37$~cm. The TRT has approximately $350\,000$ readout channels \cite{Aad:1129811}. Since the TRT straws only run along the beamline with no crossing straws, the TRT only provides $R-\phi$ information in the barrel with an intrinsic accuracy of $130$ \textmu{}m per straw \cite{Aad:1129811}.

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/trt_electron_drift.pdf}
	\caption{A simulation of electron drift in a $2$~T field after an electron with $10$~GeV~energy passes through ${y=0.1}$~mm of the tube with standard xenon gas mixture at 20~\textdegree{}C and $1$~atm pressure. The cascade near the wire is not shown. Adapted from \cite{Klinkby:1123367}.}
	\label{fig:atlas:trtelectrondrift}
\end{marginfigure}

The gas mixture is $70\%$ xenon, $27\%$ CO$_2$, and $3\%$ O$_2$. During the years 2015 and 2016, the xenon gas has been replaced by argon in an increasing number of modules as shown in \cref{fig:atlas:trtgasperyear} due to leaks.% This trend has unfortunately continued in recent years, although no public figures exist yet.

Charged particles traversing the straws, will create approximately $5$ ionization clusters per mm \cite{Aaboud:2017odu}. The electrons will drift towards the wire due to the strong electric field and will end in a cascade close to the wire that amplifies the signal by $4$ orders of magnitude. See \cref{fig:atlas:trtelectrondrift} for a simulation of the ionization and drift.

%Reconstructed track cross multiple straws at various distances from their wires. A "hit" is defined as precise if the 

For each straw, a $27$-bit word is recorded over $75$~ns \cite{Mindur:2139567}, such that each bit is $3.125$~ns. The word is split into three even windows ($25$~ns each), where the most significant bit is set if the high threshold was exceeded in that window and the remaining bits are set if the low thresholds were exceeded in the respective bits. The low threshold is set to around $300$~eV and the high threshold (HT) is around $6$~keV. See \cref{fig:atlas:trtdigitization} for an example of this. Due to bandwidth limitations coming from increased luminosity, the four least significant bits are discarded and the signal must peak within the "validity gate" (green bits in figure). Only the middle HT is saved.

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/trtdigitization.png}
	\caption{A pulse recorded by a straw and saved into the $27$-bit word. From \cite{Mindur:2139567}.}
	\label{fig:atlas:trtdigitization}
\end{marginfigure}

The TRT is capable of providing electron identification through the detection of TR photons created by ultra-relativistic ($\gamma>1\, 000$) charged particles passing through the passive radiator material in front of the straws. This TR is identified by charge deposition that exceeds the HT, as xenon is efficient in absorbing the TR photons. Argon has a much lower efficiency in absorbing these photons but shows similar tracking capabilities \cite{Aaboud:2017odu}. The details of electron identification will be presented in \cref{sec:trttheory}.

The current TRT electron identification is calibrated using only simulated data. A study into deriving the calibration in data has been done. The study shows little improvement in identification but does eliminate issues with an increasing number of non-electrons at low non-electron probability and correction factors that show regression in data only~\cite{Nielsen:2318461}.\footnote{This refers to the TRT analysis of this thesis.}

Until recently, the TRT has only used the likelihood method for electron identification. A recent study \cite{Bayirli:2705706} on simulated data using long short-term memory neural networks with all hits shows an improvement in background reduction of up to a factor of 2. The neural network is also less sensitive to increasing pileup.

%Time-over-threshold (ToT) is a measure of the time a signal spends over the low threshold. ToT is a measure of the ionization (dE/dx).

\FloatBarrier
\section{The calorimeters}
\begin{figure}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/Calorimeter_d3.pdf}
	\caption{The calorimeters. From \cite{Aad:1129811}.}
	\label{fig:atlas:calo}
\end{figure}
The calorimeters consist of dense absorber material, with which particles will interact and lose energy through various radiative processes explained in \cref{sec:hadronphysics:charged}, and active material, which will absorb the photons and convert them to electric signals.

The calorimeters of ATLAS serve different purposes. The high granularity liquid-argon (LAr) electromagnetic calorimeter with lead as passive material identifies and measures electrons and photons within $|\eta|<1.475$ in the barrel (ECAL) and $1.375<|\eta|<3.2$ in the end-caps (EMEC), and the scintillator-tile hadronic calorimeter (Tile) with steel as passive material, which is split into a central part with extended parts on either side, measures hadrons within $|\eta|<1.7$ in the barrel \cite{Aad:1129811}. In the end-caps, there is a LAr hadronic calorimeter in the ranges $1.5<|\eta|<3.2$ (HEC). Beyond this, there is a LAr calorimeter (FCAL) for electromagnetic and hadronic energy measurements in the ranges $3.1<|\eta|<4.9$. The calorimeters can be identified in \cref{fig:atlas:calo}.

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/LARG3-TDR-barrelM.pdf}
	\caption{The three layers of the ECAL after the pre-sampler. From \cite{Aad:1129811}.}
	\label{fig:atlas:caloaccordion}
\end{marginfigure}

The ECAL is accordion-shaped to give full coverage in $\phi$. This can be seen in \cref{fig:atlas:caloaccordion}, which shows the three barrel layers of the ECAL. The first layer is only coarsely segmented in $\phi$ but very finely in $\eta$. The read-out electronics is at the back for layers 2 and 3, but in front of the ECAL for layer 1. There is a pre-sampler -- a layer 0 -- in front of the ECAL up to $|\eta|<1.8$, which measures the energy lost in front of the ECAL \cite{Aad:1129811}. The second layer has a depth of $16~X_0$, which captures the majority of the electron and photons even at high energies.

The sizes of the calorimeters are determined by the containment of particle showers needed in terms of radiations lengths $X_0$ (for electromagnetic particles) and interactions length $\lambda$ (for hadronic particles). The ECAL extends to $>22 X_0$ in the barrel and $>24 X_0$ in the end-caps. The hadronic calorimeters cover approximately $10 \lambda$.

\section{The muon spectrometer}
\begin{figure}[hptb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/MuonSystem_d3.pdf}
	\caption{The muon spectrometer. From \cite{Aad:1129811}.}
	\label{fig:atlas:muon}
\end{figure}

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/Muon_sector_numbering.pdf}
	\caption{A cross-section of the MS in the barrel with the toroid magnet system in-between. From \cite{Aad:1129811}.}
	\label{fig:atlas:muoncrosssection}
\end{marginfigure}

The muon spectrometer (MS) consists of several subsystems, which are mainly responsible for either tracking or triggering. The subsystems encompass the calorimeters together with the toroid magnet system (see \cref{fig:atlas:muon}). The muon chambers and toroid magnets are intertwined as seen in \cref{fig:atlas:muoncrosssection} with a gap at $|\eta|<0.1$ for cabling. This outer part is rather large and mainly responsible for giving ATLAS the diameter of $25$~m compared to the $15$~m of CMS.

Muons are mips (cf.~\cref{sec:hadronphysics:charged}), so they will traverse the calorimeters, and since they are the only known particles (except for the neutrinos, of course) to do so, one can measure the momenta of the tracks left in the MS by charged particles under the assumption that these are left by muons. The magnetic field to bend the muon tracks is provided by the barrel toroid for $|\eta|<1.4$, by the end-cap toroids for $1.6<|\eta|<2.7$, and both the barrel and end-caps toroids in $1.4<|\eta|<1.6$ \cite{Aad:1129811}.

The four subsystems, which will be detailed in a moment, are each placed in multiple layers in the barrel and end-caps. The first two subsystems measure the track coordinates in the $r-z$ plane (the bending plane), from which the muon momentum can be derived. The last two subsystems provide trigger information and additionally provide coordinate information in the $r-\phi$ plane (the non-bending plane) \cite{muonpaper}.

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\subfloat[A cross-section of an MDT tube. From \cite{Aad:1129811}]{\includegraphics[width=0.95\linewidth]{figures/outside/MDT_tube_cross_section.pdf}\label{fig:atlas:mdtcrosssection}}
	\\
	\subfloat[The CSC wires and strips with a pulse across one wire. Adapted from \cite{Aad:1129811}]{\includegraphics[width=\linewidth]{figures/outside/csc_readout_strips.jpg}\label{fig:atlas:cscsegments}}
	\caption{The precision measurement subsystems of the MS.}
\end{marginfigure}

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\subfloat[A cross-section of an RPC. Adapted from \cite{Aad:1129811}]{\includegraphics[width=0.95\linewidth]{figures/outside/RPC_structure.pdf}\label{fig:atlas:rpc}}
	\\
	\subfloat[The TGC structure with the wires and strips. From \cite{Aad:1129811}]{\includegraphics[width=\linewidth]{figures/outside/TGC_structure.pdf}\label{fig:atlas:tgc}}
	\caption{The triggering subsystems of the MS.}
\end{marginfigure}

The four subsystems are \cite{Aad:1129811}:
\begin{myitemize}
	\item[\textbf{Monitored Drift Tubes (MDTs):}] These modules, consisting of several drift tubes, measure track coordinates with high precision -- resolutions in $z$ of approximately $80$~\textmu{}m per tube or $35$~\textmu{}m per module -- within $|\eta|<2.7$ (though only $|\eta|<2.0$ for the inner-most layer). The drift tubes are similar to those of the TRT as in they are held at a potential (about $-3000$~V) with respect to a tungsten-rhenium wire that will pull electrons that have been ionized by the gas ($93\%$ argon and $7$ CO$_2$) towards the wire as drawn in \cref{fig:atlas:mdtcrosssection}. The MDTs are unable to unambiguously provide coordinate information for more than one track per chamber, but collinear muons can still be unambiguously resolved using the inner detector (more on this in the reconstruction chapter, \cref{sec:rec:mu}).
	\item[\textbf{Cathode Strip Chambers (CSCs):}] These multiwire proportional chambers are placed in the end-caps only with wires running radially from the center with strips perpendicular to the wire, from which signals are read out, as shown in \cref{fig:atlas:cscsegments}. The chambers operate at $1900$~V with a gas mixture of $80\%$ argon and $20\%$ CO$_2$. These strips measure track coordinates in the bending plane with resolutions of approximately $60$~\textmu{}m per plane or $40$~\textmu{}m per chamber in the range $2.0<|\eta|<2.7$. There are additional strips parallel to the wires that give $\phi$ coordinates with resolutions of $5$~mm. The CSCs are capable of disambiguating two tracks and provide accurate $\eta$ and $\phi$ information for each due to the different pulse heights left by the tracks.
	\item[\textbf{Resistive Plate Chambers (RPCs):}] The chambers consist of parallel resistive plates with a gas mixture of primarily C$_2$H$_2$F$_4$. The transverse and longitudinal strips shown in \cref{fig:atlas:rpc} provide triggering information as well as coarse track information within $|\eta|<1.05$ with $10$~mm resolutions in $z$ and $\phi$.
	\item[\textbf{Thin Gap Chambers (TGCs):}] The TGC is also a multiwire proportional chamber (\cref{fig:atlas:tgc}). It contains $55\%$ CO$_2$ and $45\%$ n-pentane with a $2900$~V potential between its wires and the plates. It provide triggering information in the range $1.05<|\eta|<2.4$ as well as track information with up to $6$~mm in $R$ and up to $7$~mm in $\phi$.
\end{myitemize}

All three chambers (the MDT is not included) have adequate time resolutions ($7$, $1.5$, and $4$~ns in the above order), making it possible to tag beam-crossings. The trigger chambers, the RPCs and TGCs, require coincidence between the last three layers to generate a trigger with the curvature of the track matching well-defined \pT thresholds, where the TGCs can provide this information up to $|\eta|<2.7$ \cite{Aad:1129811}.

\section{Trigger and data acquisition}
\label{sec:atlas:trigger}
\begin{figure}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/fig_01.pdf}
	\caption{The ATLAS TDAQ flow chart showing the process by which sub-detector information is handled by the L1 and passed to the HLT. Adapted from \cite{Aad:2020wji}.}
	\label{fig:atlas:trigger}
\end{figure}
The ATLAS trigger and data acquisition (TDAQ) is responsible for deciding which collisions are saved as they occur every $25$~ns (${\mathrm{rate}=40}$~MHz) \cite{Aad:2020wji,Aad:2020uyd,Aad:2019wsl}. This is done in two steps that bring the initial $40$~MHz down to approximately $100$~kHz using an analog system named the first level trigger (L1) and further down to approximately $1$~kHz using a software-based trigger running on a cpu farm named the high-level trigger (HLT). The L1 will evidently make fast decisions using rudimentary reconstructions of the events -- by defining regions of interest (RoIs) -- within the $2.5$~\textmu{}s windows (to keep the output rate at $100$~kHz) and pass these triggered events to the much slower HLT, which is capable of performing more complex reconstructions to reject more background.

The flow is shown in \cref{fig:atlas:trigger}. The L1Calo takes input from the calorimeters and identifies candidates for electrons, photons, $\tau$-leptons, jets, and missing transverse energy from clusters in the calorimeter. The L1Muon determines curvatures from hits in RPCs and TGCs as mentioned in the previous section. For L1-triggered events, data from the sub-detectors is sent to the HLT along with the RoIs. The HLT has the full detector information available from the calorimeters, MS, and the ID, which are not available at L1. The HLT will reconstruct events within the RoIs. While the HLT uses the same software as the offline reconstruction, much work has gone into making it as fast as possible, including selecting particle candidates in steps and stopping immediately when a criterion is not met.








