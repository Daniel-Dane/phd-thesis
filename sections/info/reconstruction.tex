\chapter{Reconstruction}
\emph{\small
	The previous chapters introduced the particles of the Standard Model and their interactions in hadronic colliders (\cref{sec:theory}) as well as the sub-detectors of the ATLAS detector (\cref{sec:atlas}), which record the results of these interactions. It is now time to connect these and look into how an event recorded by the ATLAS detector is related back to the fundamental particles that are produced in a collision. This act is called \emph{reconstruction}. In this chapter, we will introduce vertices, tracks, and clusters, which are integral for the reconstruction of leptons and jets.
}
\minitoc
\label{sec:reconstruction}

~\\\noindent
Collision events\footnote{Non-collision events such as cosmic muon calibration runs are not considered in this.} are reconstructed several times from the raw data recorded by ATLAS into objects that are used in analysis, first by the triggers and then finally offline. In \cref{sec:atlas:trigger}, the trigger system was introduced, in which it was stated that preliminary reconstruction is done already at the L1 trigger and afterwards in the HLT. The raw data as well as objects and decisions made by the triggers are written to disk.

The L1 and HLT triggers identify particles of specific types under given conditions using algorithms called "triggers", and their names follow this convention \cite{Aad:2019wsl}:
\begin{verbatim}
[Trigger level]_[object multiplicity][object type][minimum transvese energy in GeV],
\end{verbatim}
with details listed in \cref{tab:reconstruction:triggernames}. The trigger level may refer to L1 or HLT and is often omitted. The object multiplicity is omitted for single-particle triggers. The type refers to photons "g" or electrons "e" for HLT triggers. The L1 trigger seeding the HLT trigger is sometimes added as a suffix. As an example, the trigger named e24\_lhmedium\_L1EM20VH fires when the HLT identifies an electron candidate with $\ET>24~\GeV$ and passing LHMedium identification coming from the L1 seed of an EM cluster with an average $\ET$ of $20~\GeV$, depending on $\eta$, and with an additional hadronic veto.

\begin{table*}[thbp]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\begin{tabularx}{\textwidth}{lrp{13em}X}
		\toprule
		             &                & \textbf{Photons}        & \textbf{Electrons}                             \\ \midrule
		\textbf{HLT} & Identification & loose, medium, tight    & lhvloose, lhloose, lhmedium, lhtight           \\
		             & Isolation      & icalovloose, icalotight & ivarloose                                      \\
		             & "nod0"         & -                       & Transverse impact parameter not used ("no d0") \\
		             & "etcut"        &      \multicolumn{2}{c}{$\ET$-only requirement applied in the HLT}       \\
		\textbf{L1}  & "V"            & -                       & $\eta$-dependence of $\ET$ threshold           \\
		             & "H"            & -                       & Veto on hadronic activity                      \\ \bottomrule
		             &                &                         &
	\end{tabularx}
	\caption{Some suffixes and their meanings for photon and electron triggers. From \cite{Aad:2019wsl}.}
	\label{tab:reconstruction:triggernames}
\end{table*}

To keep the output rate from the L1 and HLT triggers level, triggers that fire too often, due to increased activity, are ignored following a probabilistic decision-maker \cite{Aad:2019wsl}. This is called \emph{prescaling}, and extra care in calculating the recorded luminosity must be taken when using prescaled triggers. Many analyses use \emph{unprescaled} triggers since they often have higher \pT requirements anyway.

%\begin{figure}[hptb]
%	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
%	\centering
%	\includegraphics[width=\linewidth]{figures/outside/egammarec.png}
%	\caption{The trigger sequence for photons and electrons in the HLT. From \cite{Aad:2019wsl}.}
%	\label{fig:reconstruction:egammarec}
%\end{figure}

% sec 6 of ref
Triggers for electrons (and photons) at the L1 use a sliding window called a \emph{trigger tower} of size $4\times4$ and granularity $0.1\times0.1$ in $\eta\times\phi$ in the ECAL to build the RoI \cite{Aad:2019wsl}, in which the $E_T$ is calculated and used as input for the different thresholds ($20~\GeV$ in the case of the previously named trigger). A similar tower is built in the hadronic calorimeter to apply the hadronic veto to electrons if the \ET in the hadronic tower exceeds an \ET-dependent threshold. Similarly, the isolation requirement fails the \ET of any tower around the RoI in the ECAL exceeds an \ET-dependent threshold.

%\cref{fig:reconstruction:egammarec} shows the reconstruction chain for the HLT. Fast algorithms offer early rejection of background, while the slower, precise algorithms are similar to the offline reconstructions with some exceptions \cite{Aad:2019wsl}.

Recorded events are processed using the full offline reconstruction framework using the raw and trigger data. This step reconstructs tracks, vertices, leptons, jets, and more, and saves these in datasets to disk. While these datasets can be used in analysis, they are incredibly large in size (at least hundreds of terabytes), due to the number of events recorded and the information recorded and constructed for the events. In ATLAS, these datasets are converted to the xAOD format, which keeps much less raw information. For analyses, even the xAOD datasets are too large, and derived datasets, or DxAODs, are made, which remove irrelevant events using simple selections in accordance with signal signatures (e.g. for signals with a leptonically decaying $Z$, at least two leptons with invariant mass above $50~\GeV$) and unused objects and lastly unused variables for the remaining objects. These DxAOD datasets will be on the order of percent in size compared to their xAOD parent datasets. Even then, an analysis may require many DxAODs with a total size of tens or hundreds of terabytes.

The remaining sections of this chapter are dedicated to the reconstruction of objects used by the analyses in this thesis and will end with some universal selections used to clean events.

\section{Tracks and vertices}
\label{sec:rec:track}
\label{sec:rec:vertex}
As charged particles pass through the ID, they are detected by the \emph{hits} that they leave in the different layers. Their paths, or \emph{tracks}, can be reconstructed by connecting the hits \cite{Aaboud:2019ynx,Cornelissen:2007vba}. The tracks are seeded by three-dimensional coordinates, or \emph{space points}, created from the hits in the Pixel and SCT sub-detectors and extrapolated through to hits in outer layers using the Kalman filter \cite{Fruhwirth:1987fm}, which extrapolates tracks to the next layers by inverting small covariance matrices, compares the extrapolations to the actual hits, and detects outliers by their higher contribution to the $\chi^2$ of the evolving fit \cite{PeterHansen,Cornelissen:2007vba}. Tracks that fail reconstruction may be retried under an electron hypothesis that accounts for bremsstrahlung, if ${\pT>1~\GeV}$ and the track failed to gather at least $7$ Pixel and SCT hits \cite{Aaboud:2019ynx}. The first step creates many track candidates, which must be cleaned for shared hits, incomplete tracks, and fake tracks that do not originate from single charged particles (ie. noise or many crossing particles) \cite{Cornelissen:2007vba}. Finally, the track candidates are extended into the TRT, and tracks are refitted using all sub-detectors.

Tracks can also be formed from seeds in the TRT in order to find particles from secondary decays and converted photons, which are photons that have pair-produced to real electron pairs \cite{Cornelissen:2007vba}. These converted photons originate from secondary vertices within the ID and can be identified by regular or the TRT tracks.

Primary vertices are formed from tracks that are extrapolated back to the beam spot \cite{PeterHansen,Cornelissen:2007vba}. Only tracks that pass the following criteria are considered in vertex formation \cite{ATL-PHYS-PUB-2015-026}:
\begin{myitemize}
	\item $\pT>400~\MeV$
	\item $|\eta|<2.5$
	\item Number of Pixel+SCT hits $\ge 9$ for $|\eta|\le 1.65$ and $\ge 11$ for $|\eta|>1.65$
	\item At least one IBL hit
	\item No Pixel holes\footnote{A hole is a Pixel point, which is passed by the track but was not registered by the point. A veto on holes suppresses fake tracks.}
	\item At most one SCT hole
\end{myitemize}
A vertex requires at least two passing tracks.

Tracks are parameterized in $(z_0, d_0, \theta, \phi, q/p)$ from the \emph{perigee} point, which is defined as the point of closest approach to the beamline. $z_0$ is the longitudinal impact parameter and is the distance from the perigee to the origin. $d_0$ is the transverse impact parameter and is the shortest distance from the track to the beamline. Usually, the significance of $d_0$ is used to suppress fake tracks. $q/p$ is the track curvature defined as the inverse of the momentum, where the sign of $q$ is the sign of the charge and hence the curvature of the track.

\section{Electrons}
\label{sec:rec:el}
Electrons and photons produce similar signatures in the ECAL, which is reflected in the reconstruction. During the reconstruction of electrons, several steps are taken to disambiguate them from converted and non-converted photons. Therefore, this section will occasionally cover photons when needed. Electron candidates are created from \emph{superclusters} made from \emph{topo-clusters}, which are topologically connected ECAL or HCAL cells, and matched to tracks from the ID. The steps leading up to the supercluster formations are as follows~\cite{1908.00005} (see also \cref{fig:rec:el:flow}).

Topo-clusters \cite{topoclustering} are formed by seed cells in the second layer of the ECAL. A cell with significance $|\rho_\mathrm{cell}^\mathrm{EM}|>4$, where \( \rho_\mathrm{cell}^\mathrm{EM} = E_\mathrm{cell}^\mathrm{EM}/\sigma_\mathrm{noise,cell}^\mathrm{EM} \), seeds a topo-cluster. $\sigma_\mathrm{noise,cell}^\mathrm{EM}$ is the expected noise in that cell and includes electronic noise and noise from the level of pileup. All neighboring cells with $|\rho_\mathrm{cell}^\mathrm{EM}|>2$ are added until the cluster is surrounded by cells with $|\rho_\mathrm{cell}^\mathrm{EM}|<2$. Clusters are combined if they merge at this step. Finally, all cells immediately neighboring the clusters are added. Clusters are split if they contain multiple cells with $E_\mathrm{cell}^\mathrm{EM}>500~\MeV$, at least four neighbors and no neighbor with a larger signal. At this stage, the topo-clusters are formed.

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/electron_reconstruction_algorithm.pdf}
	\caption{The steps in reconstruction of electrons and photons. Adapted from \cite{1908.00005}.}
	\label{fig:rec:el:flow}
\end{marginfigure}

Tracks that loosely match fixed-size clusters in the ECAL are refitted with the Gaussian Sum Filter (GSF) algorithm, which is more general than the Kalman filter and less sensitive to bremsstrahlung~\cite{ATLAS-CONF-2012-047}. The tracks are then matched to the topo-clusters. If multiple tracks match the same topo-cluster, a ranking system is employed that, among other things, prioritizes tracks with more Pixel hits.

Conversion vertices for converted photons are reconstructed from the regular and TRT-seeded tracks, which are required to be identified as electrons by the TRT PID. Several steps are taken to identify single- and double-track conversions and lower the fraction of unconverted photons. The vertices are then matched to the topo-clusters.

\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/electron_supercluster.png}
	\caption{The electron supercluster. Adapted from \cite{1908.00005}.}
	\label{fig:rec:el:supercluster}
\end{marginfigure}

Superclusters are then seeded by the \ET-leading topo-clusters, if they pass \ET and ID hit requirements and have not been used already in another supercluster. Topo-clusters in windows of $0.075\times 0.125$ in $\Delta\eta\times\Delta\phi$ are added as satellites (first box of \cref{fig:rec:el:supercluster}). Topo-clusters in a larger window of $0.125\times0.300$ (second box) are added as satellites if their associated tracks are the same as the tracks associated to their seeding clusters.

Electron and photon superclusters are built independently and are therefore disambiguated by considering the possibly matched tracks and their number of hits and the possibly matched conversion vertices. This step may fail to disambiguate the clusters, and the failed objects are placed in both containers with the "ambiguity" author.

Calibrations are applied both before and after disambiguation, including calibrations on the energy scale. Variables pertaining to shower shapes, discrimination, etc. are calculated.

The quality of a candidate is asserted through different selection criteria that \emph{identify} the object to some level. Electrons are identified using a series of likelihoods \cite{1908.00005} for different energy and $\eta$ ranges, which use shower shape variables calculated from their clusters, matching between cluster and track, and the TRT electron PID. The identification seeks to reject especially hadronic jets (for which a rejection factor of about $100\,000$ is needed at low energies, if memory serves). Three working points (WPs) offer an increasing level of signal purity at the expense of signal efficiency, namely \emph{LooseLH}, \emph{MediumLH}, and \emph{TightLH}. The target signal efficiencies are quoted as $93\%$, $88\%$, and $80\%$, respectively. However, the efficiencies of the selections depend on energy and $\eta$ with the loose WP having a signal efficiency of at least $86\%$ and the tight at least $58\%$ for any $\eta$ and energy \cite{1908.00005}.

A separate approach to the suppression of electrons from hadronic decays is known as \emph{isolation} \cite{1908.00005}. The hadronic decays lead to larger activity in the ID and calorimeters. Isolation is defined as the sum of \pT of tracks ($\pT>1~\GeV$ per track) or clusters, respectively, in a cone of size $\dR=\sqrt{\Delta\eta^2+\Delta\phi^2}$ around the object in question, where the object itself is not included in the sum. The isolation in the calorimeter must be less than $20\%$ ($6\%$) of the electron's \pT for a cone of size $\dR=0.2$ for the loose (tight) WP. The track isolation requires $15\%$ ($6\%$) for a cone of size $\dR=\min(10~\GeV/\pT,\,0.2)$. A WP, HighPtCaloOnly, efficient for highly energetic electrons, has a threshold of $3.5~\GeV$ on the sum of clusters in a cone of $\dR=0.2$.

\section{Muons}
\label{sec:rec:mu}

Muons are created from tracks reconstructed at least either in the muon spectrometer or inner detector.

Independent track segments are created in the various chambers of the MS. Muon track candidates are fitted using the hits of these segments. Muon tracks can share some of their segments with other muons to allow for very collinear muons. During the full $\chi^2$ fits of candidates, hits may be removed or added. There are four types of muons that differ by the ability to reconstruct the muons' traversal through the whole detector \cite{1603.05598}:
\begin{myitemize}
	\item[\textbf{Combined (CB) muons}] are reconstructed from tracks in the MS and ID, which are refit as a combined track.
	\item[\textbf{Segment-tagged (ST) muons}] match an ID track with track segments in the MDT or CSC\footnote{These are the two precision measurement subsystems.} of the MS. These types of muons are created when only one MS layer can be matched to the muon.
	\item[\textbf{Calorimeter-tagged (CT) muons}] are ID tracks matched with low energy deposit in the calorimeters left by mips. Due to their low purity, these types of muons are only reconstructed in $|\eta|<0.1$ to recover lost acceptance.
	\item[\textbf{Extrapolated (ME) or stand-alone (SA) muons}] are MS tracks that, accounting for energy loss, loosely extrapolate back to the interaction point. These types of muons are created in the $2.5<|\eta|<2.7$ range to compensate for the lack of the ID.
\end{myitemize}

The order listed is the priority given to the different types when different types of muons share the same ID track.

Muon identification primarily relies on minimum requirements on the number of hits in the different sub-detectors. For all types but the extrapolated muons, a minimum number of Pixel and SCT hits are independently required as well as a successful TRT extension for the three inclusive WPs (loose, medium, and tight). Additionally, for the medium and tight WPs, the tracks must have a minimum number of hits in the MS. Combined muons have further requirements on the significance of the $q/p$ imbalance between the ID and MS tracks as well as the \pT imbalance between ID and MS tracks against their combined \pT to suppress fakes from hadronic activity. To further suppress fakes, the tight WP also selects on the normalized $\chi^2$ of the combined fit \cite{1603.05598}. All four muon types are considered for the loose WP. For the medium WP, only CB and ME muons are used. Finally, only CB muons can pass the tight WP. Almost all loose muons are combined. For $20<\pT<100~\GeV$, the signal efficiencies are approx. $98\%$, $96\%$, and $92\%$ for loose, medium, and tight WPs, respectively, according to Ref. \cite{1603.05598}. These values may have changed since the publication. Two special WPs can be used for very low \pT ($\pT<5~\GeV$) and very high \pT muons ($\pT>300~\GeV$).

Isolation for muons are similar to that of electrons. The isolation in the calorimeter must be less than $30\%$ ($6\%$) of the electron's \pT for a cone of size $\dR=0.2$ for the loose (tight) WP. The track isolation requires $15\%$ ($6\%$) for a cone of size $\dR=\min(10~\GeV/\pT,\,0.3)$. The efficiency of the loose WP is $96\%$ at $15~\GeV$ and increases steadily for higher transverse momentum according to Ref. \cite{1603.05598}. These values may also have changed since the publication. A WP, TightTrackOnly, has a threshold of $6\%$ for a cone of size $\dR=\min(10~\GeV/\pT,0.2)$.

\section{Jets}
\label{sec:rec:jet}
The physical process of forming jets in the detector was covered in \cref{sec:theory:hadronphysics:QG}. In this section, the jets will be reconstructed from topo-clusters\footnote{The process of forming topo-clusters was introduced in the previous section.} in the HCAL \cite{Lampl:1099735,topoclustering} by the anti-$k_t$ algorithm \cite{Cacciari_2008} with $R=0.4$ using Fastjet \cite{Cacciari:2011ma}.% Jets are calibrated by a jet energy scale (JES) correction.

The anti-$k_t$ algorithm belongs to a class of sequential recombination algorithms along with the $k_t$~\cite{Ellis:1993tq} and Cambridge/Aachen~\cite{CambridgeAachen} algorithms, differing only by the power of the energy scale in the distance measure \cite{Cacciari_2008}. The distance measure and its upper limit per jet are defined as,
\begin{align}
	d_{ij} &= \min\left(k_{ti}^{2p},\, k_{tj}^{2p}\right)\frac{\dR^2}{R^2}, \quad \dR^2=(y_i-y_j)^2+(\phi_i-\phi_j)^2,\\
	d_{iB} &= k_{ti}^{2p},
\end{align}
where $k_{ti}$, $y_i$, and $\phi_i$ are the transverse momentum, rapidity, and azimuthal angle of particle $i$, respectively. ${p=1}$ and ${p=0}$ recover the $k_t$ and Cambridge/Aachen algorithms, respectively, while ${p=-1}$ is named the anti-$k_t$ algorithm. ${R=0.4}$ is the radius parameter. The distance metric $d_{ij}$ has two parts, the energy scale and $\dR$. The algorithm combines two topo-clusters $i$ and $j$, which have the smallest $d_{ij}$. The $p=-1$ in the energy scale ensures that the size of the metric is determined by the hardest\footnote{Jargon for the \pT-leading.} particle, while $d_{ij}$ between two soft particles will be larger. As such, soft topo-clusters will combine with harder topo-clusters. The process continues until $d_{iB}<d_{ij}$, at which point the combined topo-cluster is called a jet and is removed from the list. The combination then continues with the smallest $d_{ij}$. Due to $p=-1$, jets will be circular around the leading topo-cluster component. If two jets are within $2R$ of each other, the leading jet $1$ will be circular, overlapping the sub-leading jet $2$, in case of $k_{t1}\gg k_{t2}$. If their transverse momenta are similar, they will divide the area among them. The result of an example event is shown in \cref{fig:rec:jet:clustering}.

\begin{figure}[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/jet_clustering.pdf}
	\caption{The jets formed by the anti-$k_t$, $k_t$, and Cambridge/Aachen algorithms, respectively. From \cite{Cacciari_2008}.}
	\label{fig:rec:jet:clustering}
\end{figure}

The Cambridge/Aachen algorithm is used to make $R=1.0$ \emph{large-R jets}, which better capture the soft substructures within their cones since the distance metric is independent of energy.

While the anti-$k_t$ algorithm is less sensitive to soft components, which may lead to large jet areas for the other algorithms \cite{Cacciari_2008}, several steps are needed to correct for contributions from the underlying event, pileup, and detector effects \cite{Aaboud:2257300}. They include, in the following order:
\begin{myitemize}
	\item[\textbf{Origin correction:}] The origin of the jet is moved to the primary vertex of the hard scatter without changing the energy.
	\item[\textbf{Pileup correction:}] The contribution from pileup jets is subtracted from the energy of the jet, based on the pileup of the event and jet area.
	\item[\textbf{Jet energy scale:}] The jet energy scale (JES) is corrected by applying scale factors derived from simulation. An example is shown in \cref{fig:rec:jet:jes}.
\begin{marginfigure}%[htpb]
	\checkoddpage \ifoddpage \forcerectofloat \else \forceversofloat \fi %Automatically moves the caption to the correct side, if LaTeX pushes the figure down a page after creating the figure
	\centering
	\includegraphics[width=\linewidth]{figures/outside/jes.pdf}
	\caption{The jet energy response $E^\mathrm{reco}/E^\mathrm{truth}$ as a function of the jet $\eta$ calculated from the geometric center of the detector for jets at different energies. From \cite{Aaboud:2257300}.}
	\label{fig:rec:jet:jes}
\end{marginfigure}
	\item[\textbf{Global calibration:}] The energy is further improved using calorimeter, MS, and track-based variables.
	\item[\textbf{In situ calibration:}] A final correction is applied to jets in real data using calibrations derived in situ.
\end{myitemize}

Jets originating from pileup are suppressed by the jet vertex tagger (JVT) \cite{ATLAS-CONF-2014-018}, which assigns a probability for whether the jet originates from the hard scatter. It is applied to jets with $\pT<60~\GeV$ to improve purity in a regime otherwise dominated by pileup.

While there are means of identifying whether a gluon or a quark originally initiated the jet (although the distinction is ill-defined \cite{1704.03878}), only discrimination between jets initiated by heavy and light quarks is efficient.

Some jets can be reconstructed from processes other than the hard scatter at the interaction point such as proton collision in the beam pipe, cosmic rays, and calorimeter noise \cite{Gonski:2272136}. Since these "fake" or "bad" jets do not originate from proton--proton collisions, they must be identified through \emph{jet cleaning} and disregarded in the analysis. Jet cleaning has two WPs, LooseBad and Tight. The former is used in most analyses.

\section{Event reconstruction quality}
\label{sec:rec:eventquality}

%    if ((eventInfoIn->errorState(xAOD::EventInfo::EventFlagSubDet::Tile) == xAOD::EventInfo::Error)
%|| (eventInfoIn->errorState(xAOD::EventInfo::EventFlagSubDet::LAr) == xAOD::EventInfo::Error)
%|| (eventInfoIn->errorState(xAOD::EventInfo::EventFlagSubDet::SCT) == xAOD::EventInfo::Error)
%|| (eventInfoIn->isEventFlagBitSet(xAOD::EventInfo::Core,18))) {

Several factors from detector malfunction to improper reconstruction and non-collision backgrounds are taken into account when determining the quality of an event. The list includes but is not limited to:
\begin{myitemize}
	\item[\textbf{Good Runs List (GRL):}] The GRL \cite{DAPR-2018-01} marks parts of runs as bad at the luminosity block ("lumiblock", ~1-minute intervals) level if they are deemed unusable by any sub-detector or data quality groups. There are many reasons to mark a lumiblock as bad such as bad calibrations, noisy channels, etc.
	\item[\textbf{Primary vertex:}] An event may seldom be reconstructed with no vertex. An event must have at least one primary vertex with two tracks.
	\item[\textbf{Event cleaning:}] Several defects not covered by the GRL are grouped into the selection known as "event cleaning". The event may contain corrupted data from the ECAL, the Tile calorimeter, the SCT, or due to a mid-run restart.
	\item[\textbf{Bad jet veto:}] Since the fake jets (mentioned in the previous section) may interfere with the reconstruction of hadronic jets, an event must be discarded if a fake jet otherwise passes for a real jet \cite{Gonski:2272136}; if a jet passes the JVT and overlap removal\footnote{Disambiguating different objects built from the same information in the event, e.g. an electron and muon sharing the same ID track. The precise approach will be defined in \cref{sec:azh:obj:or}.} (as well as the kinematic selections, $\pT>20~\GeV$ and $|\eta|<2.8$), and is identified as a fake jet, the event is discarded.
\end{myitemize}
